Changelog
=========

REL_53 (2022-08-05)
------------------
* Feature release
* Add Constantly Calculated Impact Point (CCIP) functionality
* Move Theme option to menu "Manage/System" menu
* Release EFIS v6.13

REL_52 (2022-07-01)
------------------
* Minor release
* Enhancements to dynamic database use
* Correct simulator longitude calculation
* Release EFIS v6.12

REL_51 (2021-08-01)
------------------
* Minor release
* Enhancements to HITS display (base bearing and DME on departure)
* Correct localization for en-US
* Updates to localization (Korean, German, US)
* Release EFIS v6.11

REL_50 (2021-07-15)
------------------
* Minor release
* Enhancements to Wx overlay
* Enhancements to Simulator
* Release EFIS v6.10

REL_48 (2021-07-01)
------------------
* Minor release
* Implement option to enable/disable the GPX cache
* Implement RainViewer (www.rainviewer.com) as Wx radar source
* Clear issue: 67 - Remove default 1,000 dynamic waypoint limitation
* Clear issue: 68 - Light up the spinner triangles when they are tapped
* Release EFIS v6.9

REL_47 (2021-06-07)
------------------
* Internal release
* Alternative Gpx cache handling
* Release EFIS v6.8

REL_46 (2021-05-12)
------------------
* Major release
* Combine PFD, MFD and CFD into a single monolithic application (player.efis.pfd)
* Clear issue: 37 - Consolidate EFIS, DMAP and COMP into a single app
* Retire DMAP and COMP apps
* Release EFIS v6.7

REL_45 (2021-05-01)
------------------
* Release Antarctica / South Pole DEM (player.efis.data.ant.spl)

REL_44 (2021-04-14)
------------------
* Add Antarctica support to DemGTOPO30 (ant-spl is non-standard DEM)
* Add special handling near the geographic poles
* Gpx database infinite loop bugfix
* Release EFIS v6.6
* Release DMAP v6.6
* Release COMP v6.6

REL_43 (2021-03-22)
------------------
* Maintenance release
* Increase font size of ASI, ALT and HDG
* Add units for ASI, ALT and HDG
* Improve compass rose readability
* Release EFIS v6.5
* Release DMAP v6.5
* Release COMP v6.5

REL_42 (2021-02-25)
------------------
* Maintenance release
* Clear issue: 63 - Wpt / Airspace not showing
* Release EFIS v6.4
* Release DMAP v6.4
* Release COMP v6.4

REL_41 (2021-02-02)
------------------
* Maintenance release
* Use departure distance for CalcDme and calcBrg
* Improvements to Apt/Waypoint database loading
* Clamp heading when stationary
* Add North (N90W) and South (S90E) poles as fixed waypoints
* Keep last valid waypoint if none found (instead of defaulting to ZZZZ)
* Replace FastLane with Triple-T
* Release EFIS v6.3
* Release DMAP v6.3
* Release COMP v6.3

REL_40 (2021-01-10)
------------------
* Maintenance release
* Clear issue: 62 - Apt/Waypoint loading
* Minor change to generic A/C naming
* Waypoint database cycle 2021-01 (airport.gpx.xml)
* Release EFIS v6.2
* Release DMAP v6.2
* Release COMP v6.2

REL_39 (2020-12-12)
------------------
* Maintenance release
* Add (always required) Null Island (ZZZZ) to the intl APT database 
* Add Greenland to eur.rus DEM
* Add Indian ocean islands to zar.aus DEM
* Add Pacific ocean islands to usa.can DEM
* Performance improvements
* Clear issue: 58 - Precipitation color in HC theme
* Clear issue: 59 - FDroid icon issue hopefully resolved
* Release EFIS v6.1
* Release DMAP v6.1
* Release COMP v6.1

REL_38 (2020-11-25)
------------------
* Major release
* Implement OpenWeatherMap precipitation overlay on maps
* Introduce generic aircraft instead of individual 
* General bugfixes
* Clear issue: 51 - Compass rose - Issue
* Clear issue: 52 - Add weather data
* Clear issue: 53 - Missing UK airports
* Clear issue: 54 - Slip ball in landscape
* Clear issue: 56 - Weather map obscures data
* Release EFIS v6.0
* Release DMAP v6.0
* Release COMP v6.0

REL_37 (2020-07-08)
------------------
* Bugfix release
* Clear issue: 55 - Compass Rose bug
* Release EFIS v5.3
* Release DMAP v5.3
* Release COMP v5.3

REL_36 (2020-07-03)
------------------
* Maintenance release
* Clear issue: 45 - High quality text rendering 
* Release EFIS v5.2
* Release DMAP v5.2
* Release COMP v5.2

REL_35 (2020-05-05)
------------------
* Maintenance release
* opensky-network.org traffic feed polling time adjustment
* Release EFIS v5.1
* Release DMAP v5.1
* Release COMP v5.1

REL_34 (2020-03-03)
------------------
* Major release
* Add opensky-network.org as a real-time traffic feed
* Enhance traffic target data readability
* Synchronize version naming 
* Release EFIS v5.0
* Release DMAP v5.0
* Release COMP v5.0

REL_33 (2020-02-06)
------------------
* Maintenance release
* Exclude unused APT codes during spinner selection
* Add Null Island (ZZZZ) to all APT databases
* Remove duplicates from gpx.south.west database
* Release EFIS v4.8
* Release DMAP v2.9
* Release COMP v1.6

REL_32 (2020-01-08)
------------------
* Maintenance release
* Clear issues: 43 
* Release EFIS v4.7
* Release DMAP v2.8
* Release COMP v1.5

REL_31 (2019-12-12)
------------------
* Clear issues: 42
* Release COMP v1.4

REL_30 (2019-11-20)
------------------
* Clear issues: 42
* Release COMP v1.3

REL_29 (2019-09-14)
------------------
* Performance tweak to synthetic vision (Adaptive DEM render)
* Clear issues: 40, 41
* Release EFIS v4.6
* Release DMAP v2.7
* Release COMP v1.2

REL_28 (2019-08-25)
------------------
* Implement side by side display for COMP in landscape view
* Refactor for code consistency - alignment cfd to pfd & mfd
* Merge TacoTheDank branch
* Clear issues: 39
* Release EFIS v4.5
* Release DMAP v2.6
* Release COMP v1.1

REL_27 (2019-07-05)
------------------
* First version of Kwik Composite
* Implement altitude discrimination to target display
* Clear issues: 34, 34
* Release EFIS v4.4
* Release DMAP v2.5
* Release COMP v1.0

REL_26 (2019-04-25)
------------------
* Improvements to "stratux" Wifi handling
* Improvements to Simulator
* Clear issues: 31, 32, 33
* Release EFIS v4.3
* Release DMAP v2.4

REL_25 (2019-01-14)
------------------
* Clear issues: 30
* Release DMAP v2.3

REL_24 (2019-01-02)
------------------
* Clear issues: 29
* Release EFIS v4.2
* Release DMAP v2.2

REL_23 (2018-10-15)
------------------
* Add generic bizjet
* Use Vso as simulator speed
* Make ADS-B targets more prominent
* Clear issues: 26, 27, 28
* Release EFIS v4.1
* Release DMAP v2.1

REL_22 (2018-09-01)
------------------
* Improve Stratux Wifi and GPS handling
* Add individual red X's for Ah, Rose and Map
* Add audio alert for traffic for DMAP (work in progress)
* Clear issues: 20, 22, 21
* Bump Stratux development up a major version
* Release EFIS v4.0
* Release DMAP v2.0

REL_21 (2018-08-18)
------------------
* Add support for Stratux ADS-B ADHRS 
* Drive simulator using the flight director
* Release EFIS v3.12
* Release DMAP v1.7

REL_20 (2018-06-28)
------------------
* Change airspace sequencing
* Improve monochrome handling
* Long overdue code maintenance
* Release EFIS v3.11
* Release DMAP v1.6

REL_19 (2018-06-08)
------------------
* Add generic helicopter
* Release EFIS v3.10
* Release DMAP v1.5

REL_3.11 (2017-03-31)
------------------
* Implement Monochrome theme for HUD usage
* Release EFIS v3.9
* Release DMAP v1.4

REL_3.10 (2017-11-23)
------------------
* Implement Auto zoom
* Scale compass rose font
* Auto geographic selection expanded to all available pacs
* Waypoint database cycle 2017-11 (gpx.south.west)
* Release EFIS v3.8
* Release DMAP v1.3

REL_3.9 (2017-11-12)
------------------
* Add DEM region: pan.arg - South America

REL_3.8 (2017-11-04)
------------------
* Add speed, altitude and AGL displays to map
* Zoom with swipe up/down (use XCSoar convention)
* Improved code commonality and fault tolerance
* Implement Light/Dark themes for better sunlight contrast
* Release EFIS v3.7
* Release DMAP v1.2

VER_3.7 (2017-10-16)
------------------
* Add terrain awareness
* Release DMAP v1.1

VER_3.6 (2017-10-04)
------------------
* Add app: Kwik Digital Map (DMAP v1.0)
* Minor bugfix in compass rose
* Minor performance tweak to data loading
* Refactor EFIS/DMAP to common code
* DMAP: Airspace class filtering
* eur.txt.air (Denmark, Germany, Switzerland)
* Release EFIS v3.6

VER_3.5 (2017-09-21)
------------------
* Activate main menu on Back key
* Add Quit option to main menu
* Add DEM region: sah.jap - Sahara / North Africa, India, Indonesia, Japan
* Waypoint database cycle 2017-10 (gpx.south.east)

VER_3.4 (2017-08-28)
------------------
* Add automatic DEM region determination
* Audio alerts only with valid GPS fix
* Minor change to startup behavior
* Waypoint database cycle 2017-08 (gpx.north.east)

VER_3.3 (2017-07-10)
------------------
* Add fatfinger support
* Minor performance tweaks
* Waypoint database cycle 2017-07 (zar.aus)

VER_3.2 (2017-06-30)
------------------
* Add audio annouciations (stall, sink, terrain, 500)
* DataPac enhanced to allow multiple regions
* Add DEM region: zar.aus - Southern Africa, Australia
* Add DEM region: usa.can - United States, Canada
* Add DEM region: eur.rus - Europe, Russia

VER_3.1 (2017-05-18)
------------------
* Synthetic vision performance optimization
* Improvements to radio altimeter
* Update to DataPac front end
* Waypoint database cycle 2017-06 (zar.aus)

VER_3.0 (2017-05-10)
------------------
* Synthetic vision (DEM) implementation
* Add digital elevation model (DEM) data packs (zar.aus)
* Waypoint database cycle 2017-05 (zar.aus)

VER_2.4 (2014-03-17)
------------------
* Waypoint database cycle 2017-03 (usa.can)
* Change bank/skid correction filter constant
* Add automatic de-clutter to HITS

VER_2.3 (2014-03-15)
------------------
* Add REFLOG.md 

2.3 (2014-03-10)
------------------
* Adopt different versioning scheme
* Rolled up bugfix release 

VER_2.2 (2017-03-02)
------------------
* Waypoint database cycle 2017-03 (zar.aus)
* First Release version on F-Droid

Prior History
------------------
* Fix the selected altitude bug introduced by HITS
* Highway In The Sky (HITS) implementation
* Add M20J. Cosmetic changes
* Production release 2
* Add vertical view and rework panorama
* Increase maximum taxi speed
* Production release 1
* Minor bugfix in demo mode simulator
* Add taxi mode. Bufix version display
* Tweak filter constants. Remove auto calibration
* Set GPS to maximum/realtime performance
* Change pitch to 25 degrees in view. Change wording for sensor bias choices
* Introduce calibration routine. Change pitch filter to 32
