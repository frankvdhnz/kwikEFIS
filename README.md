# kwikEFIS
Kwik Electronic Flight Information System for Android.

<a href="https://search.f-droid.org/?q=kwik">
    <img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">
</a>

Instructions: https://ninelima.org/efis/instructions.html

The author wishes to acknowledge the following people:

Tom Dollmeyer (http://avionics.sourceforge.net/)
Avionics for Experimental Aircraft; Served as the basis and original inspiration 
for the kwikEFIS project.

Ed Williams (https://edwilliams.org/)
Aviation Formulary; I have used information from this most useful work so many times 
and in so many projects that I have lost count.

Apps4Av (https://github.com/apps4av)
WiFi and GDL90; Served as the basis for Stratux integration.
