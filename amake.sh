#!/bin/bash

pskill java
#pskill adb

rm ./apk/kwik-efis.apk

cat ./settings.gradle
./gradlew clean
#./gradlew build
#./gradlew assemble
./gradlew assembleDebug

cp ./app/build/outputs/apk/debug/app-debug.apk ./apk/kwik-efis.apk
cp ./CHANGELOG.md ./apk/CHANGELOG.md

pskill java
#exit

# Copy to Sync 
cp ./app/build/outputs/apk/debug/app-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis.apk
cp ./CHANGELOG.md v:/Sync/Public/KwikEFIS/CHANGELOG.md

pskill java
exit



