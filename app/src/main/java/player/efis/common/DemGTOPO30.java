/*
 * Copyright (C) 2017 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
http://www.webgis.com/srtm30.html
https://dds.cr.usgs.gov/srtm/version2_1/SRTM30/
 */

package player.efis.common;

// Standard imports

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import player.ulib.UNavigation;


/*

a
 +------------------------------------+
 |            b                       |     a = demTopLeftLat,  demTopLeftLon(in decimal egrees)
 |             +---------+            |     b = x0, y0
 |             |         |            |     c = lat0, lon0
 |             |  c +    |            |     d = maxcol, maxrow (?)
 |             |         |            |
 |             +---------+            |
 |                                    |
 |             |< BUFX  >|            |
 |                                    |
 +------------------------------------+
                                        d

 Note: BUFX should fit completely in the DEM tile
       Consider this when choosing size
*/

public class DemGTOPO30
{
    static Context context;
    public static String region = "null.null";

    public final static float DEM_HORIZON = 20; // nm

    static /*final*/ int maxcol = 4800;
    static /*final*/ int maxrow = 6000;
    static /*final*/ int TILE_WIDTH = 40;     // width in degrees, must be integer
    static /*final*/ int TILE_HEIGHT = 50;    // height in degrees, must be integer

    public static final int BUFX = 600;   //800 = 400nm square ie at least  200nm in each direction
    public static final int BUFY = BUFX;  // in units of 30 arcSec (approx 1km / 0.5 nm)

    static final int MAX_ELEV = 6000;  // in meters

    public static final short[][] buff = new short[BUFX][BUFY];
    static final DemColor[] colorTbl = new DemColor[MAX_ELEV];  // 600*3 = r*3

    static float demTopLeftLat = -10;
    static float demTopLeftLon = +100;

    // Also see description above
    static private int x0;  // top left of the BUFX tile
    static private int y0;  // top left of the BUFX tile

    public static float lat0; // center of the BUFX tile
    public static float lon0; // center of the BUFX tile

    public static boolean demDataValid = false;
    public static float gamma = 1;

    //-------------------------------------------------------------------------
    // Construct a new default loader with no flags set
    //
    public DemGTOPO30()
    {
    }

    //public DemGTOPO30(Context context)
    public static void setContext(Context ctx)
    {
        DemGTOPO30.context = ctx;
        setGamma(1);
    }

    public static void setGamma(float g)
    {
        gamma = g;

        //for (short i = 0; i < colorTbl.length; i++) colorTbl[i] = calcColor(i);
        for (short i = 0; i < colorTbl.length; i++) colorTbl[i] = calcHSVColor(i); //optimal so far!
    }

    //-------------------------------------------------------------------------
    // Return the elevation
    // in m (meter) using the DEM
    //
    public static short getElev(float lat, float lon)
    {

        // *60 -> min * 2 -> 30 arcsec = 1/2 a min
        int y = (int) ((demTopLeftLat - lat) * 120) - y0;
        int x = (int) ((lon - demTopLeftLon) * 120) - x0;

        if ((x < 0) || (y < 0) || (x >= BUFX) || (y >= BUFY))
            return 0;
        else return buff[x][y];
    }

    //-------------------------------------------------------------------------
    // Utility function to calculate above ground altitude
    // in m (meter) using the DEM
    public static float calculateAgl(float lat, float lon, float alt)
    {
        if (demDataValid) return Math.max(0, alt - (int) (getElev(lat, lon)));
        else return 0;
    }


    public static DemColor getColor(short c)
    {
        if (c < MAX_ELEV)
            return colorTbl[c];
        else
            return colorTbl[MAX_ELEV-1];
    }

    //-----------------------------
    //color stuff - moved from renderer and renamed
    //
    private DemColor calcColor(short c)
    {
        float red;
        float blue;
        float green;

        final float r = 600; // Earth mean terrain elevation is 840m
        final float max = 0.5f;
        final float max_green = max * 0.587f;
        final float min_green = 0.2f;

        // elevated terrain
        red = 0f;
        blue = 0f;
        green = (float) c / r;
        if (green > max) {
            green = max;
            red = (c - 1 * r) / r;
            if (red > max) {
                red = max;
                blue = (c - 2 * r) / r;
                if (blue > max) {
                    blue = max;
                }
            }
        }
        else if (green == 0) {
            // assume ocean
            green = 0;
            red = 0;
            blue = 0.26f;
        }
        else if (green < min_green) {
            // beach, special case
            red = min_green - green;
            blue = min_green - green;
            green = min_green + min_green - green;
        }

        // HSV  allows adjustment hue, sat and val
        float[] hsv = {0, 0, 0};
        int colorBase = Color.rgb((int) (red * 255), (int) (green * 255), (int) (blue * 255));
        Color.colorToHSV(colorBase, hsv);

        if (hsv[2] > 0.25) {
            hsv[0] = hsv[0] - ((hsv[2] - 0.25f) * 60);  // adjust the hue max 15%,  hue 0..360
            hsv[2] = 0.25f; // clamp the value, val 0..1
        }
        int color = Color.HSVToColor(hsv);

        return new DemColor((float) Color.red(color) / 255, (float) Color.green(color) / 255, (float) Color.blue(color) / 255);
    }


    private static DemColor calcHSVColor(short c)
    {

        int r = 600;  // 600m=2000ft
        int MaxColor = 128;
        float[] hsv = {0, 0, 0};
        int colorBase;
        int min_v = 25;

        int v = MaxColor * c / r;

        if (v > 3 * MaxColor) {
            // mountain
            v %= MaxColor;
            colorBase = Color.rgb(MaxColor - v, MaxColor, MaxColor);
        }
        else if (v > 2 * MaxColor) {
            // highveld
            v %= MaxColor;
            colorBase = Color.rgb(MaxColor, MaxColor, v); // keep building to white
        }
        else if (v > MaxColor) {
            // inland
            v %= MaxColor;
            colorBase = Color.rgb(v, MaxColor, 0);
        }
        else if (v > 0) {
            // coastal plain
            if (v > min_v)
                colorBase = Color.rgb(0, v, 0);
            else {
                // close to the sea (lower than 25m),
                // is a special case otherwise it gets too dark
                colorBase = Color.rgb(min_v - v, min_v + (min_v - v), min_v - v);
            }
        }
        else {
            // the ocean
            colorBase = Color.rgb(0, 0, MaxColor); //bright blue ocean
        }

        // this allows us to adjust hue, sat and val
        Color.colorToHSV(colorBase, hsv);
        //hsv[0] = hsv[0];  // hue 0..360
        //hsv[1] = hsv[1];  // sat 0..1
        //hsv[2] = hsv[2];  // val 0..1

        if (hsv[2] > 0.25) {
            hsv[0] = hsv[0] - ((hsv[2] - 0.25f) * 60);  // adjust the hue max 15%,  hue 0..360
            hsv[2] = 0.30f; // clamp the value, val 0..1
        }

        /*
        On HSV model, H (hue) define the base color, S (saturation) control the amount of gray
        and V controls the brightness. So, if you enhance V and decrease S at same time, you gets
        more luminance
        */
        hsv[1] = hsv[1]/gamma;  // sat 0..1
        hsv[2] = hsv[2]*gamma;  // val 0..1

        int color = Color.HSVToColor(hsv);
        return new DemColor((float) Color.red(color) / 255, (float) Color.green(color) / 255, (float) Color.blue(color) / 255);
    }
    //-----------------------------


    public static void setBufferCenter(float lat, float lon)
    {
        lat0 = lat;
        lon0 = lon;
        x0 = (int) (Math.abs(lon0 - demTopLeftLon) * 60 * 2) - BUFX / 2;
        y0 = (int) (Math.abs(lat0 - demTopLeftLat) * 60 * 2) - BUFY / 2;
    }


    //-------------------------------------------------------------------------
    // use the lat lon to determine which tile is active
    //
    public static String setDEMRegionTile(float lat, float lon)
    {
        demTopLeftLat = 90 - (int) (90 - lat) / TILE_HEIGHT * TILE_HEIGHT;
        demTopLeftLon = -180 + (int) (lon + 180) / TILE_WIDTH * TILE_WIDTH;

        return String.format("%c%03d%c%02d",
                demTopLeftLon < 0 ? 'W' : 'E', (int) Math.abs(demTopLeftLon),
                demTopLeftLat < 0 ? 'S' : 'N', (int) Math.abs(demTopLeftLat));

    }

    //-------------------------------------------------------------------------
    // use the lat lon to determine which region file is active
    //
    public static String getRegionDatabaseName(float lat, float lon)
    {
        String sRegion = "null.null";

        // Everywhere except Antartica ( lat < -60)
        maxcol = 4800;
        maxrow = 6000;
        TILE_WIDTH = 40;     // width in degrees, must be integer
        TILE_HEIGHT = 50;    // height in degrees, must be integer

        if ((lat <= -10) && (lon > -20)
                && (lat > -60)) {
            sRegion = "zar.aus";
        }
        else if ((lat > 40) && (lon > -60)) {
            sRegion = "eur.rus";
        }
        else if ((lat > -10) && (lon <= -60)) {
            sRegion = "usa.can";
        }
        else if ((lat <= -10) && (lon <= -20)
                && (lat > -60)) {
            sRegion = "pan.arg";
        }
        else if ((lat <= 40) && (lon > -20)
              && (lat > -10) ) {
            sRegion = "sah.jap";
        }

        // 66°33'48.5" S = South Polar Circle
        // The non standard DEM size for Antartica is causing a lot of problems.
        // Needs special handling
        else if ((lat <= -60)) {
            sRegion = "ant.spl";
            //static final int maxcol = 4800;
            //static final int maxrow = 6000;
            //static final int TILE_WIDTH = 40;     // width in degrees, must be integer
            //static final int TILE_HEIGHT = 50;    // height in degrees, must be integer

            maxcol = 7200; // 4800 * 3 / 2 = 7,200
            maxrow = 3600;  // 51,840,000 bytes / 2 / 7200
            TILE_WIDTH = 60;     // width in degrees, must be integer
            TILE_HEIGHT = 30;    // height in degrees, must be integer
        }

        return sRegion;
    }


    //-------------------------------------------------------------------------
    // Fill the entire with a single value
    //
    private static void fillBuffer(short c)
    {
            for (int y = 0; y < BUFY; y++) {
                for (int x = 0; x < BUFX; x++) {
                    buff[x][y] = c;  // fill in the buffer
                }
            }
    }

    //-------------------------------------------------------------------------
    // Check if a lat lon in on the current tile
    //
    public static boolean isOnTile(float lat, float lon)
    {
        return (lat <= demTopLeftLat)
                && (lat > demTopLeftLat - TILE_HEIGHT)
                && (lon >= demTopLeftLon)
                && (lon < demTopLeftLon + TILE_WIDTH);
    }

    //-------------------------------------------------------------------------
    // Check if specific application is installed of not
    //
    private static boolean isAppInstalledOrNot(String uri)
    {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        }
        catch (PackageManager.NameNotFoundException ignored) {
        }
        return false;
    }

    private static int dummyAntSpl(float lat, float lon)
    {
        // Set the buffer to 3000m in the Artic circle
        if (lat < -66.6)
            fillBuffer((short) 3000);  // approx average elevation
        else
            fillBuffer((short) 0); // sea level

        demDataValid = true;
        return demLastError = DEM_OK;
    }


    /*
    error return codes:
     0 : OK
    -1: //b2-  cause bug: Toast.makeText(context, "DataPac (player.efis.data." + region + ") not installed.\nSynthetic vision not available",Toast.LENGTH_LONG).show();
    -2: //b2-  cause bug: Toast.makeText(context, "Terrain file error: " + region + "/" + DemFilename, Toast.LENGTH_LONG).show();
     */
    public final static int DEM_OK = 0;
    public final static int DEM_SYN_NOT_INSTALLED = -1;
    public final static int DEM_TERRAIN_ERROR = -2;

    public static int demLastError = DEM_OK;

    public static int loadDemBuffer(float lat, float lon)
    {
        demDataValid = false;

        // Automatic region determination with getRegionDatabaseName
        //   not 100% sure if this is such a good idea. It works but there
        //   are may be some some unintended behaviour. For now leave the code,
        //   but wpt.north.west disable the call here.
        region = getRegionDatabaseName(lat, lon);
        String DemFilename = setDEMRegionTile(lat, lon);
        setBufferCenter(lat, lon);

        // Temporary hardcode until we have a compatible Antartica DEM
        /*
        if (region == "ant.spl") {
            return dummyAntSpl(lat, lon);
        }
        */

        // Check to see if player.efis.data.nnn.mmm (datapac) is installed
        if (!isAppInstalledOrNot("player.efis.data." + region)) {
            fillBuffer((short) 0);
            demDataValid = false;
            return demLastError = DEM_SYN_NOT_INSTALLED;
        }

        if (UNavigation.isValidLatLon(lat, lon)) {
            fillBuffer((short) 0);

            try {
                // We have 3 possible mechnisms for data. Leave them commented out
                // here as a reference for later.
                /*
                // read from local directory "/data/ ...
                File storage = Environment.getExternalStorageDirectory();
                File file = new File(storage + "/data/player.efis.pfd/terrain/" + DemFilename + ".DEM");
                FileInputStream inp = new  FileInputStream(file);
                DataInputStream demFile = new DataInputStream(inp);
                //*/

                /*
                // read from local "assets"
                InputStream inp = context.getAssets().open("terrain/" + DemFilename + ".DEM");
                DataInputStream demFile = new DataInputStream(inp);
                //*/

                // read from a datapac "assets"
                Context otherContext = context.createPackageContext("player.efis.data." + region, 0);
                InputStream inp = otherContext.getAssets().open("terrain/" + DemFilename + ".DEM");
                DataInputStream demFile = new DataInputStream(inp);

                final int NUM_BYTES_IN_SHORT = 2;
                short c;
                int x, y;
                int x1, y1, x2, y2;

                // buffer is wholly in tile
                x1 = x0;
                x2 = x0 + BUFX;
                y1 = y0;
                y2 = y0 + BUFY;

                // Handle borders
                // Buffer west and north
                if (x0 < 0) x1 = 0;
                if (y0 < 0) y1 = 0;

                // Buffer east and south
                if (x0 + BUFX > maxcol) x2 = maxcol;
                if (y0 + BUFY > maxrow) y2 = maxrow;

                demFile.skipBytes(NUM_BYTES_IN_SHORT * (maxcol * y1));
                demFile.skipBytes(NUM_BYTES_IN_SHORT * (x1));
                for (y = y1; y < y2; y++) {
                    for (x = x1; x < x2; x++) {
                        c = demFile.readShort();
                        // deliberately avoid 0
                        if (c > 0) buff[x - x0][y - y0] = c;
                        else buff[x - x0][y - y0] = 0;
                    }
                    demFile.skipBytes(NUM_BYTES_IN_SHORT * (maxcol - x2));
                    demFile.skipBytes(NUM_BYTES_IN_SHORT * (x1));
                }
                demFile.close();
                demDataValid = true;
            }
            catch (PackageManager.NameNotFoundException e) {
                // thrown by: context.createPackageContext
                demDataValid = false;
                fillBuffer((short) 0);
                e.printStackTrace();
                // Try to fix the problem
                region = getRegionDatabaseName(lat, lon);
            }
            catch (IOException e) {
                // thrown by: otherContext.getAssets().open
                demDataValid = false;
                fillBuffer((short) 0);
                e.printStackTrace();
                return demLastError = DEM_TERRAIN_ERROR;

            }
            //catch (Exception e) { }
        }
        else {
            // Not a valid requested location
            demTopLeftLat = -9999;
            demTopLeftLon = -9999;
            x0 = -9999;
            y0 = -9999;
        }
        return demLastError = DEM_OK;
    }
}


/*
SRTM30

1.0 Introduction

SRTM30 is a near-global digital elevation model (DEM) comprising a combination of
data from the Shuttle Radar Topography Mission, flown in February, 2000 and the
U.S. Geological Survey's GTOPO30 data set. It can be considered to be either an
SRTM data set enhanced with GTOPO30, or as an upgrade to GTOPO30.

It is formatted and organized in a fashion that mimics the GTOPO30 convention so
software and GIS systems that work with GTOPO30 should also work with SRTM30.
GTOPO30 is described in the file 'GTOPO30 Documentation' included here, and this
SRTM document stresses the differences and additional files contained in SRTM30.
The GTOPO30 data set can be downloaded from http://edcdaac.usgs.gov/gtopo30/
gtopo30.html.

The SRTM data resulted from a collaborative effort by the National Aeronautics and
Space Administration (NASA) and the National Geospatial-Intelligence Agency (NGA,
formerly NIMA), as well as the participation of the German and Italian space
agencies, to generate a near-global digital elevation model (DEM) of the Earth using
radar interferometry. The SRTM instrument consisted of the Spaceborne Imaging
Radar-C (SIR-C) hardware set modified with a Space Station-derived mast and
additional antennae to form an interferometer with a 60 meter long baseline. A
description of the SRTM mission can be found in Farr and Kobrick (2000).

Synthetic aperture radars are side-looking instruments and acquire data along
continuous swaths. The SRTM swaths extended from about 30 degrees off-nadir to
about 58 degrees off-nadir from an altitude of 233 km, and thus were about 225 km
wide. During the data flight the instrument was operated at all times the orbiter was
over land and about 1000 individual swaths were acquired over the ten days of
mapping operations. Length of the acquired swaths range from a few hundred to
several thousand km. Each individual data acquisition is referred to as a "data take."

SRTM was the primary (and pretty much only) payload on the STS-99 mission of the
Space Shuttle Endeavour, which launched February 11, 2000 and flew for 11 days.
Following several hours for instrument deployment, activation and checkout,
systematic interferometric data were collected for 222.4 consecutive hours. The
instrument operated virtually flawlessly and imaged 99.96% of the targeted landmass
at least one time, 94.59% at least twice and about 50% at least three or more times.
The goal was to image each terrain segment at least twice from different angles (on
ascending, or north-going, and descending orbit passes) to fill in areas shadowed
from the radar beam by terrain.

This 'targeted landmass' consisted of all land between 56 degrees south and 60
degrees north latitude, which comprises almost exactly 80% of the total landmass.

1.1 Generation of SRTM30

SRTM radar echo data were processed into elevation information in a systematic
fashion using the SRTM Ground Data Processing System (GDPS) supercomputer
system at the Jet Propulsion Laboratory. Elevation data were mosaiced into more
than 14,000 one degree by one degree cells and formatted according to the Digital
Terrain Elevation Data (DTED) specification for delivery to NGA, who is using it to
update and extend their DTED products.

Sample spacing for the fundamental SRTM data set is 1 arc-second in latitude and
longitude (approximately 30 meters at the equator), consistent with NGA's existing
DTED Level 2 product. By agreement between NGA and NASA this product is under
control of NGA and is subject to limited distribution, using procedures similar to those
for the existing DTED products.

A second product with sample spacing of 3 arc-seconds was generated by a 3x3
averaging of the 1 arc-second data, and is publicly available through the U.S.
Geological Survey’s EROS Data Center. Data can be downloaded or ordered via the
‘seamless server’ at http://seamless.usgs.gov/, and ordered in NGA’s DTED format at
http://edc.usgs.gov/products/elevation/srtmdted.html. In addition, individual cells can
be downloaded via anonymous ftp from ftp://e0srp01u.ecs.nasa.gov. These 3 arcsecond
data were then further averaged 10x10 to produce 30 arc-second data
commensurate with GTOPO30.

The SRTM elevation data delivered to NGA were unedited, and they contain
occasional voids, or gaps, where the terrain lay in the radar beam's shadow or in
areas of extremely low radar backscatter where an elevation solution could not be
found. Globally these voids amount to no more than 0.15% of the SRTM data, but in
some regions such as the Himalayas or northern Africa they are extensive enough to
be evident even after the 10x10 averaging.

To construct SRTM30, mosaics were assembled at 30 arc-second spacing in tiles
that matched the GTOPO30 tiles. Then the results were combined with GTOPO30
such that each sample contains an SRTM data point where SRTM data were valid, or
GTOPO30 data where the SRTM data were void. Since the SRTM mission was only
able to map up to approximately 60.25 degrees north latitude values above this point
are completely from GTOPO30.

The geodetic reference for SRTM data is the WGS84 EGM96 geoid as documented
at http://earth-info.nga.mil/GandG/wgs84/gravitymod/index.htm, and no attempt was
made to adjust the vertical reference of either data set during the combination.

1.2 Updates for version 2.0

SRTM30 ver 1.1 was generated using unedited SRTM 3 arc-second sampled data,
known as SRTM3 ver. 1.1 These data were exactly as produced by the GDPS, and in
addition to some voids over land surfaces contained very noisy or void data for ocean
and lake surfaces caused by the very low radar reflectivity of undisturbed water. For
this reason the water mask contained in GTOPO30 was also used for SRTM30 ver.1.1.

Part of the data editing task undertaken by the NGA was a very careful delineation of
coastlines based on radar and optical imagery and other sources, as well as editing
to assure that lake and ocean surfaces were at constant elevation and that rivers
decreased in elevation in the downstream direction. This resulted in a considerably
improved water body definition, and this editing was incorporated in SRTM3 ver. 2.0.
These were also the data used to generate SRTM30 ver. 2.0, and these improved
coastline definitions were maintained. That means that instead of the -9999 flag for
oceans used by GTOPO30, SRTM30 ver. 2.0 shows the oceans at zero elevation and
other water bodies at the elevations determined during the editing.

2.0 Data Format

SRTM30 has been divided into the same tiles as GTOPO30, except that since the
data do not extend below 60 degrees south latitude the corresponding tiles, as well
as the Antarctica file in GTOPO30, have not been generated.

The following table lists the name, latitude and longitude extent, and elevation
statistics for each SRTM30 tile.

Latitude Longitude Elevation

Tile Minimum Maximum Minimum Maximum Minimum Maximum Mean Std.Dev.
w180n90 40 90 -180 -140 -6 6098 67 246
w140n90 40 90 -140 -100 -71 4635 378 563
w100n90 40 90 -100 -60 -18 2416 185 267
w060n90 40 90 -60 -20 -14 3940 520 924
w020n90 40 90 -20 20 -179 4536 93 266
e020n90 40 90 20 60 -188 5472 116 254
e060n90 40 90 60 100 -156 7169 340 618
e100n90 40 90 100 140 -110 3901 391 464
e140n90 40 90 140 180 -26 4578 415 401
w180n40 -10 40 -180 -140 -2 4120 1 34
w140n40 -10 40 -140 -100 -83 4228 198 554
w100n40 -10 40 -100 -60 -42 6543 139 414
w060n40 -10 40 -60 -20 -10 2503 29 94
w020n40 -10 40 -20 20 -139 3958 256 314
e020n40 -10 40 20 60 -415 5778 516 573
e060n40 -10 40 60 100 -46 8685 784 1534
e100n40 -10 40 100 140 -71 7213 236 625
e140n40 -10 40 140 180 -6 4650 14 144
w180s10 -60 -10 -180 -140 0 1784 0 7
w140s10 -60 -10 -140 -100 0 910 0 1
w100s10 -60 -10 -100 -60 -206 6813 262 814
w060s10 -60 -10 -60 -20 -61 2823 83 211
w020s10 -60 -10 -20 20 -12 2498 73 291
e020s10 -60 -10 20 60 -1 3408 186 417
e060s10 -60 -10 60 100 -4 2555 0 8
e100s10 -60 -10 100 140 -20 1360 64 145
e140s10 -60 -10 140 180 -43 3119 40 140

The 8 files included for each tile in GTOPO30 are also present in SRTM30, using the
following extensions:

Extension Contents
------------- ------------
DEM digital elevation model data
HDR header file for DEM
DMW world file
STX statistics file
PRJ projection information file
GIF shaded relief image
SRC source map
SCH header file for source map

In addition several additional files are included using these extensions:

Extension Contents
------------- ------------
DIF difference between SRTM30 and GTOPO30
JPG color coded shaded relief image
NUM number of valid point included in the 10x10 average
STD standard deviation of the elevations used in the average
Further information on the contents and format of the files is provided below.

2.1 DEM File (.DEM) - Same as GTOPO30

The DEM is provided as 16-bit signed integer data in a simple binary raster. There
are no header or trailer bytes imbedded in the image. The data are stored in row
major order (all the data for row 1, followed by all the data for row 2, etc.).

2.2 Header File (.HDR) - Same as GTOPO30

The DEM header file is an ASCII text file containing size and coordinate information
for the DEM. The following keywords are used in the header file:
BYTEORDER byte order in which image pixel values are stored
M = Motorola byte order (most significant byte first)
LAYOUT organization of the bands in the file
BIL = band interleaved by line (note: the DEM is a single
band image)
NROWS number of rows in the image
NCOLS number of columns in the image
NBANDS number of spectral bands in the image (1 for a DEM)
NBITS number of bits per pixel (16 for a DEM)
BANDROWBYTES number of bytes per band per row (twice the number of columns
for a 16-bit DEM)
TOTALROWBYTES total number of bytes of data per row (twice the number of
columns for a single band 16-bit DEM)
BANDGAPBYTES the number of bytes between bands in a BSQ format image
(0 for a DEM)
NODATA value used for masking purposes
ULXMAP longitude of the center of the upper-left pixel (decimal degrees)
ULYMAP latitude of the center of the upper-left pixel (decimal degrees)
XDIM x dimension of a pixel in geographic units (decimal degrees)
YDIM y dimension of a pixel in geographic units (decimal degrees)
Example header file (W100N40.HDR):
BYTEORDER M
LAYOUT BIL
NROWS 6000
NCOLS 4800
NBANDS 1
NBITS 16
BANDROWBYTES 9600
TOTALROWBYTES 9600
BANDGAPBYTES 0
NODATA -9999
ULXMAP -99.99583333333334
ULYMAP 39.99583333333333
XDIM 0.00833333333333
YDIM 0.00833333333333

2.3 World File (.DMW) - Same as GTOPO30

The world file is an ASCII text file containing coordinate information. It is used by
some packages for georeferencing of image data. The following is an example world
file (W100N40.DMW) with a description of each record:
0.00833333333333 x dimension of a pixel (decimal degrees)
0.00000000000000 rotation term (will always be zero)
0.00000000000000 rotation term (will always be zero)
-0.00833333333333 negative y dimension of a pixel (decimal degrees)
-99.99583333333334 longitude of the center of the upper-left pixel
39.99583333333333 latitude of the center of the upper-left pixel

2.4 Statistics File (.STX) - Same as GTOPO30

The statistics file is an ASCII text file which lists the band number, minimum value,
maximum value, mean value, and standard deviation of the values in the DEM data
file.
Example statistics file (W100N40.STX):
1 -42 6543 138.7 414.3

2.5 Projection File (.PRJ) - Same as GTOPO30

The projection information file is an ASCII text file which describes the projection of
the DEM and source map image.
Example projection file (W100N40.PRJ):
Projection GEOGRAPHIC
Datum WGS84
Zunits METERS
Units DD
Spheroid WGS84
Xshift 0.0000000000
Yshift 0.0000000000
Parameters

2.6 Shaded Relief Image (.GIF)

Same as for GTOPO30, except that brightness is also modulated by the elevation.
This is a actually a greyscale version of the .JPG file noted below.

2.7 Source Map (.SRC)

Same as for GTOPO30, except a new source code has been added for SRTM data.
The codes are now:
Value Source
------- ---------
0 Ocean
1 Digital Terrain Elevation Data
2 Digital Chart of the World
3 USGS 1-degree DEM's
4 Army Map Service 1:1,000,000-scale maps
5 International Map of the World 1:1,000,000-scale maps
6 Peru 1:1,000,000-scale map
7 New Zealand DEM
8 Antarctic Digital Database
9 SRTM data

2.8 Source Map Header File (.SCH) - Same as GTOPO30

The source map header file is an ASCII text file containing size and coordinate
information, similar to the DEM header file. The following keywords are used in the
source map header file:
BYTEORDER byte order in which image pixel values are stored
M = Motorola byte order (most significant byte first)
LAYOUT organization of the bands in the file
BIL = band interleaved by line (note: the source map is a single
band
image)
NROWS number of rows in the image
NCOLS number of columns in the image
NBANDS number of spectral bands in the image (1 for the source map)
NBITS number of bits per pixel (8 for the source map)
BANDROWBYTES number of bytes per band per row (the number of columns for
an
8-bit source map)
TOTALROWBYTES total number of bytes of data per row (the number of columns
for a
single band 8-bit source map)
BANDGAPBYTES the number of bytes between bands in a BSQ format image
(0 for the source map)
NODATA value used for masking purposes
ULXMAP longitude of the center of the upper-left pixel (decimal degrees)
ULYMAP latitude of the center of the upper-left pixel (decimal degrees)XDIM x dimension of a pixel in geographic units (decimal degrees)
YDIM y dimension of a pixel in geographic units (decimal degrees)
Example source map header file (W100N40.SCH):
BYTEORDER M
LAYOUT BIL
NROWS 6000
NCOLS 4800
NBANDS 1
NBITS 8
BANDROWBYTES 4800
TOTALROWBYTES 4800
BANDGAPBYTES 0
NODATA -9999
ULXMAP -99.99583333333334
ULYMAP 39.99583333333333
XDIM 0.00833333333333
YDIM 0.00833333333333

2.9 Difference file (.DIF)

16 bit signed integers indicating the difference between the SRTM30 DEMs and the
corresponding GTOPO30 tiles. Calculated as difference = SRTM30 value -
GTOPO30 value.

2.10 Color Shaded Relief Image (.JPG)

Color coded shaded relief image of the data in each file. Colors were assigned by
elevation, then manipulated to produce a pleasing image - thus they cannot be
related directly to elevation.

2.11 Number of Points in Average (.NUM)

8 bit integers indicating the number of valid data points that were included in the
10x10 averaging process.

2.12 Standard Deviation (.STD)

16 bit integers indicating the standard deviation of the data points used in the
averaging. This is thus an indication of topographic roughness useful in some
applications.

3.0 References

Farr, T.G., M. Kobrick, 2000, Shuttle Radar Topography Mission produces a wealth of
data, Amer. Geophys. Union Eos, v. 81, p. 583-585.

Rosen, P.A., S. Hensley, I.R. Joughin, F.K. Li, S.N. Madsen, E. Rodriguez, R.M.
Goldstein, 2000, Synthetic aperture radar interferometry, Proc. IEEE, v. 88, p.
333-382.
DMATR 8350.2, Dept. of Defense World Geodetic System 1984, Its Definition and
Relationship with Local Geodetic Systems, Third Edition, 4 July 1997.
http://earthinfo.nga.mil/GandG/wgs84/gravitymod/index.htm

Lemoine, F.G. et al, NASA/TP-1998-206861, The Development of the Joint NASA
GSFC and NIMA Geopotential Model EGM96, NASA Goddard Space Flight Center,
Greenbelt, MD 20771, U.S.A., July 1998.

Other Web sites of interest:

NASA/JPL SRTM: http://www.jpl.nasa.gov/srtm/
NGA: http://www.nga.mil/portal/site/nga01/
STS-99 Press Kit: http://www.shuttlepresskit.com/STS-99/index.htm
Johnson Space Center STS-99:
http://spaceflight.nasa.gov/shuttle/archives/sts-99/index.html
German Space Agency: http://www.dlr.de/srtm
Italian Space Agency: http://srtm.det.unifi.it/index.htm
U.S. Geological Survey, EROS Data Center: http://edc.usgs.gov/
Note: DTED is a trademark of the National Geospatial-Intelligence Agency

*/
