/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.common;

import android.content.Context;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.regex.Pattern;

import player.ulib.UNavigation;

public class Gpx {
    private static Context context = null;
    public static ArrayList<Apt> aptList = null;
    public static boolean bReady = false;

    public static void setContext(Context ctx) {
        context = ctx;
        aptList = new ArrayList<>();
    }

    public static String unAccent(String s) {
        //
        // JDK1.5
        //   use sun.text.Normalizer.normalize(s, Normalizer.DECOMP, 0);
        //
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    public static void loadDatabase(float lat, float lon, boolean dynamic) {
        bReady = false;
        if ((context != null) && (aptList != null)) {
            XmlPullParserFactory pullParserFactory;
            try {
                pullParserFactory = XmlPullParserFactory.newInstance();
                XmlPullParser parser = pullParserFactory.newPullParser();

                InputStream in_s = context.getAssets().open("waypoint/airport.gpx.xml");
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(in_s, null);
                parseXML(parser);
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bReady = true;

            // Remove waypoints from list the list. Call it the "cache" for historical reasons
            // Only keep the nearest 600 waypoints in the current hemisphere
            if (dynamic) {
                final int KEEP_MAX_WPTS = 600; // Cache size - Maximum number of nearest wpts to keep
                float range = UNavigation.EARTH_CIRC_2 / 2; //  1/4 the globe, ~ 5,400nm

                // World total #apts = 32,068
                // Remove all but the closest KEEP_MAX_WPTS airports
                while (aptList.size() > KEEP_MAX_WPTS) {
                    for (int i = aptList.size() - 1; i > 0; i--) {
                        Apt currApt = aptList.get(i);
                        if ((UNavigation.calcDme(lat, lon, currApt.lat, currApt.lon) > range)
                                && (!currApt.fixed)) {
                            aptList.remove(i);
                        }
                    }
                    range *= 0.922; // 85% of surface = sqrt(0.85) = 0.922
                }
                Log.v("kwik", "range: " + (int) range + " airports: " + aptList.size());
            }
        } else {
            throw new NullPointerException();
        }
    }

    private static void parseXML(XmlPullParser parser) throws XmlPullParserException, IOException {
        int eventType = parser.getEventType();
        Apt currentWpt = null;
        int ctr = 0;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String txt;
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    // To help avoid the ConcurrentModificationException
                    aptList.clear();
                    break;

                case XmlPullParser.START_TAG:
                    txt = parser.getName();
                    if (txt.equals("wpt")) {
                        currentWpt = new Apt();
                        if (parser.getAttributeCount() == 2) {
                            currentWpt.lat = Float.parseFloat(parser.getAttributeValue(0));
                            currentWpt.lon = Float.parseFloat(parser.getAttributeValue(1));
                        }
                    } else if (currentWpt != null) {
                        if (txt.equals("ele")) {
                            currentWpt.elev = Float.parseFloat(parser.nextText());
                        }
                        switch (txt) {
                            case "name":
                                currentWpt.name = parser.nextText();
                                break;
                            case "cmt":
                                currentWpt.cmt = unAccent(parser.nextText());
                                break;
                            case "fixed":
                                currentWpt.fixed = Boolean.parseBoolean(parser.nextText());
                                break;
                        }
                    }
                    break;

                case XmlPullParser.END_TAG:
                    txt = parser.getName();
                    // Only add non null wpt's that contain exactly 4 upper-case numbers or letters
                    if (txt.equalsIgnoreCase("wpt")
                            && currentWpt != null
                            && currentWpt.name.matches("[0-Z]+")) {

                        // handle any 3 letter UG airport codes
                        if (currentWpt.name.length() == 3) {
                            currentWpt.name += " ";
                            aptList.add(currentWpt);
                            ctr++;
                        } else if (currentWpt.name.length() == 4) {
                            aptList.add(currentWpt);
                            ctr++;
                        }
                    }
            }
            eventType = parser.next();
        }
        Log.v("kwik", "parsed: " + ctr + " airports: " + aptList.size());
        //printProducts(aptList); // only used for debugging
    }

    public static ArrayList<Apt> getAptSelect(float lat, float lon, int range, int nr) {
        ArrayList<Apt> nearestAptList = new ArrayList<>();

        for (Apt currProduct : aptList) {
            // add code to determine  the <nr> apts in range
            double deltaLat = lat - currProduct.lat;
            double deltaLon = lon - currProduct.lon;
            // double d =  Math.hypot(deltaLon, deltaLat);  // in degree, 1 deg = 60 nm
            double d = Math.sqrt(deltaLon * deltaLon + deltaLat * deltaLat);  // faster then hypot, see www

            if (d < range) {
                nearestAptList.add(currProduct);
            }
        }
        return nearestAptList;
    }

    private void printProducts(ArrayList<Apt> list) {
        String content = "";
        for (Apt currProduct : list) {
            content = content + "\nName :" + currProduct.name + "\n";
            content = content + "Cmt :" + currProduct.cmt + "\n";
            System.out.println(content);
            Log.v("kwik", "b2 - " + content);
        }
        //Log.v("b2", "b2 - " + content);
        //TextView display = (TextView)findViewById(R.id.info);
        //display.setText(content);
    }
}

