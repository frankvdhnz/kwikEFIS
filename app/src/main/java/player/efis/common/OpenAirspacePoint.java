package player.efis.common;

public class OpenAirspacePoint
{
    public final float lat;
    public final float lon;

    public OpenAirspacePoint(float lat, float lon)
    {
        this.lat = lat;
        this.lon = lon;
    }
}
