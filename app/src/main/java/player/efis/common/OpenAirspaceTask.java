/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.common;

import android.os.AsyncTask;

import java.util.concurrent.Semaphore;


public class OpenAirspaceTask extends AsyncTask<String, Void, Void>
{
    final float gps_lat;
    final float gps_lon;

    public OpenAirspaceTask(String id, float lat, float lon)
    {
        gps_lat = lat;
        gps_lon = lon;
    }

    protected Void doInBackground(String... urls)
    {
        mainExecutionLoop();
        return null;
    }

    static final Semaphore mutex = new Semaphore(1, true);
    private void mainExecutionLoop()
    {
        try {
            mutex.acquire();
            OpenAirspace.loadDatabase(gps_lat, gps_lon);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            mutex.release();
        }
    }
}


