/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.common;

import android.content.Context;

import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class OpenSky
{
    Context context;
    public static final LinkedList<String> trafficList = new LinkedList<>();  // the list
    private String id;
    static final Semaphore mutex = new Semaphore(1, true);
    public static float gps_lon;
    public static float gps_lat;

    public OpenSky()
    {
    }

    public OpenSky(Context context)
    {
        this.context = context;
    }

    public static LinkedList<String> getTargetList()
    {
        try {
            mutex.acquire();
            try {
                return new LinkedList<>(trafficList);
            }
            finally {
                mutex.release();
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    //-------------------------------------------------------------------------
    // Geographic coordinates (lat, lon)
    // in decimal degrees
    //
    public static void setLatLon(float lat, float lon)
    {
        gps_lat = lat;
        gps_lon = lon;
    }


}


/*
https://opensky-network.org/apidoc/rest.html

Index	Property	Type	Description
0	icao24	string	Unique ICAO 24-bit address of the transponder in hex string representation.
1	callsign	string	Callsign of the vehicle (8 chars). Can be null if no callsign has been received.
2	origin_country	string	Country name inferred from the ICAO 24-bit address.
3	time_position	int	Unix timestamp (seconds) for the last position update. Can be null if no position report was received by OpenSky within the past 15s.
4	last_contact	int	Unix timestamp (seconds) for the last update in general. This field is updated for any new, valid message received from the transponder.
5	longitude	float	WGS-84 longitude in decimal degrees. Can be null.
6	latitude	float	WGS-84 latitude in decimal degrees. Can be null.
7	baro_altitude	float	Barometric altitude in meters. Can be null.
8	on_ground	boolean	Boolean value which indicates if the position was retrieved from a surface position report.
9	velocity	float	Velocity over ground in m/s. Can be null.
10	true_track	float	True track in decimal degrees clockwise from north (north=0°). Can be null.
11	vertical_rate	float	Vertical rate in m/s. A positive value indicates that the airplane is climbing, a negative value indicates that it descends. Can be null.
12	sensors	int[]	IDs of the receivers which contributed to this state vector. Is null if no filtering for sensor was used in the request.
13	geo_altitude	float	Geometric altitude in meters. Can be null.
14	squawk	string	The transponder code aka Squawk. Can be null.
15	spi	boolean	Whether flight status indicates special purpose indicator.
16	position_source	int	Origin of this state’s position: 0 = ADS-B, 1 = ASTERIX, 2 = MLAT
*/



/*
{"time":1582524070,
        "states":
        [
        0        1        2          3          4          5         6               7     8      9        10   11       12    13    14
        ["7c6b2f","JST106","Australia",1582524070,1582524070,115.3819,-31.7377,5532.12,false,187.87,1.88,9.1,null,5768.34,"1372",false,0],
        ["7c1bee","VOZ9471 ","Australia",1582524069,1582524069,115.0588,-30.4632,9966.96,false,175.88,338.91,2.6,null,10393.68,"3234",false,0],
        ]
        }
*/

/*
{"time":1582524070,"states":
[
["7c6b2f","JST106","Australia",1582524070,1582524070,115.3819,-31.7377,5532.12,false,187.87,1.88,9.1,null,5768.34,"1372",false,0],
["7c1bee","VOZ9471 ","Australia",1582524069,1582524069,115.0588,-30.4632,9966.96,false,175.88,338.91,2.6,null,10393.68,"3234",false,0],
]
}
 */



