/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.common;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import player.ulib.Unit;

public class OpenSkyTask extends AsyncTask<String, Void, Void>
{
    protected static final int CONNECTED = 1;
    protected static final int CONNECTING = 2;
    protected static final int DISCONNECTED = 0;

    private boolean mRunning;

    public OpenSkyTask(String id)
    {
        //DatagramSocket mSocket;
        //private int mPort = 4000;
        int mState = DISCONNECTED;
    }

    protected Void doInBackground(String... urls)
    {
        mRunning = true;
        mainExecutionLoop();
        return null;
    }

    private void mainExecutionLoop()
    {
        BufferProcessor bp = new BufferProcessor();

        // Traffic
        //String buff = getHttp("https://opensky-network.org/api/states/all?lamin=-36&lomin=114&lamax=-30&lomax=119");

        String url = String.format("https://opensky-network.org/api/states/all?lamin=%f&lomin=%f&lamax=%f&lomax=%f",
                OpenSky.gps_lat - 1.0,
                OpenSky.gps_lon - 1.0,
                OpenSky.gps_lat + 1.0,
                OpenSky.gps_lon + 1.0);
        String buff = getHttp(url);

        if (!buff.isEmpty()) {
            try {
                OpenSky.mutex.acquire();
                LinkedList<String> objs = bp.decode();

                    /*-------------------------------------
                    // < debug - add ghost AC traffic

                    // fixed target
                    JSONObject object = new JSONObject();
                    try {
                        object.put("type", "traffic");
                        object.put("longitude", (double) 115.83);
                        object.put("latitude", (double) -31.80);
                        object.put("speed", (double) 123.0);
                        object.put("bearing", (double) 348.7);
                        //object.put("altitude", (double) Unit.Meter.toFeet(75));
                        object.put("altitude", 3000);
                        object.put("callsign", (String) "GHOST-0");
                        object.put("address", (int) 777);
                        object.put("time", (long) unixTime);

                        objs.add(object.toString());
                    }
                    catch (JSONException e1) {
                        continue;
                    }
                    // debug >
                    //-------------------------------------*/


                //JSON the buff to make the trafficList
                JSONObject object = new JSONObject();
                try {
                    JSONObject jObject;
                    jObject = new JSONObject(buff);
                    if (!jObject.get("states").equals(null)) {

                        JSONArray states = jObject.getJSONArray("states");

                        for (int i = 0; i < states.length(); i++) {
                            JSONArray entry = states.getJSONArray(i);

                            double altitude = Unit.Meter.toFeet((float)validedateDouble(entry.getString(7)));            // org in m
                            double speed =    Unit.MeterPerSecond.toKnots(validedateFloat(entry.getString(9)));    // org in m/s
                            //if (speed < 10) continue; // ignore ground targets
                            //if (speed == 0) continue;  // ignore stationary targets

                            object.put("type", "traffic");
                            object.put("address",     validedateLong(entry.getString(0)));
                            object.put("callsign",    entry.getString(1));
                            object.put("time", (long) validedateDouble(entry.getString(4)));
                            object.put("longitude",   validedateDouble(entry.getString(5)));
                            object.put("latitude",    validedateDouble(entry.getString(6)));
                            object.put("altitude",    Unit.Meter.toFeet(validedateFloat(entry.getString(7))));          // org in m
                            object.put("bearing",     validedateDouble(entry.getString(10)));
                            object.put("speed",       Unit.MeterPerSecond.toKnots((float)validedateDouble(entry.getString(9))));    // org in m/s

                            objs.add(object.toString());
                        }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

                // Extract traffic
                OpenSky.trafficList.clear(); // Delete whole list

                for (String s : objs) {
                    try {
                        JSONObject js = new JSONObject(s);
                        // "traffic" is hardcoded for OpenSkyNetwork. No need to check
                        OpenSky.trafficList.add(s);
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            finally {
                OpenSky.mutex.release();
            }
        }
    }

    // Safe numeric conversions - Deals with the "null"
    double validedateDouble(String s)
    {
        double d;
        try {
            d = Double.parseDouble(s);
        }
        catch (NumberFormatException e) {
            d = 0;
        }
        return d;
    }

    float validedateFloat(String s)
    {
        float d;
        try {
            d = Float.parseFloat(s);
        }
        catch (NumberFormatException e) {
            d = 0;
        }
        return d;
    }

    long validedateLong(String s)
    {
        long d;
        try {
            d = Long.parseLong(s, 16);
        }
        catch (NumberFormatException e) {
            d = 0;
        }
        return d;
    }


    private String getHttp(String addr)
    {
        return doHttp(addr, "GET");
    }

    private static String postHttp(String addr)
    {
        return doHttp(addr, "POST");
    }


    private static String doHttp(String addr, String method)
    {
        URL url;
        StringBuilder response = new StringBuilder();
        try {
            url = new URL(addr);
        }
        catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url");
        }

        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(false);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod(method); //"GET"
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
                throw new IOException("Post failed with error code " + status);
            }
            else {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.disconnect();
            }
            //Here is your json in string format
            return response.toString();
        }
    }


    public void finish()
    {
        mRunning = false;
    }

    protected boolean isRunning()
    {
        return mRunning;
    }

    protected boolean isStopped()
    {
        return !mRunning;
    }

}


/*
https://opensky-network.org/apidoc/rest.html

Index	Property	Type	Description
0	icao24	string	Unique ICAO 24-bit address of the transponder in hex string representation.
1	callsign	string	Callsign of the vehicle (8 chars). Can be null if no callsign has been received.
2	origin_country	string	Country name inferred from the ICAO 24-bit address.
3	time_position	int	Unix timestamp (seconds) for the last position update. Can be null if no position report was received by OpenSky within the past 15s.
4	last_contact	int	Unix timestamp (seconds) for the last update in general. This field is updated for any new, valid message received from the transponder.
5	longitude	float	WGS-84 longitude in decimal degrees. Can be null.
6	latitude	float	WGS-84 latitude in decimal degrees. Can be null.
7	baro_altitude	float	Barometric altitude in meters. Can be null.
8	on_ground	boolean	Boolean value which indicates if the position was retrieved from a surface position report.
9	velocity	float	Velocity over ground in m/s. Can be null.
10	true_track	float	True track in decimal degrees clockwise from north (north=0°). Can be null.
11	vertical_rate	float	Vertical rate in m/s. A positive value indicates that the airplane is climbing, a negative value indicates that it descends. Can be null.
12	sensors	int[]	IDs of the receivers which contributed to this state vector. Is null if no filtering for sensor was used in the request.
13	geo_altitude	float	Geometric altitude in meters. Can be null.
14	squawk	string	The transponder code aka Squawk. Can be null.
15	spi	boolean	Whether flight status indicates special purpose indicator.
16	position_source	int	Origin of this state’s position: 0 = ADS-B, 1 = ASTERIX, 2 = MLAT
*/



/*
{"time":1582524070,
        "states":
        [
        0        1        2          3          4          5         6               7     8      9        10   11       12    13    14
        ["7c6b2f","JST106","Australia",1582524070,1582524070,115.3819,-31.7377,5532.12,false,187.87,1.88,9.1,null,5768.34,"1372",false,0],
        ["7c1bee","VOZ9471 ","Australia",1582524069,1582524069,115.0588,-30.4632,9966.96,false,175.88,338.91,2.6,null,10393.68,"3234",false,0],
        ]
        }
*/

/*
{"time":1582524070,"states":
[
["7c6b2f","JST106","Australia",1582524070,1582524070,115.3819,-31.7377,5532.12,false,187.87,1.88,9.1,null,5768.34,"1372",false,0],
["7c1bee","VOZ9471 ","Australia",1582524069,1582524069,115.0588,-30.4632,9966.96,false,175.88,338.91,2.6,null,10393.68,"3234",false,0],
]
}
 */



