/*
 * Copyright (C) 2017 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.common;

// Standard imports
import android.content.Context;
import android.graphics.Bitmap;

import java.math.BigInteger;
import java.util.concurrent.Semaphore;

import player.ulib.MercatorInfo;
import player.ulib.UMath;
import player.ulib.UNavigation;


/*

a
 +------------------------------------+
 |            b                       |     a = demTopLeftLat,  demTopLeftLon
 |             +---------+            |     b = x0, y0
 |             |         |            |     c = lat0, lon0
 |             |  c +    |            |
 |             |         |            |
 |             +---------+            |
 |                                    |
 |             |< BUFX  >|            |
 |                                    |
 +------------------------------------+

 Note: BUFX should fit completely in the DEM tile
       Consider this when choosing size
*/

public class WxRadarMap
{
    public static boolean __DEBUG__ = false;  // Flag to enable debugging

    Context context;
    public static Bitmap mBitmap;  // the actual weather map.

    public static int zoomOWM;// = 0;  // 360 x 180
    public static float SPANLON; //= 360f / (float) Math.pow(2, zoomOWM);
    public static float SPANLAT; // = 180f / (float) Math.pow(2, zoomOWM);

    //static final int MAX_ELEV = 6000;  // in meters

    public static  int BUFX;// = 341; // 256; // for some reason the loaded bitmap is a different size?
    public static  int BUFY;// = 341; //256;

    //static DemColor colorTbl[] = new DemColor[MAX_ELEV];  // 600*3 = r*3

    static final float demTopLeftLat = +90;
    static final float demTopLeftLon = -180;

    static public float lat0;    // center of the BUFX tile
    static public float lon0;    // center of the BUFX tile

    //public static float gamma = 1;

    public static float gps_lon;
    public static float gps_lat;

    public static int bmTL_x;
    public static int bmTL_y;

    public static final String checksum = "f31cba1b06b4f63ac8ff5c298afac78e";

    private static final Semaphore mutex = new Semaphore(1, true);

    //-------------------------------------------------------------------------
    // Construct a new default loader with no flags set
    //
    /*public OpenWeatherMap()
    {
    }*/

    /*public OpenWeatherMap(Context context)
    {
        this.context = context;
        //setGamma(1);
    }*/

    public static String decode(String s)
    {
        StringBuilder buff = new StringBuilder(s);
        StringBuilder mask = new StringBuilder(s);

        for (char i = 0; i < 32; i++) {
            char c = Integer.toString(15 - i % 16, 16).charAt(0);
            mask.setCharAt(i, c);
            buff.setCharAt(i, '0');
        }
        BigInteger a = new BigInteger(s, 16);
        BigInteger b = new BigInteger(mask.toString(), 16);

        String r = a.xor(b).toString(16);
        return (buff + r).substring(r.length()) ;
    }


    //-------------------------------------------------------------------------
    // Geographic coordinates (lat, lon)
    // in decimal degrees
    //
    public static void setLatLon(float lat, float lon)
    {
        gps_lat = lat;
        gps_lon = lon;
    }


    public static void setBitmap(Bitmap bm)
    {
        try {
            mutex.acquire();

            mBitmap = bm;

            if (mBitmap != null) {
                BUFX = mBitmap.getWidth();
                BUFY = mBitmap.getHeight();
            }
            else {
                BUFX = 0;
                BUFY = 0;

                SPANLON = 0;
                SPANLAT = 0;
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            mutex.release();
        }
    }


    // Google Spherical Mercator
    static final float SPHERICAL_MERCATOR_MAX_LAT =  85.051129f;
    static final float SPHERICAL_MERCATOR_MAX_LON = 180.000000f;
    public static void setzoomOWM(int z)
    {
        int numTiles = (int) Math.pow(2, z);
        zoomOWM = z;

        SPANLON = 2 * SPHERICAL_MERCATOR_MAX_LON / numTiles;  // 22.5 for 4
        SPANLAT = 4 * SPHERICAL_MERCATOR_MAX_LAT / numTiles;  // 21.262783 for 4

        MercatorInfo p = UNavigation.getMercatorTileInfo(gps_lat, gps_lon, z);
        bmTL_x = (int)p.tileCoordinate.x;
        bmTL_y = (int)p.tileCoordinate.y;

        float clon = (float) (p.east + p.west) / 2.0f;
        float clat = (float) (p.south + p.north) / 2.0f;

        setBufferCenter(clat, clon);  // wx_200: 67,5, -135
        setDEMRegionTile(clat, clon);  //?? remove todo
    }


    /*public static void setGamma(float g)
    {
        gamma = g;

        //for (short i = 0; i < colorTbl.length; i++) colorTbl[i] = calcColor(i);
        for (short i = 0; i < colorTbl.length; i++) colorTbl[i] = calcHSVColor(i); //optimal so far!
    }*/


    static public int getDbz(float lat, float lon)
    {
        int dbz;

        if (mBitmap == null)
            return 0;

        // Note this is also where the 2x difference between lat and lon is handled

        int x =  BUFX/2 + UMath.trunc((lon - lon0) * BUFX / SPANLON);
        int y =  BUFY/2 - UMath.trunc((lat - lat0) * BUFY / SPANLAT);

        if ((x < 0) || (y < 0) || (x >= BUFX) || (y >= BUFY))
            return 0;
        else
            dbz = mBitmap.getPixel(x, y);

        return dbz;
    }

    //-----------------------------
    public static void setBufferCenter(float lat, float lon)
    {
        lat0 = lat;
        lon0 = lon;

        // x0 and y0 are in pixels
        int x0 = UMath.trunc(Math.abs(lon0 - demTopLeftLon) * BUFX / SPANLON) - BUFX / 2;   // 1 xwxpixel = BUFX / SPANLON
        int y0 = UMath.trunc(Math.abs(lat0 - demTopLeftLat) * BUFY / SPANLAT) - BUFY / 2;   // 1 ywxpixel = BUFY / SPANLAT
    }

    // Note - we can refactor out demTopLeftLon and Lat later.


    //-------------------------------------------------------------------------
    // use the lat lon to determine which tile is active
    //
    public static String setDEMRegionTile(float lat, float lon)
    {

        return String.format("%c%03d%c%02d",
                demTopLeftLon < 0 ? 'W' : 'E', (int) Math.abs(demTopLeftLon),
                demTopLeftLat < 0 ? 'S' : 'N', (int) Math.abs(demTopLeftLat));
    }

}


/*
Tiles:
Zoom 0 = 0,0      360  x 180
Zoom 1 = 0 -> 1   180  x 90 deg
Zoom 2 = 0 -> 3   90   x 45 deg
Zoom 3 = 0 -> 7   45   x 22.5 deg
Zoom 4 = 0 -> 15  22.5 x 11.25 deg
Zoom 5 = 0 -> 31  11.25 x 5.625 deg

Zoom 10 = 0 -> 1023
Bitmap is 256 x 256
https://tile.openweathermap.org/map/precipitation_new/4/15/7.png?appid=0dc0008370e0c42a3623e6b1fcaef59e
*/
