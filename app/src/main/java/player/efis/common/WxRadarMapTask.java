/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Semaphore;


public class WxRadarMapTask extends AsyncTask<String, Void, Void>
{

    public WxRadarMapTask(String id)
    {
    }

    protected Void doInBackground(String... urls)
    {
        mainExecutionLoop();
        return null;
    }

    /*protected void onPostExecute()
    {
        // TODO: check this.exception
        // TODO: do something with the feed
    }*/


    static final Semaphore mutex = new Semaphore(1, true);

    private static void doSleep(int ms)
    {
        // Wait ms milliseconds
        try {
            Thread.sleep(ms);
        }
        catch (Exception ignored) {
        }
    }


    private void mainExecutionLoop()
    {
        Bitmap bm;

        //
        // Try for a connection until a valid bitmap is received
        // then update periodically (15 minutes)
        //
        //while (mRunning == true) {
        {
            // Weather
            try {
                mutex.acquire();

                if (WxRadarMap.gps_lat + WxRadarMap.gps_lon != 0) {
                    String sc = WxRadarMap.decode(WxRadarMap.checksum);

                    // 2 ~ practical minimum.
                    // 4 ~ good general use.
                    // 5 ~ max DEM
                    // 6 ~ 50% DEM
                    // 7 ~ good for quilting

                    /*String url = String.format("https://tile.openweathermap.org/map/precipitation_new/%d/%d/%d.png?appid=%s",
                            OpenWeatherMap.zoomOWM,
                            OpenWeatherMap.bmTL_x,  //x
                            OpenWeatherMap.bmTL_y,  //y
                            sc);  //y
                    */

                    long unixTime = System.currentTimeMillis() / 1000L;
                    long timeStamp = (long) ((double)unixTime/(double)(10*60)) * (10*60) ;

                    // For a specific time
                    String url = String.format("https://tilecache.rainviewer.com/v2/radar/%d/%d/%d/%d/%d/%d/1_1.png",
                            timeStamp,
                            256,                // 512 or 256, 256 is Google standard
                            WxRadarMap.zoomOWM, // Zoom, normally 5
                            WxRadarMap.bmTL_x,  //x
                            WxRadarMap.bmTL_y,  //y
                            4                   // color 4=TWC, 6=SELEX-SI
                            );

                    Log.v("kwik", "Read " + url);

                    if (WxRadarMap.__DEBUG__) bm = WxRadarMap.mBitmap;  // use for debugging
                    else bm = getBitmapFromURL(url);                    // comment this line for debugging

                    if (bm != null) {
                        WxRadarMap.setBitmap(bm);
                    }

                    // Quilt code
                    /*
                    // top
                    url = String.format("https://tile.openweathermap.org/map/precipitation_new/%d/%d/%d.png?appid=%s",
                            OpenWeatherMap.zoomOWM,
                            OpenWeatherMap.bmTL_x,  //x
                            OpenWeatherMap.bmTL_y);  //y
                    Bitmap bm_b = getBitmapFromURL(url);

                    url = String.format("https://tile.openweathermap.org/map/precipitation_new/%d/%d/%d.png?appid=%s",
                            OpenWeatherMap.zoomOWM,
                            OpenWeatherMap.bmTL_x,  //x
                            OpenWeatherMap.bmTL_y-1);  //y
                    Bitmap bm_a = getBitmapFromURL(url);

                    bm = appendBitmap(bm_a, bm_b, false); //vertical

                    //OpenWeatherMap.setzoomOWM(7-1);  // also calculates SPANLON and SPANLON
                    OpenWeatherMap.setBitmap(bm);
                    */
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            finally {
                // wait
                //if (bm == null) doSleep(30*1000);  // sleep 30 seconds
                //else doSleep(15*60*1000);          // sleep 15 minutes

                mutex.release();
            }
        }
    }


    public Bitmap appendBitmap(Bitmap fr, Bitmap sc, boolean horizontal)
    {
        Bitmap comboBitmap = null;
        int width, height;

        if ((fr != null) &&  (sc != null )) {
            if (horizontal) {
                width = fr.getWidth() + sc.getWidth();
                height = fr.getHeight();

                comboBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas comboImage = new Canvas(comboBitmap);
                comboImage.drawBitmap(fr, 0f, 0f, null);

                comboImage.drawBitmap(sc, fr.getWidth(), 0f, null);
            }
            else {
                width = fr.getWidth() ;
                height = fr.getHeight() + sc.getHeight() ;

                comboBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas comboImage = new Canvas(comboBitmap);
                comboImage.drawBitmap(fr, 0f, 0f, null);

                comboImage.drawBitmap(sc, 0f, fr.getHeight(), null);
            }
        }
        return comboBitmap;
    }


    private String getHttp(String addr)
    {
        return doHttp(addr, "GET");
    }

    private static String postHttp(String addr)
    {
        return doHttp(addr, "POST");
    }


    private static String doHttp(String addr, String method)
    {
        URL url;
        StringBuilder response = new StringBuilder();
        try {
            url = new URL(addr);
        }
        catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url");
        }

        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(false);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod(method); //"GET"
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
                throw new IOException("Post failed with error code " + status);
            }
            else {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.disconnect();
            }
            //Here is your json in string format
            return response.toString();
        }
    }

    public static Bitmap getBitmapFromURL(String src)
    {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
}
