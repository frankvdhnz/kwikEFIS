/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package player.efis.common;

public enum module_t
{
	PFD,
	MFD,
	CFD;

  public static module_t fromInteger(int i)
  {
      switch (i) {
        case 1:
            return PFD;
        case 2:
            return MFD;
        case 3:
            return CFD;
        default:
          return null;  
      }
  }

    public static int toInteger(module_t m)
    {
        if (m == null) {
            return 0;
        }

        switch (m) {
            case PFD:
                return 1;
            case MFD:
                return 2;
            case CFD:
                return 3;
            default:
                return 0;
        }
    }

}
