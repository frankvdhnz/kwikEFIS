/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.pfd;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.LinkedList;

import player.efis.common.AircraftData;
import player.efis.common.Apt;
import player.efis.common.DemColor;
import player.efis.common.DemGTOPO30;
import player.efis.common.Gpx;
import player.efis.common.Point;
import player.efis.common.prefs_t;
import player.gles20.GLText;
import player.gles20.Line;
import player.gles20.PolyLine;
import player.gles20.Polygon;
import player.gles20.Square;
import player.gles20.Triangle;
import player.ulib.Coordinate;
import player.ulib.UMath;
import player.ulib.UNavigation;
import player.ulib.UTrig;
import player.ulib.Unit;


abstract public class EFISRenderer
{
    private static final String TAG = "EFISRenderer";

    protected float DemIncAngle = 5f;      // incremental delta of the arc

    protected Triangle mTriangle;
    protected Square mSquare;
    protected Line mLine;
    protected PolyLine mPolyLine;
    protected Polygon mPolygon;

    // GLText Instances
    // 2% size equi 2
    protected GLText glText3;
    protected GLText glText4;
    protected GLText glText5;
    protected GLText glText6;
    protected GLText glText7;
    protected GLText glText8;
    protected GLText glText9;
    //protected GLText glText10;
    //protected GLText glText18;


    // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
    protected final float[] mMVPMatrix = new float[16];
    protected final float[] mProjectionMatrix = new float[16];
    protected final float[] mViewMatrix = new float[16];
    protected final float[] mRotationMatrix = new float[16];
    protected final float[] mFdRotationMatrix = new float[16];  // for Flight Director
    protected final float[] mRmiRotationMatrix = new float[16]; // for RMI / Compass Rose

    // Terrain drawing
    private float mAngle;
    protected final float MAX_ZOOM = 120;
    protected final float MIN_ZOOM = 5; // for debug =1 // normal = 5;

    // OpenGL
    protected int pixW;
    protected int pixH;               // Width & Height of window in pixels
    protected int pixW2;
    protected int pixH2;              // Half Width & Height of window in pixels
    protected int pixM;               // The smallest dimension of pixH or pixM
    protected int pixM2;              // The smallest dimension of pixH2 or pixM2
    protected float zfloat;           // A Z to use for layering of ortho projected markings*/

    // Artificial Horizon
    protected float pitchInView;      // The degrees pitch to display above and below the lubber line
    public float pitchTranslation;    // Pitch amplified by 1/2 window pixels for use by glTranslate
    protected float rollRotation;     // Roll converted for glRotate
    // Airspeed Indicator
    protected float IASInView;        // The indicated units to display above the center line

    //private int   IASValue;         // Indicated Airspeed
    protected float IASValue;         // Indicated Airspeed, in knots
    protected float IASTranslation;   // Value amplified by 1/2 window pixels for use by glTranslate

    // The following should be read from a calibration file by an init routine
    private final int IASMaxDisp;           // The highest speed to show on tape

    // Altimeter
    protected float MSLInView;        // The indicated units to display above the center line
    protected int MSLValue;           // Altitude above mean sea level, MSL in feet
    protected float MSLTranslation;   // Value amplified by 1/2 window pixels for use by glTranslate
    public int AGLValue;              // Altitude above ground, AGL in feet

    // The following should be read from a calibration file by an init routine
    protected final int MSLMinDisp;         // The lowest altitude to show on tape
    protected final int MSLMaxDisp;         // The highest altitude to show on tape

    // VSI
    private final float VSIInView;          // Vertical speed to display above the centerline
    protected int VSIValue;           // Vertical speed in feet/minute
    private float VSINeedleAngle;     // The angle to set the VSI needle

    //DI
    private float DIInView;           // The indicated units to display above the center line
    protected float DIValue;          // Direction Indicator / Compass, in degrees
    private float SlipValue;          // was int
    private float BatteryPct;         // Battery usage
    private float GForceValue;        // G force
    private float GSValue;
    protected float ROTValue;         // Rate Of Turn in deg per sec
    private float DITranslation;      // Value amplified by 1/2 window pixels for use by glTranslate

    // Geographic Coordinates
    protected float LatValue;        // Latitude, in degrees
    protected float LonValue;        // Longitude, in degrees

    //FPV - Flight Path Vector
    private float fpvX;              // Flight Path Vector X
    private float fpvY;              // Flight Path Vector Y

    //Flight Director
    protected float FDTranslation;           // = -6 / pitchInView  * pixM2;  // command 6 deg pitch up
    protected float FDRotation;              // = 20;  // command 20 deg roll

    // Onscreen elements
    protected boolean displayInfoPage;       // Display The Ancillary Information
    protected boolean displayFlightDirector; // Display Flight Director
    protected boolean displayRMI;            // Display RMI
    protected boolean displayHITS;           // Display the Highway In The Sky
    protected boolean displayDEM;            // Display the DEM terrain
    protected boolean displayWX;             // Display the Precip Weather
    protected boolean displayCCIP;           // Display the CCIP
    protected boolean autoZoomActive;

    // 3D map display
    protected boolean displayAirport = true;
    protected boolean displayAirspace;
    protected boolean displayAHColors;
    protected boolean displayTape;
    public boolean displayMirror;
    protected boolean displayFPV;

    protected boolean ServiceableDevice;  // Flag to indicate no faults
    protected boolean ServiceableAh;      // Flag to indicate AH failure
    protected boolean ServiceableAlt;     // Flag to indicate Altimeter failure
    protected boolean ServiceableAsi;     // Flag to indicate Airspeed failure
    protected boolean ServiceableDi;      // Flag to indicate DI failure
    protected boolean ServiceableRose;    // Flag to indicate Rose failure

    protected boolean ServiceableMap;      // Flag to indicate AH failure

    protected boolean bBannerActive;      // Banner message
    private String sBannerMsg;            // Flag to control banner display

    protected float portraitOffset = 0.40f;  // the magic number for portrait offset

    //Demo Modes
    protected boolean bSimulatorActive;
    private String sDemoMsg;

    protected final Context context;    // Context (from Activity)

    // Colors
    protected float tapeShadeR = 0.600f; // grey
    protected float tapeShadeG = 0.600f; // grey
    protected float tapeShadeB = 0.600f; // grey

    protected float foreShadeR = 0.999f; // white
    protected float foreShadeG = 0.999f; // white
    protected float foreShadeB = 0.999f; // white

    protected float backShadeR = 0.001f; // black
    protected float backShadeG = 0.001f; // black
    protected float backShadeB = 0.001f; // black

    protected int colorTheme;

    private float gamma = 1;     // gamma is the overall lightness / saturation
    protected float theta = 1;   // theta is the adjustment for the high contrast scheme

    public boolean proximityAlert;  // traffic alert

    protected final float wxDim = 0.55f;  // Alpha for the Weather dimming boxes


    public enum layout_t
    {
        PORTRAIT,
        LANDSCAPE
    }

    public layout_t Layout = layout_t.LANDSCAPE;

    public EFISRenderer(Context context)
    {
        super();
        this.context = context;      // Save Specified Context

        // Initialisation of variables
        pitchTranslation = rollRotation = 0; // default object translation and rotation

        IASTranslation = 0;  // default IAS tape translation
        IASValue = 0;        // The default to show if no IAS calls come in
        MSLTranslation = 0;  // default MSL tape translation
        MSLValue = 0;        // The default to show if no MSL calls come in
        VSIValue = 0;        // The default vertical speed

        IASMaxDisp = 200;
        MSLMinDisp = -1000;
        MSLMaxDisp = 20000;

        VSIInView = 2000;
        displayFPV = true;

        //LinkedList<String> objs = new LinkedList<String>();

    }

    int FrameCounter = 0;
    protected final float[] scratch1 = new float[16];  // moved to class scope
    protected final float[] scratch2 = new float[16];  // moved to class scope
    protected final float[] altMatrix = new float[16]; // moved to class scope
    protected final float[] iasMatrix = new float[16]; // moved to class scope
    protected final float[] fdMatrix = new float[16];  // moved to class scope
    protected final float[] rmiMatrix = new float[16]; // moved to class scope

    //-------------------------------------------------------------------------
    // setSpinnerParams must be implemented in the child classes
    //
    abstract public void setSpinnerParams();


    //-------------------------------------------------------------------------
    // Utility method for debugging OpenGL calls. Provide the name of the call
    // just after making it:
    //
    // mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
    // MyGLRenderer.checkGlError("glGetUniformLocation");
    //
    // If the operation is not successful, the check throws an error.
    //
    // @param glOperation - Name of the OpenGL call to check.
    //
    public static void checkGlError(String glOperation)
    {
        int error;
        if ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, glOperation + ": glError " + error);
            throw new RuntimeException(glOperation + ": glError " + error);
        }
    }

    //-------------------------------------------------------------------------
    //  Returns the rotation angle of the triangle shape (mTriangle).
    //
    public float getAngle()
    {
        return mAngle;
    }

    //-------------------------------------------------------------------------
    //  Sets the rotation angle of the triangle shape (mTriangle).
    //
    public void setAngle(float angle)
    {
        mAngle = angle;
    }

    protected void renderBannerMsg(float[] matrix)
    {
        String s = sBannerMsg;
        glText5.begin(1.0f, 0f, 0f, 1.0f, matrix); // Red
        glText5.setScale(2);
        glText5.drawCX(s, 0, pixM2/2);
        glText5.setScale(1);
        glText5.end();
    }

    public void setBannerMsg(boolean cal, String msg)
    {
        bBannerActive = cal;
        sBannerMsg = msg;
    }

    protected void renderSimulatorActive(float[] matrix)
    {
        String s = sDemoMsg;
        glText6.begin(1.0f, 0f, 0f, 0.125f, matrix); // Red
        glText6.setScale(3);
        glText6.drawCX(s, 0, 0);
        glText6.setScale(1);
        glText6.end();
    }

    public void setSimulatorActive(boolean status, String msg)
    {
        bSimulatorActive = status;
        sDemoMsg = msg;
    }

    //-------------------------------------------------------------------------
    // Flight Director
    //

    //        mTriangle.SetColor(foreShadeR, foreShadeG, 0/*backShadeB*/, 1); //light yellow

    protected final float PPD_DIV = 30; // for landscape

    protected void renderFlightDirector(float[] matrix)
    {
        float z, pixPerDegree;

        z = zfloat;
        pixPerDegree = pixM2 / PPD_DIV;

        // fwd triangles
        mTriangle.SetWidth(1);
        if (colorTheme == 2) mTriangle.SetColor(0, foreShadeG, 0, 1);  // light green
        else mTriangle.SetColor(theta, theta * 0.5f, theta, 1);  //purple
        mTriangle.SetVerts(0.0f * pixPerDegree, 0.0f * pixPerDegree, z,
                10.0f * pixPerDegree, -3.0f * pixPerDegree, z,
                12.0f * pixPerDegree, -2.0f * pixPerDegree, z);
        mTriangle.draw(matrix);
        mTriangle.SetVerts(0.0f * pixPerDegree, 0.0f * pixPerDegree, z,
                -12.0f * pixPerDegree, -2.0f * pixPerDegree, z,
                -10.0f * pixPerDegree, -3.0f * pixPerDegree, z);
        mTriangle.draw(matrix);

        // rear triangles
        if (colorTheme == 2) mTriangle.SetColor(0, tapeShadeG, 0, 1);  // light green
        else mTriangle.SetColor(theta * 0.6f, theta * 0.3f, theta * 0.6f, 1);  //purple'ish
        mTriangle.SetVerts(10.0f * pixPerDegree, -3.0f * pixPerDegree, z,
                12.0f * pixPerDegree, -2.0f * pixPerDegree, z,
                12.0f * pixPerDegree, -3.0f * pixPerDegree, z);
        mTriangle.draw(matrix);
        mTriangle.SetVerts(-12.0f * pixPerDegree, -2.0f * pixPerDegree, z,
                -10.0f * pixPerDegree, -3.0f * pixPerDegree, z,
                -12.0f * pixPerDegree, -3.0f * pixPerDegree, z);
        mTriangle.draw(matrix);
    }

    public void setFlightDirector(boolean active, float pit, float rol)
    {
        displayFlightDirector = active;
        FDTranslation = -pit / pitchInView * pixM2;  // pit = 6, command 6 deg pitch up
        FDRotation = -rol; // rol = 20, command 20 deg roll
    }


    //-------------------------------------------------------------------------
    // Attitude Indicator
    //
    protected void renderFixedHorizonMarkers()
    {
        int i;
        float z, pixPerDegree, sinI, cosI;
        float _sinI, _cosI;

        z = zfloat;
        pixPerDegree = pixM2 / PPD_DIV;

        // We might make this configurable in future
        // for now force it to false
        if (false) {
            // The lubber line - W style
            mPolyLine.SetColor(1, 1, 0, 1);
            mPolyLine.SetWidth(6);

            float[] vertPoly = {
                    // in counter clockwise order:
                    -6.0f * pixPerDegree, 0.0f, z,
                    -4.0f * pixPerDegree, 0.0f, z,
                    -2.0f * pixPerDegree, -2.0f * pixPerDegree, z,
                    0.0f * pixPerDegree, 0.0f, z,
                    2.0f * pixPerDegree, -2.0f * pixPerDegree, z,
                    4.0f * pixPerDegree, 0.0f, z,
                    6.0f * pixPerDegree, 0.0f, z
            };
            mPolyLine.VertexCount = 7;
            mPolyLine.SetVerts(vertPoly);
            mPolyLine.draw(mMVPMatrix);
        }
        else {
            // The lubber line - Flight Director style
            // side lines
            int B2 = 3;
            mLine.SetWidth(2 * B2);

            if (colorTheme == 2) mLine.SetColor(0, foreShadeG, 0, 1);  // light green
            else mLine.SetColor(1, 1, 0/*backShadeB*/, 1);  // hardcoded light yellow

            mLine.SetVerts(11.0f * pixPerDegree, B2, z,
                    15.0f * pixPerDegree, B2, z);
            mLine.draw(mMVPMatrix);
            mLine.SetVerts(-11.0f * pixPerDegree, B2, z,
                    -15.0f * pixPerDegree, B2, z);
            mLine.draw(mMVPMatrix);

            if (colorTheme == 2) mLine.SetColor(0, tapeShadeG, 0, 1);  // dark green
            else mLine.SetColor(tapeShadeR, tapeShadeG, 0, 1);  // dark yellow
            mLine.SetVerts(11.0f * pixPerDegree, -B2, z,
                    15.0f * pixPerDegree, -B2, z);
            mLine.draw(mMVPMatrix);
            mLine.SetVerts(-11.0f * pixPerDegree, -B2, z,
                    -15.0f * pixPerDegree, -B2, z);
            mLine.draw(mMVPMatrix);

            // outer triangles
            mTriangle.SetWidth(1);
            if (colorTheme == 2) mTriangle.SetColor(0, foreShadeG, 0, 1);  // light green
            else mTriangle.SetColor(1, 1, 0, 1); //hardcoded light yellow

            mTriangle.SetVerts(0.0f * pixPerDegree, 0.0f * pixPerDegree, z,
                    6.0f * pixPerDegree, -3.0f * pixPerDegree, z,
                    10.0f * pixPerDegree, -3.0f * pixPerDegree, z);
            mTriangle.draw(mMVPMatrix);
            mTriangle.SetVerts(0.0f * pixPerDegree, 0.0f * pixPerDegree, z,
                    -10.0f * pixPerDegree, -3.0f * pixPerDegree, z,
                    -6.0f * pixPerDegree, -3.0f * pixPerDegree, z);
            mTriangle.draw(mMVPMatrix);

            // inner triangle
            if (colorTheme == 2) mTriangle.SetColor(0, tapeShadeG, 0, 1);  // light green
            else mTriangle.SetColor(0.6f, 0.6f, 0, 1); //hardcoded dark yellow
            mTriangle.SetVerts(0.0f * pixPerDegree, 0.0f * pixPerDegree, z,
                    4.0f * pixPerDegree, -3.0f * pixPerDegree, z,
                    6.0f * pixPerDegree, -3.0f * pixPerDegree, z);
            mTriangle.draw(mMVPMatrix);
            mTriangle.SetVerts(0.0f * pixPerDegree, 0.0f * pixPerDegree, z,
                    -6.0f * pixPerDegree, -3.0f * pixPerDegree, z,
                    -4.0f * pixPerDegree, -3.0f * pixPerDegree, z);
            mTriangle.draw(mMVPMatrix);

            // Center Triangle - Optional
            //mTriangle.SetVerts(0.0f * pixPerDegree,  0.0f * pixPerDegree, z,
            //		-4.0f * pixPerDegree, -2.0f * pixPerDegree, z,
            //		 4.0f * pixPerDegree, -2.0f * pixPerDegree, z);
            //mTriangle.draw(mMVPMatrix);
        }

        // The fixed roll marker (roll circle marker radius is 15 degrees of pitch, with fixed markers on the outside)
        mTriangle.SetColor(foreShadeR, foreShadeG, 0.0f, 1); //yellow
        mTriangle.SetVerts(0.035f * pixM2, 16.5f * pixPerDegree, z,
                -0.035f * pixM2, 16.5f * pixPerDegree, z,
                0.0f, 15f * pixPerDegree, z);
        mTriangle.draw(mMVPMatrix);

        mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // grey
        mLine.SetWidth(2);
        // The lines
        for (i = 10; i <= 30; i = i + 10) {
            sinI = UTrig.isin(i);
            cosI = UTrig.icos(i);
            mLine.SetVerts(
                    15 * pixPerDegree * sinI, 15 * pixPerDegree * cosI, z,
                    16 * pixPerDegree * sinI, 16 * pixPerDegree * cosI, z
            );
            mLine.draw(mMVPMatrix);
            mLine.SetVerts(
                    15 * pixPerDegree * -sinI, 15 * pixPerDegree * cosI, z,
                    16 * pixPerDegree * -sinI, 16 * pixPerDegree * cosI, z
            );
            mLine.draw(mMVPMatrix);
        }
        // 45 - even though it is only one number, leave the loop
        // for consitency and possible changes
        for (i = 45; i <= 60; i = i + 15) {
            sinI = UTrig.isin(i);
            cosI = UTrig.icos(i);
            // The lines
            mLine.SetVerts(
                    15 * pixPerDegree * sinI, 15 * pixPerDegree * cosI, z,
                    16 * pixPerDegree * sinI, 16 * pixPerDegree * cosI, z
            );
            mLine.draw(mMVPMatrix);
            mLine.SetVerts(
                    15 * pixPerDegree * -sinI, 15 * pixPerDegree * cosI, z,
                    16 * pixPerDegree * -sinI, 16 * pixPerDegree * cosI, z
            );
            mLine.draw(mMVPMatrix);
        }
        // 30 and 60
        for (i = 30; i <= 60; i = i + 30) {
            sinI = UTrig.isin(i);
            cosI = UTrig.icos(i);

            mLine.SetVerts(
                    15 * pixPerDegree * sinI, 15 * pixPerDegree * cosI, z,
                    17 * pixPerDegree * sinI, 17 * pixPerDegree * cosI, z
            );
            mLine.draw(mMVPMatrix);
            mLine.SetVerts(
                    15 * pixPerDegree * -sinI, 15 * pixPerDegree * cosI, z,
                    17 * pixPerDegree * -sinI, 17 * pixPerDegree * cosI, z
            );
            mLine.draw(mMVPMatrix);
        }

        // The arc
        _sinI = 0;
        _cosI = 1;
        for (i = 10; i <= 60; i = i + 5) {
            sinI = UTrig.isin(i);
            cosI = UTrig.icos(i);

            mLine.SetVerts(
                    15 * pixPerDegree * _sinI, 15 * pixPerDegree * _cosI, z,
                    15 * pixPerDegree * sinI, 15 * pixPerDegree * cosI, z
            );
            mLine.draw(mMVPMatrix);
            mLine.SetVerts(
                    15 * pixPerDegree * -_sinI, 15 * pixPerDegree * _cosI, z,
                    15 * pixPerDegree * -sinI, 15 * pixPerDegree * cosI, z
            );
            mLine.draw(mMVPMatrix);
            _sinI = sinI;
            _cosI = cosI;
        }

    } // renderFixedHorizonMarkers

    protected void renderRollMarkers(float[] matrix)
    {
        float z, pixPerDegree;
        z = zfloat;
        pixPerDegree = pixM2 / PPD_DIV;   // Put the markers in open space at zero pitch

        mTriangle.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);
        mTriangle.SetVerts(
                0.035f * pixM2, 13.5f * pixPerDegree, z,
                -0.035f * pixM2, 13.5f * pixPerDegree, z,
                0.0f, 15f * pixPerDegree, z);
        mTriangle.draw(matrix);
    }

    protected void renderPitchMarkers(float[] matrix)
    {
        int i;
        float innerTic, outerTic, z, pixPerDegree, iPix;
        float wid = 4;
        z = zfloat;

        if (Layout == layout_t.LANDSCAPE) {
            pixPerDegree = pixM / pitchInView;
        }
        else {
            pixPerDegree = pixM / pitchInView * 100 / 60;
        }

        innerTic = 0.10f * pixW2;
        outerTic = 0.13f * pixW2;

        // top
        for (i = 90; i > 0; i = i - 10) {
            iPix = (float) i * pixPerDegree;
            String t = Integer.toString(i);
            {
                mPolyLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); // white
                mPolyLine.SetWidth(wid);
                float[] vertPoly = {
                        // in counterclockwise order:
                        -innerTic, iPix, z,
                        -outerTic, iPix, z,
                        -outerTic, iPix - 0.03f * pixW2, z
                };
                mPolyLine.VertexCount = 3;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
            }
            glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
            //glText.setScale(2);
            glText4.drawC(t, -0.2f * pixW2, iPix + glText4.getCharHeight() / 2);
            glText4.end();

            {
                mPolyLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); //white
                mPolyLine.SetWidth(wid);
                float[] vertPoly = {
                        // in counterclockwise order:
                        0.1f * pixW2, iPix, z,
                        outerTic, iPix, z,
                        outerTic, iPix - 0.03f * pixW2, z
                };
                mPolyLine.VertexCount = 3;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
            }
            glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
            glText4.drawC(t, 0.2f * pixW2, iPix + glText4.getCharHeight() / 2);
            glText4.end();
        }


        mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // white
        mLine.SetWidth(wid);
        for (i = 9; i >= 6; i = i - 1) {
            iPix = (float) i * pixPerDegree;
            mLine.SetVerts(-0.03f * pixW2, iPix, z,
                    0.03f * pixW2, iPix, z);
            mLine.draw(matrix);
        }

        mLine.SetVerts(-0.1f * pixW2, 5.0f * pixPerDegree, z,
                0.1f * pixW2, 5.0f * pixPerDegree, z);
        mLine.draw(matrix);

        for (i = 4; i >= 1; i = i - 1) {
            iPix = (float) i * pixPerDegree;

            mLine.SetVerts(-0.03f * pixW2, iPix, z,
                    0.03f * pixW2, iPix, z);
            mLine.draw(matrix);
        }

        // horizon line - longer and thicker
        if (colorTheme == 2) mLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);  // bright white
        mLine.SetWidth(wid*2.5f);
        mLine.SetVerts(-0.95f * pixW2, 0.0f, z,
                0.95f * pixW2, 0.0f, z);
        mLine.draw(matrix);

        mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // white
        mLine.SetWidth(wid);
        for (i = -1; i >= -4; i = i - 1) {
            iPix = (float) i * pixPerDegree;
            mLine.SetVerts(-0.03f * pixW2, iPix, z,
                    0.03f * pixW2, iPix, z);
            mLine.draw(matrix);
        }

        mLine.SetVerts(-0.1f * pixW2, -5.0f * pixPerDegree, z,
                0.1f * pixW2, -5.0f * pixPerDegree, z);
        mLine.draw(matrix);

        for (i = -6; i >= -9; i = i - 1) {
            iPix = (float) i * pixPerDegree;
            mLine.SetVerts(-0.03f * pixW2, iPix, z,
                    0.03f * pixW2, iPix, z);
            mLine.draw(matrix);
        }

        // bottom
        for (i = -10; i >= -90; i = i - 10) {
            iPix = (float) i * pixPerDegree;
            String t = Integer.toString(i);

            {
                mPolyLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); // white
                mPolyLine.SetWidth(wid);
                float[] vertPoly = {
                        // in counterclockwise order:
                        -0.10f * pixW2, iPix, z,
                        -0.13f * pixW2, iPix, z,
                        -0.13f * pixW2, iPix + 0.03f * pixW2, z
                };

                mPolyLine.VertexCount = 3;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
            }
            glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
            glText4.drawC(t, -0.2f * pixW2, iPix + glText4.getCharHeight() / 2);
            glText4.end();

            {
                mPolyLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); // white
                mPolyLine.SetWidth(wid);
                float[] vertPoly = {
                        0.10f * pixW2, iPix, z,
                        0.13f * pixW2, iPix, z,
                        0.13f * pixW2, iPix + 0.03f * pixW2, z
                };

                mPolyLine.VertexCount = 3;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
            }
            glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
            glText4.drawC(t, 0.2f * pixW2, iPix + glText4.getCharHeight() / 2);
            glText4.end();
        }
    }

    protected void renderAHColors(float[] matrix)
    {
        float pixPitchViewMultiplier, pixOverWidth, z;

		/*!
            The ModelView has units of +/- 1 about the center.  In order to keep the gyro edges outside of the edges of
			the ViewPort, it is drawn wide to deal with affect of the aspect ratio scaling and the corners during roll

			The pitch range in degrees to be viewed must fit the ModelView units of 1. To accommodate this, the gyro must
			be ovesized, hence the multiplier 90/ pitchInView.
		 */

        pixPitchViewMultiplier = 90.0f / pitchInView * pixH;
        pixOverWidth = pixW2 * 1.80f;
        z = zfloat;


        // Earth
        // Level to -180 pitch
        // Handle Monochrome
        if (colorTheme == 2) mSquare.SetColor(0, 0.2f, 0, 1); //green
        else mSquare.SetColor(gamma * 0.30f, gamma * 0.20f, gamma * 0.10f, 1); //brown (3,2,1)
        mSquare.SetWidth(1);
        {
            float[] squarePoly = {
                    -pixOverWidth, -2.0f * pixPitchViewMultiplier, z,
                    +pixOverWidth, -2.0f * pixPitchViewMultiplier, z,
                    +pixOverWidth, 0.0f, z,
                    -pixOverWidth, 0.0f, z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }

        // Sky
        // Level to 180 pitch
        // TODO: 2017-10-31 make parameterised

        // Handle Monochrome
        if (colorTheme == 2) mSquare.SetColor(0, 0, 0, 1); //black
        else mSquare.SetColor(gamma * 0.10f, gamma * 0.20f, gamma * 0.30f, 1); //blue (1,2,3)
        mSquare.SetWidth(1);
        {
            float[] squarePoly = {
                    -pixOverWidth, 0.0f, z,
                    +pixOverWidth, 0.0f, z,
                    +pixOverWidth, 2.0f * pixPitchViewMultiplier, z,
                    -pixOverWidth, 2.0f * pixPitchViewMultiplier, z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }

    }


    //-------------------------------------------------------------------------
    // Set the pitch angle
    //
    public void setPitch(float degrees)
    {
        float pitch = -degrees;
        pitchTranslation = pitch / pitchInView * pixH;
    }

    //-------------------------------------------------------------------------
    // Set the roll angle
    //
    public void setRoll(float degrees)
    {
        // Pitch and roll in degrees
        rollRotation = degrees;
    }


    // TODO: use slide to position and get rid of the top/right vars
    //-------------------------------------------------------------------------
    // RadAlt Indicator (AGL)
    //
    protected void renderFixedRadAltMarkers(float[] matrix)
    {
        float z = zfloat;
        String t;

        float top = 0;
        float right = 0;
        float left = right - 0.35f * pixM2;


        // The tapes are positioned left & right of the roll circle, occupying the space based
        // on the vertical dimension, from .6 to 1.0 pixM2.  This makes the basic display
        // square, leaving extra space outside the edges for terrain which can be clipped if required.

        // Radio Altimeter (AGL) Display

        // Do a dummy glText so that the Heights are correct for the masking box
        //glText.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        //glText.setScale(2.5f);  //was 1.5
        //glText.end();

        glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        //glText.setScale(2.5f);  //was 1.5
        glText4.end();

        // Mask over the moving tape for the value display box
        {
            float[] squarePoly = {
                    right, top - glText4.getCharHeight(), z,
                    right, top + glText4.getCharHeight(), z,
                    left,  top + glText4.getCharHeight(), z,
                    left,  top - glText4.getCharHeight(), z
            };
            mSquare.SetColor(backShadeR, backShadeG, backShadeB, 1); // black
            mSquare.SetWidth(2);
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);

            float[] vertPoly = {
                    right, top - glText4.getCharHeight(), z,
                    right, top + glText4.getCharHeight(), z,
                    left,  top + glText4.getCharHeight(), z,
                    left,  top - glText4.getCharHeight(), z,
                    right, top - glText4.getCharHeight(), z
            };

            mPolyLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); //white
            mPolyLine.SetWidth(2);
            mPolyLine.VertexCount = 5;
            mPolyLine.SetVerts(vertPoly);
            mPolyLine.draw(matrix);
        }

        // if we are below the preset, show the warning chevrons
        final float CevronAGL = 1000.0f;
        if (AGLValue < CevronAGL) {
            float slant = 0.06f * pixM2;
            float step = 0.04f * pixM2;
            float i;
            // moving yellow chevrons
            mLine.SetColor(tapeShadeR, tapeShadeG, 0.0f, 0.5f); // yellow
            mLine.SetWidth(8);
            for (i = left; i < right - (float) AGLValue / CevronAGL * (right - left) - step; i = i + step) {
                mLine.SetVerts(
                        i, top + glText4.getCharHeight(), z,
                        slant + i, top - glText4.getCharHeight(), z
                );
                mLine.draw(matrix);
            }

            // Chevron left filler
            mLine.SetVerts(
                    left, top, z,
                    left + step / 2, top - glText4.getCharHeight(), z
            );
            mLine.draw(matrix);

            // Chevron right filler
            mLine.SetVerts(
                    i, top + glText4.getCharHeight(), z,
                    slant / 2 + i - 2, top, z
            );
            mLine.draw(matrix);

            /* this seems a little bit over the top
            // bar
            mLine.SetVerts(
                    spinnerStep + i, top + glText.getCharHeight(), z,
                    spinnerStep + i, top - glText.getCharHeight(), z
            );
            mLine.draw(matrix);
            */
        }

        int aglAlt = Math.round((float) this.AGLValue / 10) * 10;  // round to 10
        // draw the tape text in mixed sizes
        // to clearly show the thousands
        t = Integer.toString(aglAlt / 1000);
        float margin;

        // draw the thousands digits larger
        glText7.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        //glText.setScale(3.5f);
        if (aglAlt >= 1000) glText7.draw(t, left + 0.03f * pixM2, top - glText7.getCharHeight() / 2);
        if (aglAlt < 10000) margin = 0.6f * glText7.getCharWidthMax(); // because of the differing sizes
        else margin = 1.1f * glText7.getCharWidthMax(); // we have to deal with the margin ourselves
        glText7.end();

        // draw the hundreds digits smaller
        t = String.format("%03.0f", (float) aglAlt % 1000);
        glText5.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        //glText.setScale(2.5f);
        glText5.draw(t, left + 0.03f * pixM2 + margin, top - glText5.getCharHeight() / 2);
        glText5.end();

    }

    // TODO: use slide to position and get rid of the top/right vars
    //-------------------------------------------------------------------------
    // Altimeter Indicator
    //
    protected void renderFixedAltMarkers(float[] matrix)
    {
        float z = zfloat;
        String t;

        float right = 0;
        //float left = right - 0.35f * pixM2;
        float left = right - 0.40f * pixM2;
        float apex = left - 0.05f * pixM2;

        // The tapes are positioned left & right of the roll circle, occupying the space based
        // on the vertical dimension, from .6 to 1.0 pixM2.  This makes the basic display
        // square, leaving extra space outside the edges for terrain which can be clipped if required.

        // Altimeter Display

        // Do a dummy glText so that the Heights are correct for the masking box
        //glText5.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        //glText.setScale(2.5f);
        //glText5.end();

        // Mask over the moving tape for the value display box
        mSquare.SetColor(backShadeR, backShadeG, backShadeB, 1); // black
        mSquare.SetWidth(1);
        {
            float[] squarePoly = {
                    right, -glText5.getCharHeight(), z,
                    right,  glText5.getCharHeight(), z,
                    left,   glText5.getCharHeight(), z,
                    left,  -glText5.getCharHeight(), z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);

            mTriangle.SetColor(backShadeR, backShadeG, backShadeB, 1);  // black
            mTriangle.SetVerts(
                    left, glText5.getCharHeight() / 2, z,
                    apex, 0.0f, z,
                    left, -glText5.getCharHeight() / 2, z
            );
            mTriangle.draw(mMVPMatrix);

            mPolyLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); // white
            mPolyLine.SetWidth(2);
            float[] vertPoly = {
                    right, -glText5.getCharHeight(), z,
                    right,  glText5.getCharHeight(), z,
                    left,   glText5.getCharHeight(), z,
                    left,   glText5.getCharHeight() / 2, z,
                    apex,   0.0f, z,
                    left,  -glText5.getCharHeight() / 2, z,
                    left,  -glText5.getCharHeight(), z,
                    right, -glText5.getCharHeight(), z
            };
            mPolyLine.VertexCount = 8;
            mPolyLine.SetVerts(vertPoly);
            mPolyLine.draw(matrix);
        }



        int mslAlt = Math.round((float) this.MSLValue / 10) * 10;  // round to 10
        // draw the tape text in mixed sizes
        // to clearly show the thousands
        t = Integer.toString(mslAlt / 1000);
        float margin;

        // draw the thousands digits larger
        glText9.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        if (mslAlt >= 1000) glText9.draw(t, left + 0.02f * pixM2, -glText9.getCharHeight() / 2);
        if (mslAlt < 10000) margin = 0.6f * glText9.getCharWidthMax(); // because of the differing sizes
        else margin = 1.01f * glText9.getCharWidthMax(); // we have to deal with the margin ourselves
        glText9.end();

        // draw the hundreds digits smaller
        t = String.format("%03.0f", (float) mslAlt % 1000);
        glText6.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        glText6.draw(t, left + 0.03f * pixM2 + margin, -glText6.getCharHeight() / 2);
        glText6.end();

        // Units
        glText3.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        //glText3.draw("ft", (right) - 2.2f * glText3.getCharWidth('k'), 0.45f * glText5.getCharHeight());
        glText3.draw("ft", (right) - 2.2f * glText3.getCharWidth('k'), glText5.getCharHeight());
        glText3.end();

    }

    protected void renderAltMarkers(float[] matrix)
    {
        int i, j;
        float innerTic, midTic, outerTic, z, pixPerUnit, iPix;

        //pixPerUnit = pixM2 / MSLInView; //b2 landscape
        pixPerUnit = pixH2 / MSLInView; // portrait
        z = zfloat;

        innerTic = 0.70f * pixM2;    // inner & outer are relative to the vertical scale line
        midTic = 0.75f * pixM2;
        outerTic = 0.80f * pixM2;

        // The numbers & tics for the tape
        for (i = MSLMaxDisp; i >= MSLMinDisp; i = i - 100) {
            // Ugly hack but is does significantly improve performance.
            if (i > MSLValue + 1.00 * MSLInView) continue;
            if (i < MSLValue - 1.50 * MSLInView) continue;

            iPix = (float) i * pixPerUnit;

            mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // grey
            mLine.SetWidth(3);
            mLine.SetVerts(
                    innerTic, iPix, z,
                    outerTic, iPix, z
            );
            mLine.draw(matrix);

            // draw the tape text in mixed sizes
            // to clearly show the thousands
            String t = Integer.toString(i / 1000);
            float margin;

            // draw the thousands digits larger
            glText6.begin(tapeShadeR, tapeShadeG, tapeShadeB, 1.0f, matrix); // grey
            //glText.setScale(3.0f);
            if (i >= 1000) glText6.draw(t, outerTic, iPix - glText6.getCharHeight() / 2);
            if (i < 10000) margin = 0.6f * glText6.getCharWidthMax();  // because of the differing sizes
            else margin = 1.1f * glText6.getCharWidthMax();  // we have to deal with the margin ourselves
            glText6.end();

            // draw the hundreds digits smaller
            t = String.format("%03.0f", (float) i % 1000);
            glText4.begin(tapeShadeR, tapeShadeG, tapeShadeB, 1.0f, matrix); // grey
            //glText.setScale(2.0f);
            glText4.draw(t, outerTic + margin, iPix - glText4.getCharHeight() / 2);
            glText4.end();

            for (j = i + 20; j < i + 90; j = j + 20) {
                iPix = (float) j * pixPerUnit;
                mLine.SetWidth(2);
                mLine.SetVerts(
                        innerTic, iPix, z,
                        midTic, iPix, z
                );
                mLine.draw(matrix);
            }
        }

        // The vertical scale bar
        mLine.SetVerts(
                innerTic, MSLMinDisp, z,
                innerTic, (MSLMaxDisp + 100) * pixPerUnit, z
        );
        mLine.draw(matrix);
    }

    //
    //Set the altimeter - ft
    //
    public void setAlt(int feet)
    {
        MSLValue = feet;
        MSLTranslation = MSLValue / MSLInView * pixH2;
    }

    //
    //Set the barometric pressure
    //
    void setBaro(float milliBar)
    {
        // Barometric pressure in in-Hg
    }


    void renderFixedVSIMarkers(float[] matrix)
    {
        int i, j;
        float innerTic, midTic, outerTic, z, pixPerUnit, iPix;

        pixPerUnit = 0.75f * pixM2 / VSIInView;
        z = zfloat;

        innerTic = 1.20f * pixM2;    // inner & outer are relative to the vertical scale line
        midTic = 1.23f * pixM2;
        outerTic = 1.26f * pixM2;

        for (i = 0; i <= VSIInView; i = i + 500) {

            iPix = (float) i * pixPerUnit;

            String t = Float.toString((float) i / 1000);

            mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // grey
            mLine.SetWidth(2);
            mLine.SetVerts(
                    innerTic, iPix, z,
                    outerTic, iPix, z
            );
            mLine.draw(matrix);

            glText3.begin(tapeShadeR, tapeShadeG, tapeShadeB, 1, matrix); // white
            //glText.setScale(1.5f);
            glText3.draw(t, outerTic + glText3.getCharWidthMax() / 2, iPix - glText3.getCharHeight() / 2);
            glText3.end();

            if (i < VSIInView) {
                for (j = i + 100; j < i + 500; j = j + 100) {
                    iPix = (float) j * pixPerUnit;
                    mLine.SetVerts(
                            innerTic, iPix, z,
                            midTic, iPix, z
                    );
                    mLine.draw(matrix);
                }
            }
        }

        // The vertical scale bar
        mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // grey
        mLine.SetWidth(2);
        mLine.SetVerts(
                innerTic, -VSIInView, z,
                innerTic, (+VSIInView + 100) * pixPerUnit, z
        );
        mLine.draw(matrix);
    }


    //-------------------------------------------------------------------------
    // VSI Indicator
    //
    protected void renderVSIMarkers(float[] matrix)
    {
        int i;
        float z, pixPerUnit, innerTic;

        pixPerUnit = pixM2 / VSIInView;
        z = zfloat;
        innerTic = 0.64f * pixM2;    // inner & outer are relative to the vertical scale line

        // VSI box
        for (i = -2; i <= 2; i += 1) {
            mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // grey
            mLine.SetWidth(4);
            mLine.SetVerts(
                    0.64f * pixM2, i * 1000 * pixPerUnit, z,
                    0.70f * pixM2, i * 1000 * pixPerUnit, z
            );
            mLine.draw(matrix);

            if (i != 0) {
                String s = Integer.toString(Math.abs(i));
                glText6.begin(tapeShadeR, tapeShadeG, tapeShadeB, 1.0f, matrix); // light grey
                //glText.setScale(3.0f);
                glText6.draw(s, innerTic - 1.5f * glText6.getLength(s), i * 1000 * pixPerUnit - glText6.getCharHeight() / 2);
                glText6.end();
            }
        }

        // vertical speed  bar
        mLine.SetColor(0, 0.8f, 0, 1); // green
        mLine.SetWidth(16);
        mLine.SetVerts(
                0.67f * pixM2, 0.0f * pixH2, z,
                0.67f * pixM2, VSIValue * pixPerUnit, z
        );
        mLine.draw(matrix);
    }


    //
    //Set the VSI - ft
    //
    public void setVSI(int fpm)
    {
        VSIValue = fpm;
    }

    // TODO: use slide to position and get rid of the top/right vars
    //-------------------------------------------------------------------------
    // Airspeed Indicator
    //
    protected void renderFixedASIMarkers(float[] matrix)
    {
        float z = zfloat;
        String t;

        float left = 0;
        float right = left + 0.35f * pixM2;
        float apex = right + 0.05f * pixM2;


        // The tapes are positioned left & right of the roll circle, occupying the space based
        // on the vertical dimension, from .6 to 1.0 pixH2.  This makes the basic display
        // square, leaving extra space outside the edges for terrain which can be clipped if reqd.

        // Do a dummy glText so that the Heights are correct for the masking box
        //glText5.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        //glText.setScale(2.5f); // was 1.5
        //glText5.end();

        // Mask over the moving tape for the value display box
        mSquare.SetColor(backShadeR, backShadeG, backShadeB, 1); //black
        mSquare.SetWidth(2);
        {
            float[] squarePoly = {
                    left,  -glText5.getCharHeight(), z,
                    left,   glText5.getCharHeight(), z,
                    right,  glText5.getCharHeight(), z,
                    right, -glText5.getCharHeight(), z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);


            mTriangle.SetColor(backShadeR, backShadeG, backShadeB, 1);  //black
            mTriangle.SetVerts(
                    right, glText5.getCharHeight() / 2, z,
                    apex, 0.0f, z,
                    right, -glText5.getCharHeight() / 2, z
            );
            mTriangle.draw(mMVPMatrix);

            mPolyLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); //white
            mPolyLine.SetWidth(2);
            float[] vertPoly = {
                    left, -glText5.getCharHeight(), z,
                    left,  glText5.getCharHeight(), z,
                    right, glText5.getCharHeight(), z,
                    right, glText5.getCharHeight() / 2, z,
                    apex, 0.0f, z,
                    right, -glText5.getCharHeight() / 2, z,
                    right, -glText5.getCharHeight(), z,
                    left,  -glText5.getCharHeight(), z
            };

            mPolyLine.VertexCount = 8;
            mPolyLine.SetVerts(vertPoly);
            mPolyLine.draw(matrix);
        }

        t = Integer.toString(Math.round(IASValue));
        glText9.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix);     // white
        //glText9.drawC(t, left + 0.25f * pixM2, glText9.getCharHeight() / 2);
        glText9.drawC(t, left + 0.30f * pixM2, glText9.getCharHeight() / 2);
        glText9.end();

        // Units
        glText3.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        //glText3.draw("kt", (right) - 2.2f * glText3.getCharWidth('k'), 0.45f * glText5.getCharHeight());
        //glText3.draw("kt", (right) - 2.2f * glText3.getCharWidth('k'), glText5.getCharHeight());
        glText3.draw("kt", left, glText5.getCharHeight());
        glText3.end();
    }


    protected void renderASIMarkers(float[] matrix)
    {
        int i, j;
        float innerTic, midTic, outerTic;
        float z, pixPerUnit, iPix;

        z = zfloat;
        pixPerUnit = pixH2 / IASInView;

        innerTic = -0.70f * pixM2;    // inner & outer are relative to the vertical scale line
        outerTic = -0.80f * pixM2;
        midTic = -0.77f * pixM2;

        // The numbers & tics for the tape
        for (i = IASMaxDisp; i >= 0; i = i - 10) {
            iPix = (float) i * pixPerUnit;
            String t = Integer.toString(i);

            mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // grey
            mLine.SetWidth(2);
            mLine.SetVerts(
                    innerTic, iPix, z,
                    outerTic, iPix, z
            );
            mLine.draw(matrix);

            glText5.begin(tapeShadeR, tapeShadeG, tapeShadeB, 1.0f, matrix); // grey
            //glText.setScale(2.5f);
            glText5.draw(t, outerTic - 1.5f * glText5.getLength(t), iPix - glText5.getCharHeight() / 2);
            glText5.end();

            for (j = i + 2; j < i + 9; j = j + 2) {
                iPix = (float) j * pixPerUnit;
                mLine.SetVerts(
                        innerTic, iPix, z,
                        midTic, iPix, z
                );
                mLine.draw(matrix);
            }
        }

        // The vertical scale bar
        mLine.SetVerts(
                innerTic, 0, z,  // IASMinDisp - no longer used, set to 0
                innerTic, (IASMaxDisp + 100) * pixPerUnit, z
        );
        mLine.draw(matrix);

        // For monochrome display (displayTerrain false) do not use any color
        if (displayAHColors) {
            //
            // Special Vspeed markers
            //

            // Simplified V Speeds
            glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // grey
            //glText.setScale(2.0f);    // was 1.5
            glText4.draw(" Vx", innerTic, (float) AircraftData.Vx * pixPerUnit); // Vx
            glText4.draw(" Vy", innerTic, (float) AircraftData.Vy * pixPerUnit); // Vy
            glText4.draw(" Va", innerTic, (float) AircraftData.Va * pixPerUnit); // Va
            glText4.end();


            // Tape markings for V speeds
            // Re use midTic ... maybe not such a good idea ...
            midTic = -0.75f * pixM2;          // Put tape under the tics, w/ VsO-Vfe bar narrower than minor tics
            mSquare.SetColor(0, 0.5f, 0, 1);  // dark green arc
            mSquare.SetWidth(1);
            {
                float[] squarePoly = {
                        innerTic, (float) AircraftData.Vs1 * pixPerUnit, z,
                        innerTic, (float) AircraftData.Vno * pixPerUnit, z,
                        midTic, (float) AircraftData.Vno * pixPerUnit, z,
                        midTic, (float) AircraftData.Vs1 * pixPerUnit, z
                };
                mSquare.SetVerts(squarePoly);
                mSquare.draw(matrix);
            }

            mSquare.SetColor(theta * tapeShadeR, theta * tapeShadeG, theta * tapeShadeB, 1);  // white arc
            mSquare.SetWidth(1);
            {
                float[] squarePoly = {
                        innerTic, (float) AircraftData.Vs0 * pixPerUnit, z,
                        innerTic, (float) AircraftData.Vfe * pixPerUnit, z,
                        midTic, (float) AircraftData.Vfe * pixPerUnit, z,
                        midTic, (float) AircraftData.Vs0 * pixPerUnit, z
                };
                mSquare.SetVerts(squarePoly);
                mSquare.draw(matrix);
            }

            mSquare.SetColor(theta * foreShadeR, theta * foreShadeG, 0, 1);  // yellow arc
            mSquare.SetWidth(1);
            {
                float[] squarePoly = {
                        innerTic, (float) AircraftData.Vno * pixPerUnit, z,
                        innerTic, (float) AircraftData.Vne * pixPerUnit, z,
                        midTic, (float) AircraftData.Vne * pixPerUnit, z,
                        midTic, (float) AircraftData.Vno * pixPerUnit, z
                };
                mSquare.SetVerts(squarePoly);
                mSquare.draw(matrix);
            }

            // Vne
            mSquare.SetColor(theta * foreShadeR, 0, 0, 1);  // red
            mSquare.SetWidth(1);
            {
                float[] squarePoly = {
                        innerTic, (float) AircraftData.Vne * pixPerUnit, z,
                        innerTic, (float) (AircraftData.Vne + 1) * pixPerUnit, z,
                        outerTic, (float) (AircraftData.Vne + 1) * pixPerUnit, z,
                        outerTic, (float) AircraftData.Vne * pixPerUnit, z
                };
                mSquare.SetVerts(squarePoly);
                mSquare.draw(matrix);

                float[] squarePoly2 = {
                        innerTic, (float) AircraftData.Vne * pixPerUnit, z,
                        innerTic, (float) (IASMaxDisp + 10) * pixPerUnit, z,
                        midTic, (float) (IASMaxDisp + 10) * pixPerUnit, z,
                        midTic, (float) AircraftData.Vne * pixPerUnit, z
                };
                mSquare.SetVerts(squarePoly2);
                mSquare.draw(matrix);
            }
        }
    }

    //
    //Set the IAS indicator
    //
    public void setASI(float value)
    {
        IASValue = value;
        IASTranslation = IASValue / IASInView * pixH2;
    }

    //-------------------------------------------------------------------------
    // Direction Indicator
    //   Just a simple text box
    //
    protected void renderFixedDIMarkers(float[] matrix)
    {
        float z = zfloat;
        //float top = 0.9f * pixH2;
        float left = -0.20f * pixM2;
        float right = 0.20f * pixM2;

        // The tapes are positioned left & right of the roll circle, occupying the space based
        // on the vertical dimension, from .6 to 1.0 pixH2.  This makes the basic display
        // square, leaving extra space outside the edges for terrain which can be clipped if reqd.

        // Do a dummy glText so that the Heights are correct for the masking box
        glText5.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        glText5.end();

        // Mask over the moving tape for the value display box
        mSquare.SetColor(backShadeR, backShadeG, backShadeB, 1); //black
        mSquare.SetWidth(2);
        {
            float[] squarePoly = {
                    right, - glText5.getCharHeight(), z,
                    right, + glText5.getCharHeight(), z,
                    left,  + glText5.getCharHeight(), z,
                    left,  - glText5.getCharHeight(), z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }

        {
            mPolyLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); // white
            mPolyLine.SetWidth(2);
            float[] vertPoly = {
                    right, + glText5.getCharHeight(), z,
                    left,  + glText5.getCharHeight(), z,
                    left,  - glText5.getCharHeight(), z,
                    right, - glText5.getCharHeight(), z,
                    right, + glText5.getCharHeight(), z,

                    // for some reason this causes a crash on restart if there are not 8 vertexes
                    // most probably a a bug in PolyLine - b2 may be fixed comma after last z
                    left,  + glText5.getCharHeight(), z,
                    left,  - glText5.getCharHeight(), z,
                    right, - glText5.getCharHeight(), z
            };
            mPolyLine.VertexCount = 8;
            mPolyLine.SetVerts(vertPoly);
            mPolyLine.draw(matrix);
        }

        int rd = Math.round(DIValue);           // round to nearest integer
        String t = Integer.toString(rd);
        glText9.begin(foreShadeR, foreShadeG, foreShadeB, 1, matrix);     // white
        glText9.drawCX(t, 0, - glText9.getCharHeight() / 2);
        glText9.end();

        // Units The degree symbol
        glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        glText4.draw("o", right - 1.2f * glText4.getCharWidth('o'), 0.4f * glText5.getCharHeight());
        glText4.end();
    }


    //---------------------------------------------------------------------------
    // EFIS serviceability ... aka the Red X's
    //
    /*protected void renderUnserviceableDevice(float[] matrix)
    {
        //renderUnserviceablePage(matrix);
        renderUnserviceableDi(matrix);
        renderUnserviceableAlt(matrix);
        renderUnserviceableAsi(matrix);
    }*/

    // this must be overridden in the child classes
    abstract protected void renderUnserviceableDevice(float[] matrix);


    protected void renderUnserviceablePage(float[] matrix)
    {
        float z;
        z = zfloat;

        mLine.SetColor(1, 0, 0, 1);  // red
        mLine.SetWidth(20);

        mLine.SetVerts(
                -0.7f * pixM2, 0.8f * pixH2, z,
                0.7f * pixM2, -0.8f * pixH2, z
        );
        mLine.draw(matrix);
        mLine.SetVerts(
                0.7f * pixM2, 0.8f * pixH2, z,
                -0.7f * pixM2, -0.8f * pixH2, z
        );
        mLine.draw(matrix);
    }

    protected void renderUnserviceableAh(float[] matrix)
    {
        float z;
        z = zfloat;

        mLine.SetColor(1, 0, 0, 1);  // red
        mLine.SetWidth(20);

        mLine.SetVerts(
                -0.7f * pixM2, 0.8f * pixH2, z,
                0.7f * pixM2, -0.0f * pixH2, z
        );
        mLine.draw(matrix);
        mLine.SetVerts(
                0.7f * pixM2, 0.8f * pixH2, z,
                -0.7f * pixM2, -0.0f * pixH2, z
        );
        mLine.draw(matrix);
    }


    protected void renderUnserviceableCompassRose(float[] matrix)
    {
        float z;
        z = zfloat;

        mLine.SetColor(1, 0, 0, 1);  // red
        mLine.SetWidth(20);

        if (Layout == layout_t.LANDSCAPE) {
            mLine.SetVerts(
                    -pixM, pixM2, z,
                    -pixM2, 0, z
            );
            mLine.draw(matrix);
            mLine.SetVerts(
                    -pixM, 0, z,
                    -pixM2, pixM2, z
            );
            mLine.draw(matrix);
        }
        else {
            mLine.SetVerts(
                    -0.7f * pixM2, 0, z,
                    0.7f * pixM2, -0.8f * pixH2, z
            );
            mLine.draw(matrix);
            mLine.SetVerts(
                    0.7f * pixM2, 0.0f * pixH2, z,
                    -0.7f * pixM2, -0.8f * pixH2, z
            );
            mLine.draw(matrix);
        }
    }



    protected void renderUnserviceableDi(float[] matrix)
    {
        float z;
        z = zfloat;

        mLine.SetColor(1, 0, 0, 1);  // red
        mLine.SetWidth(8);

        mLine.SetVerts(
                -0.7f * pixM2, 0.95f * pixH2, z,
                0.7f * pixM2, 0.85f * pixH2, z
        );
        mLine.draw(matrix);
        mLine.SetVerts(
                0.7f * pixM2, 0.95f * pixH2, z,
                -0.7f * pixM2, 0.85f * pixH2, z
        );
        mLine.draw(matrix);
    }


    protected void renderUnserviceableAlt(float[] matrix)
    {
        float z;
        z = zfloat;

        mLine.SetColor(1, 0, 0, 1);  // red
        mLine.SetWidth(8);

        mLine.SetVerts(
                0.7f * pixM2, 0.8f * pixH2, z,
                pixM2, -0.8f * pixH2, z
        );
        mLine.draw(matrix);
        mLine.SetVerts(
                pixM2, 0.8f * pixH2, z,
                0.7f * pixM2, -0.8f * pixH2, z
        );
        mLine.draw(matrix);
    }

    protected void renderUnserviceableAsi(float[] matrix)
    {
        float z;
        z = zfloat;

        mLine.SetColor(1, 0, 0, 1);  // red
        mLine.SetWidth(8);

        mLine.SetVerts(
                -0.7f * pixM2, 0.8f * pixH2, z,
                -pixM2, -0.8f * pixH2, z
        );
        mLine.draw(matrix);
        mLine.SetVerts(
                -pixM2, 0.8f * pixH2, z,
                -0.7f * pixM2, -0.8f * pixH2, z
        );
        mLine.draw(matrix);
    }

    protected void renderUnserviceableSelWpt(float[] matrix)
    {
        float z;
        z = zfloat;

        mLine.SetColor(1, 0, 0, 1);  // red
        mLine.SetWidth(8);

        /*mLine.SetVerts(
                -0.7f * pixM2, 0.8f * pixH2, z,
                -pixM2, -0.8f * pixH2, z
        );*/
        float size = spinnerStep * 0.2f;
        mLine.SetVerts(
                ((leftC - size) * pixW2), selWptDec + 2 * size * pixM2, z,
                ((leftC + 3 * spinnerStep) + size) * pixW2, selWptInc - 2 * size * pixM2, z
        );
        mLine.draw(matrix);

        mLine.SetVerts(
                ((leftC - size) * pixW2), selWptInc - 2 * size * pixM2, z,
                ((leftC + 3 * spinnerStep) + size) * pixW2, selWptDec + 2 * size * pixM2, z
        );
        mLine.draw(matrix);
    }



    // Overall PFD serviceability
    public void setServiceableDevice()
    {
        ServiceableDevice = true;
    }

    public void setUnServiceableDevice()
    {
        ServiceableDevice = false;
    }

    // Compass Rose serviceability
    public void setServiceableRose()
    {
        ServiceableRose = true;
    }

    public void setUnServiceableRose()
    {
        ServiceableRose = false;
    }


    // Altimeter serviceability
    public void setServiceableAlt()
    {
        ServiceableAlt = true;
    }

    public void setUnServiceableAlt()
    {
        ServiceableAlt = false;
    }


    // Airspeed Indicator serviceability
    public void setServiceableAsi()
    {
        ServiceableAsi = true;
    }

    public void setUnServiceableAsi()
    {
        ServiceableAsi = false;
    }

    // Direction Indicaotor serviceability
    public void setServiceableDi()
    {
        ServiceableDi = true;
    }

    public void setUnServiceableDi()
    {
        ServiceableDi = false;
    }


    //---------------------------------------------------------------------------
    // EFIS serviceability ... aka the Red X's
    //

    // Artificial Horizon serviceability
    public void setServiceableAh()
    {
        ServiceableAh = true;
    }

    public void setUnServiceableAh()
    {
        ServiceableAh = false;
    }


    //---------------------------------------------------------------------------
    // DMAP serviceability ... aka the Red X's
    //

    // Artificial Horizon serviceability
    public void setServiceableMap()
    {
        ServiceableMap = true;
    }

    public void setUnServiceableMap()
    {
        ServiceableMap = false;
    }



    // Display control for FPV
    public void setDisplayFPV(boolean display)
    {
        displayFPV = display;
    }

    // Display control for Airports
    public void setDisplayAirport(boolean display)
    {
        displayAirport = display;
    }

    // Display control for Airspace
    public void setDisplayAirspace(boolean display)
    {
        displayAirspace = display;
    }

/*    protected void renderHDGValue(float[] matrix)
    {
        int rd = Math.round(DIValue);           // round to nearest integer
        String t = Integer.toString(rd);
        glText9.begin(foreShadeR, foreShadeG, foreShadeB, 1, matrix);     // white
        glText9.drawCX(t, 0, - glText9.getCharHeight() / 2);  // Draw String
        glText9.end();
    }*/

    public void setHeading(float value)
    {
        DIValue = value;
    }


    //-------------------------------------------------------------------------
    // Slip ball
    //
    protected void renderSlipBall(float[] matrix)
    {
        float z;

        z = zfloat;

        float radius = pixM * 0.015f;
        float x1 = SlipValue * pixM / 100 ;
        float y1 = -0.9f * pixH2;

        // slip box
        mLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);
        mLine.SetWidth(4);
        mLine.SetVerts(
                -0.07f * pixM2, y1 -  glText3.getCharHeight(), z,
                -0.07f * pixM2, y1 +  glText3.getCharHeight(), z
        );
        mLine.draw(matrix);

        mLine.SetVerts(
                0.07f * pixM2, y1 -  glText3.getCharHeight(), z,
                0.07f * pixM2, y1 +  glText3.getCharHeight(), z
        );
        mLine.draw(matrix);

        // slip ball
        mPolygon.SetWidth(1);
        mPolygon.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); // white - always?
        {
            float[] vertPoly = {
                    // some issue with draworder to figure out.
                    x1 + 2.0f * radius, y1 + 0.8f * radius, z,
                    x1 + 0.8f * radius, y1 + 2.0f * radius, z,
                    x1 - 0.8f * radius, y1 + 2.0f * radius, z,
                    x1 - 2.0f * radius, y1 + 0.8f * radius, z,
                    x1 - 2.0f * radius, y1 - 0.8f * radius, z,
                    x1 - 0.8f * radius, y1 - 2.0f * radius, z,
                    x1 + 0.8f * radius, y1 - 2.0f * radius, z,
                    x1 + 2.0f * radius, y1 - 0.8f * radius, z
            };
            mPolygon.VertexCount = 8;
            mPolygon.SetVerts(vertPoly);
            mPolygon.draw(matrix);
        }
    }

    public void setSlip(float value)
    {
        SlipValue = value;
    }


    //-------------------------------------------------------------------------
    // Flight Path Vector
    //
    protected void renderFPV(float[] matrix)
    {
        float z, pixPerDegree;

        pixPerDegree = pixM2 / PPD_DIV;
        z = zfloat;

        float radius = 10 * pixM / 736;

        float x1 = fpvX * pixPerDegree;
        float y1 = fpvY * pixPerDegree;

        mPolyLine.SetWidth(3);
        mPolyLine.SetColor(0, foreShadeG, 0, 1); // green
        {
            float[] vertPoly = {
                    // some issue with draworder to figger out.
                    x1 + 2.0f * radius, y1 + 0.8f * radius, z,
                    x1 + 0.8f * radius, y1 + 2.0f * radius, z,
                    x1 - 0.8f * radius, y1 + 2.0f * radius, z,
                    x1 - 2.0f * radius, y1 + 0.8f * radius, z,
                    x1 - 2.0f * radius, y1 - 0.8f * radius, z,
                    x1 - 0.8f * radius, y1 - 2.0f * radius, z,
                    x1 + 0.8f * radius, y1 - 2.0f * radius, z,
                    x1 + 2.0f * radius, y1 - 0.8f * radius, z,
                    x1 + 2.0f * radius, y1 + 0.8f * radius, z
            };
            mPolyLine.VertexCount = 9;
            mPolyLine.SetVerts(vertPoly);  // crash here
            mPolyLine.draw(matrix);
        }

        mLine.SetWidth(3);
        mLine.SetColor(0, foreShadeG, 0, 1); // green
        mLine.SetVerts(
                x1 + 2.0f * radius, y1 + 0.0f * radius, z,
                x1 + 4.0f * radius, y1 + 0.0f * radius, z
        );
        mLine.draw(matrix);

        mLine.SetVerts(
                x1 - 2.0f * radius, y1 + 0.0f * radius, z,
                x1 - 4.0f * radius, y1 + 0.0f * radius, z
        );
        mLine.draw(matrix);

        mLine.SetVerts(
                x1 + 0.0f * radius, y1 + 2.0f * radius, z,
                x1 + 0.0f * radius, y1 + 4.0f * radius, z
        );
        mLine.draw(matrix);
    }

    public void setFPV(float x, float y)
    {
        fpvX = x;
        fpvY = y;
    }

    // This may be a differnt name?
    //-------------------------------------------------------------------------
    // Airports / Waypoints
    //

    //
    // Variables specific to render APT
    //
    protected final int MX_APT_SEEK_RNG = 99;
    protected final int MX_NR_APT = 20;
    protected int AptSeekRange = 20;   // start with 20nm
    //protected int Aptscounter = 0;
    protected int nrAptsFound = 0;
    protected int nrAirspaceFound = 0;

    //-------------------------------------------------------------------------
    // Airports / Waypoints
    //

    //
    // Variables specific to render APT
    //
    protected void renderAPT(float[] matrix)
    {
        float z, x1, y1;

        z = zfloat;

        // 0.16667 deg lat  = 10 nm
        // 0.1 approx 5nm
        float dme;
        float _dme = 1000;
        float aptRelBrg;
        String wptId = mSelWptName;
        float elev;

        // Aways draw at least the selected waypoint
        // TODO: 2018-08-12 Add elev to selected WPT
        wptId = mSelWptName;
        dme = UNavigation.calcDme(LatValue, LonValue, mSelWptLat, mSelWptLon); // in nm
        aptRelBrg = UNavigation.calcRelBrg(LatValue, LonValue, mSelWptLat, mSelWptLon, DIValue);
        x1 = project(aptRelBrg, dme).x;
        y1 = project(aptRelBrg, dme).y;
        renderAPTSymbol(matrix, x1, y1, wptId);

        // draw all the other waypoints that fit the criteria
        if (Gpx.bReady) {
            nrAptsFound = 0;
            Iterator<Apt> it = Gpx.aptList.iterator();
            while (it.hasNext()) {
                Apt currApt;
                try {
                    currApt = it.next();
                    wptId = currApt.name;
                }
                //catch (ConcurrentModificationException e) {
                catch (NullPointerException e) {
                    e.printStackTrace();
                    continue;
                }
                catch (Exception e) {
                    break;
                }

                dme = UNavigation.calcDme(LatValue, LonValue, currApt.lat, currApt.lon); // in nm

                // Apply selection criteria
                if (dme < 10)
                    nrAptsFound++;                                               // always show apts closer than 10nm
                else if ((nrAptsFound < MX_NR_APT) && (dme < AptSeekRange))
                    nrAptsFound++; // show all others up to MX_NR_APT for AptSeekRange
                else continue;  // we already have all the apts as we wish to display

                aptRelBrg = UNavigation.calcRelBrg(LatValue, LonValue, currApt.lat, currApt.lon, DIValue);

                x1 = project(aptRelBrg, dme, currApt.elev).x;
                y1 = project(aptRelBrg, dme, currApt.elev).y;
                renderAPTSymbol(matrix, x1, y1, wptId);


                if (Math.abs(dme) < Math.abs(_dme)) {
                    // closest apt (dme)
                    float absBrg = UNavigation.calcAbsBrg(LatValue, LonValue, currApt.lat, currApt.lon);
                    float relBrg = UNavigation.calcRelBrg(LatValue, LonValue, currApt.lat, currApt.lon, DIValue);

                    setAutoWptName(wptId);
                    setAutoWptComment(currApt.cmt);
                    setAutoWptDme(dme);
                    setAutoWptBrg(absBrg);
                    setAutoWptRelBrg(relBrg);
                    _dme = dme;
                }
            }
        }

        //
        // If we dont have the full compliment of apts expand the range incrementally
        // If do we have a full compliment start reducing the range
        // This also has the "useful" side effect of "flashing" new additions for a few cycles
        //
        if ((nrAptsFound < MX_NR_APT - 2) /*&& (Aptscounter++ % 10 == 0)*/ ) AptSeekRange += 1;
        else if ((nrAptsFound >= MX_NR_APT)) AptSeekRange -= 1;
        AptSeekRange = Math.min(AptSeekRange, MX_APT_SEEK_RNG);
    }

    protected void renderAPTSymbol(float[] matrix, float x1, float y1, String wptId)
    {
        float radius = pixM / 70;
        float z = zfloat;

        mPolyLine.SetWidth(3);
        mPolyLine.SetColor(theta*foreShadeR, theta*tapeShadeG, theta*foreShadeB, 1);  //purple'ish

        float[] vertPoly = {
                x1 + radius, y1, z,
                x1, y1 + radius, z,
                x1 - radius, y1, z,
                x1, y1 - radius, z,
                x1 + radius, y1, z
        };
        mPolyLine.VertexCount = 5;
        mPolyLine.SetVerts(vertPoly);  //crash here
        mPolyLine.draw(matrix);

        glText4.begin(theta*foreShadeR, theta*tapeShadeG, theta*foreShadeB, 1, matrix);  // purple'ish
        //glText.setScale(2.0f);
        glText4.drawCY(wptId, x1, y1 + glText4.getCharHeight() / 2);
        glText4.end();
    }


    //
    // Traffic targets
    //
    //protected StratuxWiFiTask mStratux;
    //protected OpenSkyTask mOpenSky;

    protected LinkedList<String> targets = null;
    public void setTargets(LinkedList<String> targetlist)
    {
        this.targets = targetlist;
    }



    protected void renderTargets(float[] matrix)
    {
        float z, x1, y1;

        z = zfloat;
        LinkedList<String> objs = targets;
        if (objs == null) return;

        for (String s : objs) {
            // sendDataToHelper(s);
            try {
                JSONObject jObject = new JSONObject(s);
                if (jObject.getString("type").contains("traffic")) {
                    float lon = (float)jObject.getDouble("longitude");
                    float lat = (float)jObject.getDouble("latitude");
                    float spd = (float)jObject.getDouble("speed");
                    float brg = (float)jObject.getDouble("bearing");
                    float alt = (float)jObject.getDouble("altitude"); // note: in feet
                    String call = (String) jObject.getString("callsign");

                    //renderACTSymbol(matrix, lon, lat, call);
                    // 0.16667 deg lat  = 10 nm
                    // 0.1 approx 5nm
                    float actRelBrg;
                    int tgtBrg = (int)brg;
                    int tgtSpd = (int)spd;

                    float tgtDme = UNavigation.calcDme(LatValue, LonValue, lat, lon); // in nm
                    actRelBrg = UNavigation.calcRelBrg(LatValue, LonValue, lat, lon, DIValue);

                    // 1nm +- 500 ft is same specs as ATD-300
                    if ((tgtDme < 1) && (Math.abs(alt - MSLValue) < 500)) {
                        proximityAlert = true;
                    }

                    x1 = project(actRelBrg, tgtDme, Unit.Feet.toMeter(alt)).x;
                    y1 = project(actRelBrg, tgtDme, Unit.Feet.toMeter(alt)).y;
                    renderTargetSymbol(matrix, x1, y1, call, alt, tgtBrg, tgtSpd, tgtDme);
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



    final float LEADER_TIME = 1f/60f;  // n min / 60. Ie set to 1 minute.

    protected void renderTargetSymbol(float[] matrix, float x1, float y1, String callsign, float tgtAlt, int tgtBrg, int tgtSpd, float tgtDme)
    {
        float radius = pixM / 60f;
        float z = zfloat;
        String tgtDmeLabel = UMath.round(tgtDme, 1) + " nm";
        String tgtAltLabel = Math.round(tgtAlt / 100) + " FL"; // convert to flight level

        // Scale lines and text size to distance
        // Choose the biggest used font and scale down
        if (tgtDme < 5) {
            mPolyLine.SetWidth(radius/2);
            mLine.SetWidth(radius/2);
            if (Math.abs(tgtAlt - MSLValue) < 1000) glText5.setScale(1);
            else glText5.setScale(0.8f);
        }
        else if (tgtDme < 10) {
            mPolyLine.SetWidth(radius/3);
            mLine.SetWidth(radius/3);
            glText5.setScale(0.8f);
        }
        else if (tgtDme < 15) {
            mPolyLine.SetWidth(radius/4);
            mLine.SetWidth(radius/4);
            glText5.setScale(0.4f);
        }
        else {
            mPolyLine.SetWidth(1/*radius/5*/);
            mLine.SetWidth(1/*radius/5*/);
            glText5.setScale(0); //0 = invisible
        }

        mPolyLine.SetColor(theta*foreShadeR, theta*foreShadeG, theta*foreShadeB, 1);  //white
        float[] vertPoly = {
                x1 + radius, y1 + radius, z,
                x1 - radius, y1 + radius, z,
                x1 - radius, y1 - radius, z,
                x1 + radius, y1 - radius, z,
                x1 + radius, y1 + radius, z
        };
        mPolyLine.VertexCount = 5;
        mPolyLine.SetVerts(vertPoly);
        mPolyLine.draw(matrix);

        // A target NEAR AND close to our altitude - Highlight it
        if ((tgtDme < 5) && (Math.abs(tgtAlt - MSLValue) < 500)) {
            if (tgtDme < 1)      mPolygon.SetColor(theta * foreShadeG, theta * foreShadeG, theta * foreShadeG, 1); // white  - danger
            else if (tgtDme < 2) mPolygon.SetColor(theta * foreShadeG, theta * foreShadeG, theta * backShadeB, 1); // yellow - caution
            else                 mPolygon.SetColor(theta * backShadeB, theta * foreShadeG, theta * backShadeB, 1); // green  - advisory

            mPolygon.VertexCount = 5;
            mPolygon.SetVerts(vertPoly);
            mPolygon.draw(matrix);
        }

        // Mask over for the Text area - just like Dimscreen
        mSquare.SetColor(backShadeR, backShadeG, backShadeB, 0.55f); //0.75f // black xper .. float alpha = 0.75f
        mSquare.SetWidth(2);

        float w = glText5.getLength(callsign);
        float h = 3f * glText5.getCharHeight();
        float m = glText5.getCharWidth('I');

        float[] squarePoly = {
                x1 - m, y1 - 1.2f*radius, z,
                x1 - m, y1 - h, z,
                x1 + w + m, y1 - h, z,
                x1 + w + m, y1 - 1.2f*radius, z
        };
        mSquare.SetVerts(squarePoly);
        mSquare.draw(matrix);

        // Draw the info text at target
        glText5.begin(theta*foreShadeR, theta*foreShadeG, theta*foreShadeB, 1, matrix);  // white'ish
            glText5.drawCY(callsign,    x1, y1 - glText5.getCharHeight());
            glText5.drawCY(tgtAltLabel, x1, y1 - 1.8f*glText5.getCharHeight());
            glText5.drawCY(tgtDmeLabel, x1, y1 - 2.6f*glText5.getCharHeight());
            glText5.setScale(1);
        glText5.end();

        //
        // Track/speed line
        //

        mLine.SetColor(theta * foreShadeR, theta * foreShadeG, theta * foreShadeB, 1); // white

        float x2 = x1 + mMapZoom * (tgtSpd * LEADER_TIME * UTrig.icos(90-(int)(tgtBrg - DIValue)));
        float y2 = y1 + mMapZoom * (tgtSpd * LEADER_TIME * UTrig.isin(90-(int)(tgtBrg - DIValue)));
        mLine.SetVerts(
                x1, y1, z,
                x2, y2, z
        );
        mLine.draw(matrix);

        // Text at stem
        /*glText.begin(theta*foreShadeR, theta*foreShadeG, theta*foreShadeB, 1, matrix);  // white'ish
            glText.drawCY(callsign, x2, y2 - 0*glText.getCharHeight());
            glText.drawCY(alt, x2, y2 - 0.8f*glText.getCharHeight());
        glText.end();*/
    }

    //
    // Continuously Calculated Impact Point
    //
    protected void renderCCIP(float[] matrix)
    {
        float z, x1, y1;

        z = zfloat;
        // s = ut + 1/2 at^2
        // t = (-u +- sqrt(u^2 -2gs)) / g

        float u = VSIValue / 60f; // in ft/sec
        float s = AGLValue;       // in feet

        float ty = (float) (-u + Math.sqrt(u * u + 2 * UNavigation.EARTH_G_F * s)) / UNavigation.EARTH_G_F;  // y time in seconds
        float d = (IASValue * ty / 3600);
        //Log.v("kwik", "ty: " + ty + " d: " + d);

        Coordinate latlon = UNavigation.calcOffsetCoor(LatValue, LonValue, DIValue, d);
        float alt = DemGTOPO30.getElev(latlon.Latitude, latlon.Longitude);

        //renderACTSymbol(matrix, lon, lat, call);
        // 0.16667 deg lat  = 10 nm
        // 0.1 approx 5nm
        float actRelBrg;
        int tgtBrg = (int) 0;

        float tgtDme = UNavigation.calcDme(LatValue, LonValue, latlon.Latitude, latlon.Longitude); // in nm
        actRelBrg = UNavigation.calcRelBrg(LatValue, LonValue, latlon.Latitude, latlon.Longitude, DIValue);

        x1 = project(actRelBrg, tgtDme, Unit.Feet.toMeter(alt)).x;
        y1 = project(actRelBrg, tgtDme, Unit.Feet.toMeter(alt)).y;
        renderCCIPSymbol(matrix, x1, y1, alt, tgtBrg, tgtDme);
    }

    protected void renderCCIPSymbol(float[] matrix, float x1, float y1, float tgtAlt, int tgtBrg, float tgtDme)
    {
        float radius = pixM / 50f;  /// apt is 70
        float z = zfloat;

        mPolyLine.SetWidth(3);
        //mPolyLine.SetColor(theta*foreShadeR, theta*foreShadeG, theta*foreShadeB, 1);
        mPolyLine.SetColor(theta*backShadeR, theta*tapeShadeG, theta*backShadeB, 1);

        float[] vertPoly = {
                x1 + radius, y1, z,
                x1, y1 + radius, z,
                x1 - radius, y1, z,
                x1, y1 - radius, z,
                x1 + radius, y1, z
        };
        mPolyLine.VertexCount = 5;
        mPolyLine.SetVerts(vertPoly);
        mPolyLine.draw(matrix);

        radius = pixM / 20f;

        mLine.SetWidth(3);
        //mLine.SetColor(theta*foreShadeR, theta*foreShadeG, theta*foreShadeB, 1);
        mLine.SetColor(theta*backShadeR, theta*foreShadeG, theta*backShadeB, 1);
        mLine.SetVerts(x1 - radius, y1, z,
                       x1 + radius, y1, z);
        mLine.draw(matrix);
        mLine.SetVerts(x1, y1 - radius, z,
                       x1, y1 + radius, z);
        mLine.draw(matrix);

        //glText4.begin(theta*foreShadeR, theta*foreShadeG, theta*foreShadeB, 1, matrix);  // gray'ish?
        //glText4.draw(callsign, x1, y1 + glText4.getCharHeight() / 2);
        //glText4.end();
    }




    private String mActiveDevice = "NONE";
    public void setActiveDevice(String device)
    {
        if (device != null)
          mActiveDevice = device;
        else
            mActiveDevice = "NONE";
    }


    /*
    // this must be overridden in the child classes
    abstract protected Point project(float x, float y)
    {
        return new Point(0, 0);
    }

    // this must be overridden in the child classes
    abstract protected Point project(float relbrg, float dme, float elev)
    {
        return new Point(0, 0);
    }
    */


    // this must be overridden in the child classes
    abstract protected Point project(float x, float y);

    // this must be overridden in the child classes
    // relbrg in degrees
    // dme in nm
    // elev in feet
    abstract protected Point project(float relbrg, float dme, float elev);



    //-------------------------------------------------------------------------
    // Synthetic Vision
    //
    private void __getColor(short c)
    {
        float red;
        float blue;
        float green;

        float r = 600;
        float r2 = r * 2;

        green = (float) c / r;
        if (green > 1) {
            green = 1;
            red = (c - r) / r;
            if (red > 1) {
                red = 1;
                blue = (c - r2) / r;
                if (blue > 1) {
                    blue = 1;
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    // DemGTOPO30 Sky.
    //
    protected void renderDEMSky1(float[] matrix)
    {
        float pixPitchViewMultiplier, pixOverWidth, z;

        pixPitchViewMultiplier = 90.0f / pitchInView * pixH;
        pixOverWidth = pixW2 * 1.80f;
        z = zfloat;

        // Sky - simple
        // max: -0.05 to 180 pitch
        float overlap;
        if (AGLValue > 0) overlap = 0.1f;
        else overlap = 0.0f;

        // Handle Monochrome
        if (colorTheme == 2) mSquare.SetColor(0, 0, 0, 1); //black
        else mSquare.SetColor(gamma * 0.10f, gamma * 0.20f, gamma * 0.30f, 1); //blue

        mSquare.SetWidth(1);
        {
            float[] squarePoly = {
                    -pixOverWidth, -overlap * pixPitchViewMultiplier, z,
                    +pixOverWidth, -overlap * pixPitchViewMultiplier, z,
                    +pixOverWidth, 2.0f * pixPitchViewMultiplier, z,
                    -pixOverWidth, 2.0f * pixPitchViewMultiplier, z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }
    }

    protected void renderDEMSky(float[] matrix)
    {
        float pixPitchViewMultiplier, pixOverWidth, z;

        pixPitchViewMultiplier = 90.0f / pitchInView * pixH;
        pixOverWidth = pixW2 * 1.80f;
        z = zfloat;

        // Sky - simple
        // max: -0.05 to 180 pitch
        float overlap;
        if (AGLValue > 0) overlap = MSLValue/100000f;
        else overlap = 0.0f;

        // Maybe allow no altitide compensated overlap at all?
        //overlap = 0.0f;  // maybe good idea?

        // Earth
        // Level to -180 pitch
        // Handle Monochrome
        //if (colorTheme == 2) mSquare.SetColor(0, 0.2f, 0, 1); //green
        //else mSquare.SetColor(gamma * 0.30f, gamma * 0.20f, gamma * 0.10f, 1); //brown (3,2,1)
        mSquare.SetColor(0, 0, 0, 1); //black
        mSquare.SetWidth(1);
        {
            float[] squarePoly = {
                    -pixOverWidth, -2.0f * pixPitchViewMultiplier, z,
                    +pixOverWidth, -2.0f * pixPitchViewMultiplier, z,
                    +pixOverWidth, 0.0f, z,
                    -pixOverWidth, 0.0f, z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }

        // Sky
        // Level to 180 pitch
        // TODO: 2017-10-31 make parameterised

        // Handle Monochrome
        if (colorTheme == 2) mSquare.SetColor(0, 0, 0, 1); //black
        else mSquare.SetColor(gamma * 0.10f, gamma * 0.20f, gamma * 0.30f, 1); //blue (1,2,3)
        mSquare.SetWidth(1);
        {
            float[] squarePoly = {
                    //-pixOverWidth, 0.0f, z,
                    //+pixOverWidth, 0.0f, z,
                    -pixOverWidth, -overlap * pixPitchViewMultiplier, z,
                    +pixOverWidth, -overlap * pixPitchViewMultiplier, z,
                    +pixOverWidth, 2.0f * pixPitchViewMultiplier, z,
                    -pixOverWidth, 2.0f * pixPitchViewMultiplier, z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }


    }




    // This is only good for debugging
    // It is very slow
    public void renderDEMBuffer(float[] matrix)
    {
        float z = zfloat;
        int x;
        int y;

        int maxx = DemGTOPO30.BUFX;
        int maxy = DemGTOPO30.BUFY;

        for (y = 0; y < maxy /*BUFY*/; y++) {
            for (x = 0; x < maxx /*BUFX*/; x++) {
                DemColor color = DemGTOPO30.getColor(DemGTOPO30.buff[x][y]);
                mLine.SetColor(color.red, color.green, color.blue, 1);  // rgb
                mLine.SetWidth(1);
                mLine.SetVerts(
                        x - maxx / 2, -y + pixH2 / 10, z,
                        x - maxx / 2 + 1, -y + pixH2 / 10, z
                );
                mLine.draw(matrix);
            }
        }
    }


    //-------------------------------------------------------------------------
    // Highway in the Sky (HITS)
    //
    protected void renderHITS(float[] matrix)
    {
        float z, pixPerDegree, x1, y1;
        float radius;
        float gateDme;
        float hitRelBrg;
        final float altMult = 10;

        pixPerDegree = pixM / pitchInView;
        z = zfloat;

        float pinDme = UMath.trunc(UNavigation.calcDme(LatValue, LonValue, mPinLat, mPinLon));

        for (float i = 0; i < Math.min(mSelWptDme, 100); i++) {

            float hitLat;
            float hitLon;
            float latDepCorrection = (float) Math.cos(Math.toRadians((mSelWptLat + LatValue) / 2));

            // Not really necessary - Proper correction for latitude is normally adequate and QDR could be used alone
            // Only over very large distances is this useful
            //if (pinDme > mSelWptDme) {
            //if (true) {
            if (mSelWptDme < UNavigation.EARTH_CIRC_8) {   // 2700
                // Draw from the waypoint to me - QDR
                hitLat = mSelWptLat + (mSelWptDme - i) / 60 * (float) UTrig.icos((int) (mObsValue - 180));
                hitLon = mSelWptLon + (mSelWptDme - i) / 60 * (float) UTrig.isin((int) (mObsValue - 180))  / latDepCorrection;
            }
            else {
                // Draw from me to the waypoint - QDM
                hitLat = mPinLat + (pinDme + i) / 60 * (float) UTrig.icos((int) (mObsValue));
                hitLon = mPinLon + (pinDme + i) / 60 * (float) UTrig.isin((int) (mObsValue)) / latDepCorrection;
            }

            gateDme = UNavigation.calcDme(LatValue, LonValue, hitLat, hitLon);
            hitRelBrg = UNavigation.calcRelBrg(LatValue, LonValue, hitLat, hitLon, DIValue); // the relative bearing to the hitpoint
            radius = 0.1f * pixM2 / gateDme;
            float skew = (float) Math.cos(Math.toRadians(hitRelBrg));  // to misquote William Shakespeare, this may be gilding the lily?

            // Project call does not buy us anything... Just do it manually
            x1 = hitRelBrg * pixPerDegree;
            y1 = (float) (-Math.toDegrees(UTrig.fastArcTan2(MSLValue - mAltSelValue, Unit.NauticalMile.toFeet(gateDme))) * pixPerDegree * altMult);

            // De-clutter the gates
            // Limit the gates to 6 only when close to the altitude
            //if ((Math.abs(y1) < 5)
            //  && (i > 10)
            //  && (i > pinDme + 6)) continue; // QDM

            mPolyLine.SetWidth(3);
            mPolyLine.SetColor(0.0f, tapeShadeG, tapeShadeB, 1);   // darker cyan

            float rx = 3.0f * radius * skew;
            float ry = 2.0f * radius;

            float[] vertPoly = {
                    x1 - rx, y1 - ry, z,
                    x1 + rx, y1 - ry, z,
                    x1 + rx, y1 + ry, z,
                    x1 - rx, y1 + ry, z,
                    x1 - rx, y1 - ry, z
            };

            // window frame
            mPolyLine.VertexCount = 5;
            mPolyLine.SetVerts(vertPoly);
            mPolyLine.draw(matrix);

            // faded window
            mPolygon.SetColor(tapeShadeR/10, tapeShadeG/10, tapeShadeB/10, 0f);
            mPolygon.VertexCount = 5;
            mPolygon.SetVerts(vertPoly);
            mPolygon.draw(matrix);
        }
    }


    //-------------------------------------------------------------------------
    // Radio Alitimeter (agl)
    //
    // There are two events that can change agl: setLatLon and setAlt
    // This function is called directly by them.
    //
    public void setAGL(int agl)
    {
        AGLValue = agl;
        if ((AGLValue <= 0) && (IASValue < AircraftData.Vx) && DemGTOPO30.demDataValid) {  // was Vs0
            // Handle taxi as a special case
            MSLValue = 1 + (int) Unit.Meter.toFeet(DemGTOPO30.getElev(LatValue, LonValue));  // Add 1 extra ft to esure we "above the ground"
            AGLValue = 1;  // Just good form, it will get changed on the next update
        }
    }

    //-------------------------------------------------------------------------
    // Geographic coordinates (lat, lon)
    // in decimal degrees
    //
    public void setLatLon(float lat, float lon)
    {
        LatValue = lat;
        LonValue = lon;
    }

    //-------------------------------------------------------------------------
    // Turn Indicator
    //
    protected void renderTurnMarkersRad(float[] matrix)
    {
        final float STD_RATE = 0.0524f; // = rate 1 = 3deg/s
        float z;

        z = zfloat;

        // rate of turn box
        mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // grey
        mLine.SetWidth(4);
        mLine.SetVerts(
                -STD_RATE * 4 * pixM2, -0.8f * pixH2 - 10, z,
                -STD_RATE * 4 * pixM2, -0.8f * pixH2 + 10, z
        );
        mLine.draw(matrix);

        mLine.SetVerts(
                STD_RATE * 4 * pixM2, -0.8f * pixH2 - 10, z,
                STD_RATE * 4 * pixM2, -0.8f * pixH2 + 10, z
        );
        mLine.draw(matrix);

        // rate of turn bar
        mLine.SetColor(0, 0.8f, 0, 1); // green
        mLine.SetWidth(15);
        mLine.SetVerts(
                0, -0.8f * pixH2, z,
                ROTValue * 4 * pixM2, -0.8f * pixH2, z
        );
        mLine.draw(matrix);
    }

    protected void renderTurnMarkers(float[] matrix)
    {
        final float STD_RATE = 3 * 0.066f; // = rate 1 = 3deg/s
        float z;

        z = zfloat;

        // rate of turn box
        mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // grey
        mLine.SetWidth(4);
        mLine.SetVerts(
                -STD_RATE * pixM2, -0.8f * pixH2 - 10, z,  //  / 57.29f * 4
                -STD_RATE * pixM2, -0.8f * pixH2 + 10, z
        );
        mLine.draw(matrix);

        mLine.SetVerts(
                STD_RATE * pixM2, -0.8f * pixH2 - 10, z,
                STD_RATE * pixM2, -0.8f * pixH2 + 10, z
        );
        mLine.draw(matrix);

        // rate of turn bar
        mLine.SetColor(0, foreShadeG, 0, 1); // green
        mLine.SetWidth(15);
        mLine.SetVerts(
                0, -0.8f * pixH2, z,
                ROTValue * 0.066f * 4 * pixM2, -0.8f * pixH2, z
        );
        mLine.draw(matrix);
    }



    public void setTurn(float rot)
    {
        ROTValue = rot;
    }

    //
    // Percentage battery remaining
    //
    protected void renderBatteryPct(float[] matrix)
    {
        String s = String.format("BAT %3.0f", BatteryPct * 100) + "%";
        if (BatteryPct > 0.1) glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        else glText4.begin(0, foreShadeG, foreShadeB, 1.0f, matrix); // cyan

        //glText.setScale(2.0f);
        glText4.draw(s, -0.97f * pixW2, (lineAncillaryDetails - 0.2f) * pixM2 - glText4.getCharHeight() / 2); // as part of the ancillary group
        glText4.end();
    }

    public void setBatteryPct(float value)
    {
        BatteryPct = value;
    }

    protected void renderGForceValue(float[] matrix)
    {
        String t = String.format("G %03.1f", GForceValue);
        glText6.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        glText6.draw(t, -0.97f * pixW2, -0.94f * pixH2 - glText6.getCharHeight() / 2);
        glText6.end();
    }

    public void setGForce(float value)
    {
        GForceValue = value;
    }

    private String mAutoWptName = "ZZZZ";
    public void setAutoWptName(String wpt)
    {
        mAutoWptName = wpt;
    }

    private String mAutoWptComment = "Null Island";
    public void setAutoWptComment(String wpt)
    {
        mAutoWptComment = wpt.split(",")[0]; //.substring(0, 12);
    }

    private float mAutoWptBrg;
    protected void setAutoWptBrg(float brg)
    {
        mAutoWptBrg = brg;
    }

    protected float mSelWptBrg;           // Selected waypoint Bearing (deg)
    protected float mSelWptRlb;           // Selected waypoint Relative bearing
    protected float mSelWptDme;           // Selected waypoint Dme distance (nm)
    private void setSelWptBrg(float brg)
    {
        mSelWptBrg = brg;
    }

    public float getSelWptBrg()
    {
        return mSelWptBrg;
    }

    private void setSelWptDme(float dme)
    {
        mSelWptDme = dme;
    }

    public float getSelWptDme()
    {
        return mSelWptDme;
    }

    private void setSelWptRelBrg(float rlb)
    {
        mSelWptRlb = rlb;
    }

    //
    // Display all the relevant auto wpt information with
    // A combo function to replace the individual ones
    //
    protected float lineAutoWptDetails;  // Auto Wpt - Set in onSurfaceChanged

    protected void renderAutoWptDetails(float[] matrix)
    {
        String s;
        float z = 0;
        float x1 = -0.97f * pixW2;

        glText4.begin(theta * foreShadeR, theta * foreShadeG, theta * backShadeB, 1, matrix); // light yellow

        s = String.format("%s", mAutoWptName);
        glText4.draw(s, x1, (lineAutoWptDetails - 0.0f) * pixM2 - glText4.getCharHeight() / 2);

        s = String.format("%s", mAutoWptComment);
        glText4.draw(s, x1, (lineAutoWptDetails - 0.1f) * pixM2 - glText4.getCharHeight() / 2);
        glText4.end();

        s = String.format("BRG  %03.0f", mAutoWptBrg);
        glText4.draw(s, x1, (lineAutoWptDetails - 0.2f) * pixM2 - glText4.getCharHeight() / 2);

        s = String.format("DME %03.1f", mAutoWptDme);
        glText4.draw(s, x1, (lineAutoWptDetails - 0.3f) * pixM2 - glText4.getCharHeight() / 2);
        glText4.end();
    }

    protected void dimAutoWptDetails(float[] matrix, float alpha)
    {
        float z = zfloat;
        float x1 = -0.99f * pixW2;
        float y1 = (lineAutoWptDetails + 0.1f) * pixM2 - glText4.getCharHeight()/2;

        // Mask over the info display area
        mSquare.SetColor(backShadeR, backShadeG, backShadeB, alpha);
        mSquare.SetWidth(2);
        {
            float[] squarePoly = {
                    x1,               y1, z,
                    x1 + pixM2*0.33f, y1, z,
                    x1 + pixM2*0.33f, (lineAutoWptDetails - 0.3f) * pixM2 - glText4.getCharHeight() / 2, z,  // Auto Wpt DME
                    x1,               (lineAutoWptDetails - 0.3f) * pixM2 - glText4.getCharHeight() / 2, z   // Auto Wpt DME
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }
    }



    //-------------------------------------------------------------------------
    // Display all the relevant ancillary device information with
    // A combo function to replace the individual ones
    //
    protected float lineAncillaryDetails;  // Ancillary Details - Set in onSurfaceChanged

    protected void renderAncillaryDetails(float[] matrix)
    {
        String s;

        glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white

        s = mGpsStatus;
        glText4.draw(s, -0.97f * pixW2, (lineAncillaryDetails - 0.3f) * pixM2 - glText4.getCharHeight() / 2);

        s = String.format("RNG %d   #AP %d", AptSeekRange, nrAptsFound);
        glText4.draw(s, -0.97f * pixW2, (lineAncillaryDetails - 0.4f) * pixM2 - glText4.getCharHeight() / 2);

        s = String.format("%c%03.2f %c%03.2f", (LatValue < 0) ? 'S' : 'N', Math.abs(LatValue), (LonValue < 0) ? 'W' : 'E', Math.abs(LonValue));
        glText4.draw(s, -0.97f * pixW2, (lineAncillaryDetails - 0.5f) * pixM2 - glText4.getCharHeight() / 2);

        //s = String.format("DEV %s", mActiveDevice);
        s = mActiveDevice;
        glText4.draw(s, -0.97f * pixW2, (lineAncillaryDetails - 0.6f) * pixM2 - glText4.getCharHeight() / 2);

        glText4.end();
    }

    // Include the map scaler and G Meter as well
    protected void dimAcillaryDetails(float[] matrix, float alpha)
    {
        float z = zfloat;
        float x1 = -0.99f * pixW2;
        float y1 = (lineAncillaryDetails - 0.1f) * pixM2 - glText4.getCharHeight() / 2;

        // Mask over the info display area
        mPolygon.SetWidth(1);
        mPolygon.SetColor(backShadeR, backShadeG, backShadeB, alpha);
        {
            float[] vertPoly = {
                    // Clockwise from top left
                    x1,                y1, z,
                    x1 + pixM2*0.4f,   y1, z,
                    x1 + pixM2*0.6f,   y1 - glText4.getCharHeight()*2.5f, z,
                    x1 + pixM2*0.6f,   -0.94f * pixH2 - glText6.getCharHeight() / 2, z, // G Meter
                    x1,                -0.94f * pixH2 - glText6.getCharHeight() / 2, z  // G Meter
            };
            mPolygon.VertexCount = 5;//+2;
            mPolygon.SetVerts(vertPoly);
            mPolygon.draw(matrix);
        }
    }



    private String mGpsStatus; // = "GPS: 10 / 11";

    public void setGpsStatus(String gpsstatus)
    {
        mGpsStatus = gpsstatus;
    }

    public String mSelWptName = "ZZZZ";
    public String mSelWptComment = "Null Island";
    public float mSelWptLat = 00f;
    public float mSelWptLon = 00f;
    public float mAltSelValue = 0;
    public String mAltSelName = "00000";
    protected float leftC;          // Selected Wpt - Set in onSurfaceChanged
    protected float lineC;          // Selected Wpt - Set in onSurfaceChanged
    protected float selWptDec;
    protected float selWptInc;
    public float mObsValue;
    public float mPinLat = 00f;
    public float mPinLon = 00f;

    protected float spinnerStep = 0.10f;    // spacing between the spinner buttons
    protected int   spinnerTextScale = 1;
    public boolean fatFingerActive = false;

    public float commandPitch;  // in degrees
    public float commandRoll;   // in degrees


    protected void renderSelWptValue(float[] matrix)
    {
        float z = zfloat;
        float size = spinnerStep * 0.2f;

        // Draw the selecting triangle spinner buttons
        mTriangle.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);  // gray
        for (int i = 0; i < 4; i++) {
            float xPos = (leftC + i * spinnerStep);

            mTriangle.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);  // white backShade
            if ((inc < 0) && (i == pos))  mTriangle.SetColor(foreShadeR, tapeShadeG, foreShadeB, 1); // highlight tapped triangle
            mTriangle.SetVerts((xPos - size) * pixW2, selWptDec, z,
                    (xPos + size) * pixW2, selWptDec, z,
                    (xPos + 0) * pixW2, selWptDec + 2 * size * pixM2, z);
            mTriangle.draw(matrix);

            mTriangle.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);  // white backShade
            if ((inc > 0) && (i == pos))  mTriangle.SetColor(foreShadeR, tapeShadeG, foreShadeB, 1);
            mTriangle.SetVerts((xPos - size) * pixW2, selWptInc, z,
                    (xPos + size) * pixW2, selWptInc, z,
                    (xPos + 0) * pixW2, selWptInc - 2 * size * pixM2, z);
            mTriangle.draw(matrix);

            // Draw the individual select characters
            if (mSelWptName != null) {
                glText6.begin(foreShadeR, tapeShadeG, foreShadeB, 1.0f, matrix);
                glText6.setScale(spinnerTextScale);
                String s = String.format("%c", mSelWptName.charAt(i));
                glText6.drawCX(s, xPos * pixW2, ((selWptInc + selWptDec) / 2) - (glText6.getCharHeight() / 2));
                glText6.end();
                glText6.setScale(1);
            }
        }

        // Calculate the relative bearing to the selected wpt
        float dme = UNavigation.calcDme(LatValue, LonValue, mSelWptLat, mSelWptLon); // in nm
        float relBrg = UNavigation.calcRelBrg(LatValue, LonValue, mSelWptLat, mSelWptLon, DIValue);

        // Update the flight director data
        // Calculate how many degrees of pitch to command
        float deltaAlt = mAltSelValue - MSLValue;

        commandPitch = (IASValue - AircraftData.Vy) / 5 * (deltaAlt / 1000);
        commandPitch = UMath.clamp(commandPitch, -15, +15);  // Garmin spec 15 deg pitch and 30 deg roll
        commandRoll = relBrg;
        commandRoll = UMath.clamp(commandRoll, -30, +30);    // Garmin spec 15 deg pitch and 30 deg roll

        setFlightDirector(displayFlightDirector, commandPitch, commandRoll);

        // BRG
        float absBrg = UNavigation.calcAbsBrg(LatValue, LonValue, mSelWptLat, mSelWptLon);

        // Setting data in this renderer does not make much logical sense. This could be re-factored
        // Perhaps introduce a new function to explicitly handle "navigation"?
        setSelWptBrg(absBrg);
        setSelWptDme(dme);  // todo: refactor out
        setSelWptRelBrg(relBrg);
    }

    private void dimScreen(float[] matrix, float alpha)
    {
        float z = zfloat;
        // Mask over the PFD for the input area
        mSquare.SetColor(backShadeR, backShadeG, backShadeB, alpha); // black xper .. 0.75f
        mSquare.SetWidth(2);
        {
            float[] squarePoly = {
                    -pixW2 + 5, +pixH2 - 5, z,
                    -pixW2 + 5, -pixH2 + 5, z,
                    +pixW2 - 5, -pixH2 + 5, z,
                    +pixW2 - 5, +pixH2 - 5, z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }
    }


    protected void renderSelWptDetails(float[] matrix)
    {
        float z = zfloat;
        String s;

        // This may need to be in a function
        if (fatFingerActive) {
            dimScreen(matrix, 0.75f);
        }

        //glText.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        // Name
        s = mSelWptComment;
        glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
        glText4.setScale(spinnerTextScale);
        glText4.draw(s, leftC * pixW2, lineC * pixH2 - 0.5f * glText4.getCharHeight());
        glText4.setScale(1);
        glText4.end();

        // BRG
        s = String.format("BRG  %03.0f", mSelWptBrg);
        glText5.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // temp
        glText5.setScale(spinnerTextScale);
        glText5.draw(s, leftC * pixW2, lineC * pixH2 - 1.5f * glText5.getCharHeight());
        glText5.setScale(1);
        glText5.end();

        // DME
        s = String.format("DME %03.1f", mSelWptDme);  // in nm
        glText5.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // temp
        glText5.setScale(spinnerTextScale);
        glText5.draw(s, leftC * pixW2, lineC * pixH2 - 2.5f * glText5.getCharHeight());
        glText5.setScale(1);
        glText5.end();

        /*
		// Guide lines for fatfinger ... ?
        mLine.SetColor(0.9f, 0.9f, 0.9f, 0); //white
        mLine.SetVerts(leftC * pixW2 - 5, lineC * pixH2 + 0.75f*glText.getCharHeight(), z,
                               pixW2 - 5, lineC * pixH2 + 0.75f*glText.getCharHeight(), z);
        mLine.draw(matrix);
        mLine.SetVerts(leftC * pixW2 - 5, lineC * pixH2 - 2.70f*glText.getCharHeight(), z,
                               pixW2 - 5, lineC * pixH2 - 2.70f*glText.getCharHeight(), z);
        mLine.draw(matrix);
		*/
    }


    protected void dimSelWptDetails(float[] matrix, float alpha)
    {
        float z = zfloat;
        float x1;
        float y1;

        // Selected Waypoint
        x1 = (leftC - 0.05f) * pixW2;
        y1 = selWptDec + glText6.getCharHeight()/2;

        mSquare.SetColor(backShadeR, backShadeG, backShadeB, alpha);
        mSquare.SetWidth(2);
        {
            float[] squarePoly = {
                    x1, y1+5, z,
                    x1,              lineC * pixH2 - 2.5f * glText5.getCharHeight(), z,  // DME
                    x1 + 0.8f*pixM2, lineC * pixH2 - 2.5f * glText5.getCharHeight(), z,  // DME
                    x1 + 0.8f*pixM2, y1+5, z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }
    }


    protected float selAltInc;
    protected float selAltDec;

    protected void renderSelAltValue(float[] matrix)
    {
        float z;
        z = zfloat;
        float size = spinnerStep * 0.2f;

        // Draw the selecting triangle spinner buttons
        mTriangle.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);  // white
        for (int i = 0; i < 3; i++) {
            float xPos = (leftC + i * spinnerStep);

            if ((ina < 0) && (i == pos))  mTriangle.SetColor(foreShadeR, tapeShadeG, foreShadeB, 1); //
            else mTriangle.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);  // white backShade

            mTriangle.SetVerts((xPos - size) * pixW2, selAltDec, z,
                    (xPos + size) * pixW2, selAltDec, z,
                    (xPos + 0.00f) * pixW2, selAltDec + 2 * size * pixM2, z);
            mTriangle.draw(matrix);

            if ((ina > 0) && (i == pos))  mTriangle.SetColor(foreShadeR, tapeShadeG, foreShadeB, 1); //
            else mTriangle.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);  // white backShade

            mTriangle.SetVerts((xPos - size) * pixW2, selAltInc, z,
                    (xPos + size) * pixW2, selAltInc, z,
                    (xPos + 0.00f) * pixW2, selAltInc - 2 * size * pixM2, z);
            mTriangle.draw(matrix);

            // Draw the individual select characters
            if (mAltSelName != null) {
                glText6.begin(foreShadeR, tapeShadeG, foreShadeB, 1, matrix);
                glText6.setScale(spinnerTextScale);
                String s = String.format("%c", mAltSelName.charAt(i));
                glText6.drawCX(s, xPos * pixW2, ((selAltInc + selAltDec) / 2) - glText6.getCharHeight() / 2);
                glText6.setScale(1);
                glText6.end();
            }
        }

        float xPos = (leftC + 2.6f * spinnerStep);
        glText5.begin(foreShadeR, tapeShadeG, foreShadeB, 1, matrix);
        glText5.setScale(spinnerTextScale);
        String s = "F L";  // "X100 ft";
        glText5.draw(s, xPos * pixW2, ((selAltInc + selAltDec) / 2) - glText5.getCharHeight() / 2);
        glText5.setScale(1);
        glText5.end();
    }

    protected void dimSelAltValue(float[] matrix, float alpha)
    {
        float z = zfloat;
        float x1;
        float y1;

        // Selected Altitude
        x1 = (leftC - 0.05f) * pixW2;
        y1 = selAltDec + glText6.getCharHeight()/2;

        mSquare.SetColor(backShadeR, backShadeG, backShadeB, alpha);
        mSquare.SetWidth(2);
        {
            float[] squarePoly = {
                    x1, y1+5, z,
                    x1 + 0.8f*pixM2, y1, z,

                    x1 + 0.8f*pixM2, y1 - glText6.getCharHeight()*2.5f, z,
                    x1,              y1 - glText6.getCharHeight()*2.5f, z              };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }
    }




    public void setThemeDark()
    {
        colorTheme = 0;

        tapeShadeR = 0.60f; // grey
        tapeShadeG = 0.60f; // grey
        tapeShadeB = 0.60f; // grey

        foreShadeR = 0.99f; // white
        foreShadeG = 0.99f; // white
        foreShadeB = 0.99f; // white

        backShadeR = 0.01f; // black
        backShadeG = 0.01f; // black
        backShadeB = 0.01f; // black
        gamma = 1;
        theta = 1;
        DemGTOPO30.setGamma(gamma);
    }

    public void setThemeLight()
    {
        colorTheme = 1;

        tapeShadeR = 0.40f; // grey
        tapeShadeG = 0.40f; // grey
        tapeShadeB = 0.40f; // grey

        foreShadeR = 0.01f; // black
        foreShadeG = 0.01f; // black
        foreShadeB = 0.01f; // black

        backShadeR = 0.99f; // white
        backShadeG = 0.99f; // white
        backShadeB = 0.99f; // white

        gamma = 4.0f;
        theta = 0.6f;
        DemGTOPO30.setGamma(gamma);
    }

    public void setThemeGreen()
    {
        colorTheme = 2;

        tapeShadeR = 0.00f; // grey
        tapeShadeG = 0.60f; // grey
        tapeShadeB = 0.00f; // grey

        foreShadeR = 0.00f; // white
        foreShadeG = 0.99f; // white
        foreShadeB = 0.00f; // white

        backShadeR = 0.01f; // black
        backShadeG = 0.01f; // black
        backShadeB = 0.01f; // black

        gamma = 1;
        theta = 2;  // this sorts out the purple in monochrome
        DemGTOPO30.setGamma(gamma);
    }



    //---------------------------------------------------------------------------
    // Handle the tap events
    // Action spinner controls
    //
    private int pos = -1; // set to invalid / no selection
    private int inc = 0;  // initialise to 0, also acts as a flag
    private int ina = 0;  // initialise to 0, also acts as a flag

    public void setActionDown(float x, float y)
    {
        float mX = (x / pixW - 0.5f) * 2;
        // keypress location
        float mY = -(y / pixH - 0.5f) * 2;

        pos = -1; // set to invalid / no selection
        inc = 0;  // initialise to 0, also acts as a flag
        ina = 0;  // initialise to 0, also acts as a flag

        char[] wpt = mSelWptName.toCharArray();
        char[] alt = mAltSelName.toCharArray();

        // fat finger mode
        if ((Math.abs(mY - lineC + 0.1f) < 0.105) && (mX > leftC)) {
            fatFingerActive = !fatFingerActive;
            setSpinnerParams();
        }

        // Determine if we are counting up or down?
        // waypoint
        if (Math.abs(mY - selWptDec / pixH2) < 0.10) inc = -1;
        else if (Math.abs(mY - selWptInc / pixH2) < 0.10) inc = +1;

        // Determine if we are counting up or down?
        // altitude number
        else if (Math.abs(mY - selAltDec / pixH2) < 0.10) ina = -1;
        else if (Math.abs(mY - selAltInc / pixH2) < 0.10) ina = +1;

        // Determine which digit is changing
        for (int i = 0; i < 4; i++) {
            float xPos = (leftC + i * spinnerStep);

            if (Math.abs(mX - xPos) < spinnerStep / 2) {
                pos = i;
                break;
            }
        }

        //
        // Increment the appropriate digit if we got a valid tap (pos != -1)
        //
        if (pos > -1) {
            // Altitude
            if (ina != 0) {
                alt[pos] += ina;
                if (alt[pos] < '0') alt[pos] = '9';
                if (alt[pos] > '9') alt[pos] = '0';
                if (alt[0] > '2') alt[pos] = '0';
                mAltSelName = String.valueOf(alt);
                mAltSelValue = Float.parseFloat(mAltSelName);
            }

            // Waypoint
            if (inc != 0) {
                boolean found = false;
                int ctr = 0;

                // NOTE:
                // This can cause an endless loop unless ZZZZ is defined
                // in /kwikEFIS/ulib\src/main/assets/waypoint/airport.gpx.xml
                //
                while (!found) {
                    wpt[pos] += inc;
                    if (wpt[pos] < '0') wpt[pos] = 'Z';
                    if (wpt[pos] > 'Z') wpt[pos] = '0';
                    Log.v("kwik", "pos: " + pos + " wpt: " + wpt[0]+wpt[1]+wpt[2]+wpt[3]);

                    Iterator<Apt> it = Gpx.aptList.iterator();
                    try {
                        while (it.hasNext()) {
                            Apt currApt = it.next();

                            // Look for a perfect match
                            if (currApt.name.equals(String.valueOf(wpt))) {
                                // We found a perfect match
                                mSelWptName = currApt.name;
                                mSelWptComment = currApt.cmt;
                                mSelWptLat = currApt.lat;
                                mSelWptLon = currApt.lon;
                                found = true;
                                break;
                            }
                            // Look for a partial match
                            else if ((pos < 3) && currApt.name.startsWith(String.valueOf(wpt).substring(0, pos + 1))) {
                                // We found a partial match, fill in the rest for a first guess
                                for (int i = pos; i < 4; i++) wpt[i] = currApt.name.charAt(i);
                                mSelWptName = currApt.name;
                                mSelWptComment = currApt.cmt;
                                mSelWptLat = currApt.lat;
                                mSelWptLon = currApt.lon;
                                found = true;
                                break;
                            }
                            // No match at all
                            //else {
                            //    found = false;
                            //}
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }

                    // Abandon all hope after auto inc went thorugh all the valid characters
                    // (must be more than 0 - Z) = 42 -- This should never happen ?
                    if (ctr++ > 43) {
                        break;
                    }
                }

                // The selected waypoint has changed
                // Update the OBS and Pin position as well
                mObsValue = UNavigation.calcAbsBrg(LatValue, LonValue, mSelWptLat, mSelWptLon);
                mPinLat = LatValue;
                mPinLon = LonValue;
            }
        }
    }

    public void setActionUp(float x, float y)
    {
        inc = 0;  // initialise to 0, also acts as a flag
        ina = 0;  // initialise to 0, also acts as a flag
    }



        //-------------------------------------------------------------------------
    //
    // Auto Waypoint handlers
    //
    //-------------------------------------------------------------------------
    private float mAutoWptDme;
    private float mAutoWptRlb;

    public void setAutoWptDme(float dme)
    {
        mAutoWptDme = dme;
    }

    private void renderAutoWptRlb(float[] matrix)
    {
        String t = String.format("RLB  %03.0f", mAutoWptRlb);
        glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1, matrix); // white
        //glText.setScale(2.0f);
        glText4.draw(t, -0.97f * pixW2, -0.7f * pixM2 - glText4.getCharHeight() / 2);  // Draw  String
        glText4.end();
    }

    protected void setAutoWptRelBrg(float rlb)
    {
        mAutoWptRlb = rlb;
    }

    public void setPrefs(prefs_t pref, boolean value)
    {
        switch (pref) {
            case DEM:
                displayDEM = value;
                break;
            case AH_COLOR:
                displayAHColors = value;
                break;
            case TAPE:
                displayTape = value;
                break;
            case MIRROR:
                displayMirror = value;
                break;
            case INFO_PAGE:
                displayInfoPage = value;
                break;
            case FLIGHT_DIRECTOR:
                displayFlightDirector = value;
                break;
            case REMOTE_INDICATOR:
                displayRMI = value;
                break;
            case HITS:
                displayHITS = value;
                break;
            case AIRPORT:
                displayAirport = value;
                break;
            case AIRSPACE:
                displayAirspace = value;
                break;
            case WX:
                displayWX = value;
                break;
            case CCIP:
                displayCCIP = value;
                break;
        }
    }

    //-----------------------------------------------------------------------------
    //
    //  Remote Indicator
    //
    //-----------------------------------------------------------------------------

    protected float roseScale;
    //protected float roseTextScale;

    protected void renderFixedCompassMarkers(float[] matrix)
    {
        int i;
        float z, sinI, cosI;
        float roseRadius = roseScale * pixM2;

        z = zfloat;

        mLine.SetWidth(2);
        mLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);
        for (i = 0; i <= 315; i = i + 45) {

            if (i % 90 == 0) mLine.SetWidth(4);
            else mLine.SetWidth(2);

            sinI = UTrig.isin((450 - i) % 360);
            cosI = UTrig.icos((450 - i) % 360);
            mLine.SetVerts(
                    1.005f * roseRadius * cosI, 1.005f * roseRadius * sinI, z,
                    1.120f * roseRadius * cosI, 1.120f * roseRadius * sinI, z
            );
            mLine.draw(matrix);
        }

        // Apex marker
        mTriangle.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);
        mTriangle.SetVerts(
                0.035f * pixM2, 1.120f * roseRadius, z,
                -0.035f * pixM2, 1.120f * roseRadius, z,
                0.0f, roseRadius, z);
        mTriangle.draw(matrix);


    }

    protected void renderCompassRose(float[] matrix)
    {
        int i, j;
        float z, sinI, cosI;
        String t;
        float roseRadius = roseScale * pixM2;

        z = zfloat;

        mLine.SetWidth(2);
        mLine.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1);  // grey

        // The rose degree tics
        for (i = 0; i <= 330; i = i + 30) {
            sinI = UTrig.isin((450 - i) % 360);
            cosI = UTrig.icos((450 - i) % 360);
            mLine.SetVerts(
                    0.9f * roseRadius * cosI, 0.9f * roseRadius * sinI, z,
                    roseRadius * cosI, 1.0f * roseRadius * sinI, z
            );
            mLine.draw(matrix);

            // Note roseTextScale seems to always be = 1
            switch (i) {
                case 0:
                    t = "N";
                    glText9.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText9.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText9.end();
                    break;
                case 90 :
                    t = "E";
                    glText6.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText6.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText6.end();
                    break;
                case 180:
                    t = "S";
                    glText6.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText6.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText6.end();
                    break;
                case 270:
                    t = "W";
                    glText6.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText6.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText6.end();
                    break;
                case 30:
                    t = "3";
                    glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText4.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText4.end();
                    break;
                case 60:
                    t = "6";
                    glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText4.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText4.end();
                    break;
                case 120:
                    t = "12";
                    glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText4.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText4.end();
                    break;
                case 150:
                    t = "15";
                    glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText4.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText4.end();
                    break;
                case 210:
                    t = "21";
                    glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText4.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText4.end();
                    break;
                case 240:
                    t = "24";
                case 300:
                    t = "30";
                    glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText4.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText4.end();
                    break;
                case 330:
                    t = "33";
                    glText4.begin(foreShadeR, foreShadeG, foreShadeB, 1.0f, matrix); // white
                    glText4.drawC(t, 0.75f * roseRadius * cosI, 0.75f * roseRadius * sinI, -i); // angleDeg=90-i, Use 360-DIValue for vertical text
                    glText4.end();
                    break;
            }

            for (j = 10; j <= 20; j = j + 10) {
                sinI = UTrig.isin((i + j));
                cosI = UTrig.icos((i + j));
                mLine.SetVerts(
                        0.93f * roseRadius * cosI, 0.93f * roseRadius * sinI, z,
                        roseRadius * cosI, roseRadius * sinI, z
                );
                mLine.draw(matrix);
            }
            for (j = 5; j <= 25; j = j + 10) {
                sinI = UTrig.isin((i + j));
                cosI = UTrig.icos((i + j));
                mLine.SetVerts(
                        0.96f * roseRadius * cosI, 0.96f * roseRadius * sinI, z,
                        roseRadius * cosI, roseRadius * sinI, z
                );
                mLine.draw(matrix);
            }
        }
    }

    protected void _dimRose(float[] matrix, float alpha)
    {
        float z = zfloat;
        float roseRadius = roseScale * pixM2;
        float x1 = -0.99f * pixW2;
        float y1 = (lineAutoWptDetails + 0.1f) * pixM2 - glText4.getCharHeight()/2;

        // Mask over the info display area
        mPolygon.SetWidth(1);
        mPolygon.SetColor(backShadeR, backShadeG, backShadeB, 0.55f); // black xper .. 0.75f
        {
            float[] vertPoly = {
                    // Better fanning order - Clockwise from bottom left
                    x1,             y1 - glText4.getCharHeight()*12, z,
                    x1,             y1, z,
                    x1 + pixW2/3,   y1, z,
                    x1 + pixW2/3,   y1 - glText4.getCharHeight()*4, z,
                    x1 + pixW2/2,   y1 - glText4.getCharHeight()*6.5f, z,

                    //x1 + pixW2/2,   y1 - glText4.getCharHeight()*8.5f, z, //
                    //x1 + pixW2/3,   y1 - glText4.getCharHeight()*11, z,   //

                    //x1 + pixW2/3,   y1 - glText4.getCharHeight()*12, z
                    x1 + pixW2/2,   y1 - glText4.getCharHeight()*12, z
            };
            mPolygon.VertexCount = 6;//+2;
            mPolygon.SetVerts(vertPoly);
            mPolygon.draw(matrix);
        }


        x1 = 0.55f * pixW2;///2;
        y1 = selWptDec + glText4.getCharHeight()/2; //  (lineAutoWptDetails + 0.1f) * pixM2 - glText4.getCharHeight()/2;

        mSquare.SetColor(backShadeR, backShadeG, backShadeB, alpha); // black xper .. 0.75f
        mSquare.SetWidth(2);
        {
            float[] squarePoly = {
                    x1, y1+5, z,
                    x1 + 0.45f*pixW2-5, y1, z,
                    //x1 + 0.44f*pixW2, y1 - glText4.getCharHeight()*12, z,
                    //x1,               y1 - glText4.getCharHeight()*12, z
                    x1 + 0.44f*pixW2, -pixH2+5, z,
                    x1,               -pixH2+5, z
            };
            mSquare.SetVerts(squarePoly);
            mSquare.draw(matrix);
        }
    }


    //-------------------------------------------------------------------------
    // Render the two RMI needles
    //
    protected void renderBearing(float[] matrix)
    {
        float z, sinI, cosI, _sinI, _cosI;
        float roseRadius = roseScale * pixM2;

        z = zfloat;

        //
        // Bearing to Automatic Waypoint
        //
        mLine.SetWidth(5);
        mLine.SetColor(theta * foreShadeR, theta * foreShadeG, theta * backShadeB, 1);  // needle yellow

        sinI = 0.9f * UTrig.isin(90 - (int) mAutoWptBrg);
        cosI = 0.9f * UTrig.icos(90 - (int) mAutoWptBrg);
        _sinI = 0.5f * UTrig.isin(90 - (int) mAutoWptBrg);
        _cosI = 0.5f * UTrig.icos(90 - (int) mAutoWptBrg);

        //tail
        mLine.SetVerts(
                -roseRadius * _cosI, -roseRadius * _sinI, z,
                -roseRadius * cosI, -roseRadius * sinI, z
        );
        mLine.draw(matrix);
        // head
        // point L
        _sinI = 0.80f * UTrig.isin(90 - (int) mAutoWptBrg + 6);
        _cosI = 0.80f * UTrig.icos(90 - (int) mAutoWptBrg + 6);
        mLine.SetVerts(
                roseRadius * _cosI, roseRadius * _sinI, z,
                roseRadius * cosI, roseRadius * sinI, z
        );
        mLine.draw(matrix);

        // parallel
        float parr = 0.40f;

        float __sinI = parr * UTrig.isin(90 - (int) mAutoWptBrg + 12);
        float __cosI = parr * UTrig.icos(90 - (int) mAutoWptBrg + 12);
        mLine.SetVerts(
                roseRadius * _cosI, roseRadius * _sinI, z,
                roseRadius * __cosI, roseRadius * __sinI, z
        );
        mLine.draw(matrix);

        // point R
        _sinI = 0.80f * UTrig.isin(90 - (int) mAutoWptBrg - 5);
        _cosI = 0.80f * UTrig.icos(90 - (int) mAutoWptBrg - 5);
        mLine.SetVerts(
                roseRadius * _cosI, roseRadius * _sinI, z,
                roseRadius * cosI, roseRadius * sinI, z
        );
        mLine.draw(matrix);

        // parallel
        __sinI = parr * UTrig.isin(90 - (int) mAutoWptBrg - 12);
        __cosI = parr * UTrig.icos(90 - (int) mAutoWptBrg - 12);
        mLine.SetVerts(
                roseRadius * _cosI, roseRadius * _sinI, z,
                roseRadius * __cosI, roseRadius * __sinI, z
        );
        mLine.draw(matrix);

        //
        // Bearing to Selected Waypoint
        //
        mLine.SetWidth(8);
        mLine.SetColor(theta * foreShadeR, theta * 0.5f, theta * foreShadeB, 1); // green

        sinI = 0.9f * UTrig.isin(90 - (int) mSelWptBrg);
        cosI = 0.9f * UTrig.icos(90 - (int) mSelWptBrg);
        _sinI = 0.5f * UTrig.isin(90 - (int) mSelWptBrg);
        _cosI = 0.5f * UTrig.icos(90 - (int) mSelWptBrg);

        // tail
        mLine.SetVerts(
                -roseRadius * _cosI, -roseRadius * _sinI, z,
                -roseRadius * cosI, -roseRadius * sinI, z
        );
        mLine.draw(matrix);
        // head
        mLine.SetVerts(
                roseRadius * _cosI, roseRadius * _sinI, z,
                roseRadius * cosI, roseRadius * sinI, z
        );
        mLine.draw(matrix);

        mLine.SetWidth(6);
        // point
        _sinI = 0.80f * UTrig.isin(90 - (int) mSelWptBrg + 9);
        _cosI = 0.80f * UTrig.icos(90 - (int) mSelWptBrg + 9);
        mLine.SetVerts(
                roseRadius * _cosI, roseRadius * _sinI, z,
                roseRadius * cosI, roseRadius * sinI, z
        );
        mLine.draw(matrix);
        _sinI = 0.80f * UTrig.isin(90 - (int) mSelWptBrg - 8);
        _cosI = 0.80f * UTrig.icos(90 - (int) mSelWptBrg - 8);
        mLine.SetVerts(
                roseRadius * _cosI, roseRadius * _sinI, z,
                roseRadius * cosI, roseRadius * sinI, z
        );
        mLine.draw(matrix);
    }


    protected void renderBearingTxt(float[] matrix)
    {
        float roseRadius = roseScale * pixM2;
        float scale = 1.6f;  // not sure why this is > 1.0 Does not really make sense
        // it is somehow related to which matrix it is drawn on

        //
        // Bearing to Selected Waypoint
        //
        glText4.begin(foreShadeR * theta, theta * tapeShadeG, theta * foreShadeB, 1.0f, matrix); // purple'ish
        glText4.drawC(mSelWptName, 0.20f * roseRadius, -0.35f * roseRadius, 0); //0.12, 0
        glText4.end();

        //
        // Bearing to Automatic Waypoint
        //
        glText4.begin(foreShadeR * theta, foreShadeG * theta, backShadeB, 1.0f, matrix); // yellow
        glText4.drawC(mAutoWptName, -0.20f * roseRadius, -0.35f * roseRadius, 0);
        glText4.end();
    }

    //-------------------------------------------------------------------------
    // MFD specific members
    //

    public float mMapZoom = 20; // Zoom multiplier for map. 1 (max out) to 200 (max in)

    //-------------------------------------------------------------------------
    // Render the Direct To bearing line
    //
    protected void renderDctTrack(float[] matrix)
    {
        float z, x1, y1; //sinI, cosI, _sinI, _cosI;
        z = zfloat;

        //
        // Direct Track to Selected Waypoint
        //
        mLine.SetWidth(20);
        mLine.SetColor(0.45f, 0.45f, 0.10f, 0.125f); // yellow'ish --- B2 todo

        x1 = mMapZoom * (mSelWptDme * UTrig.icos(90 - (int) mSelWptRlb));
        y1 = mMapZoom * (mSelWptDme * UTrig.isin(90 - (int) mSelWptRlb));
        mLine.SetVerts(
                0, 0, z,
                x1, y1, z
        );
        mLine.draw(matrix);
        // Skunk stripe
        mLine.SetWidth(4);
        mLine.SetColor(0.0f, 0.0f, 0.0f, 1); // black
        mLine.draw(matrix);

        //
        // Direct Track to Automatic Waypoint
        //
        // /* I'm not sure I like this feature ...
        mLine.SetWidth(2);
        mLine.SetColor(theta * foreShadeR, theta * foreShadeG, theta * backShadeB, 1); // yellow

        x1 = mMapZoom * (mAutoWptDme * UTrig.icos(90-(int)mAutoWptRlb));
        y1 = mMapZoom * (mAutoWptDme * UTrig.isin(90-(int)mAutoWptRlb));
        mLine.SetVerts(
                0, 0, z,
                x1, y1, z
        );
        mLine.draw(matrix);
        // */

    }

    //-------------------------------------------------------------------------
    // Render a little airplane symbol
    //
    protected void renderACSymbol(float[] matrix, boolean bOwnShip)
    {
        float z;
        z = zfloat;

        float wx = 0.10f * pixM2;
        float wy = 0.00f * pixM2;
        float wa = 0.025f * pixM2;
        float fa = 0.075f * pixM2;
        float ft = -0.10f * pixM2;
        int wid = 12;
        mLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);

        // Use a loop to draw a "halo" around the plane as well
        for (int i = 0; i < 2; i++) {
            // Wings
            mLine.SetWidth(wid);

            mLine.SetVerts(
                    -wx, wy, z,
                    wx, wy, z
            );
            mLine.draw(matrix);
            // Left
            mLine.SetVerts(
                    -wx, 0, z,
                    0, wa, z
            );
            mLine.draw(matrix);
            // Right
            mLine.SetVerts(
                    wx, 0, z,
                    0, wa, z
            );
            mLine.draw(matrix);

            // Fuselage
            mLine.SetVerts(
                    0, fa, z,
                    0, ft, z
            );
            mLine.draw(matrix);

            // Tail
            mLine.SetVerts(
                    -wx / 2, ft, z,
                    wx / 2, ft, z
            );
            mLine.draw(matrix);

            wx = 0.09f * pixM2;
            wy = 0.00f * pixM2;
            wa = 0.020f * pixM2;
            fa = 0.065f * pixM2;
            ft = -0.10f * pixM2;
            wid = 6;
            mLine.SetColor(backShadeR, backShadeG, backShadeB, 1);
        }

        //
        // Own Ship track/speed line  - same as target leaders (1 and 2 minute markers)
        //
        if (bOwnShip) {
            float x2 = mMapZoom * (IASValue * LEADER_TIME);
            float radius = pixM / 60f;

            mLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);
            mLine.SetWidth(radius / 2);
            mLine.SetVerts(
                    0, 0, z,
                    0, x2, z
            );
            mLine.draw(matrix);
            mLine.SetVerts(
                    -0.025f * pixM2, x2, z,
                    +0.025f * pixM2, x2, z
            );
            mLine.draw(matrix);
            /*
            halfway  minute marker - Leave out for now
            mLine.SetVerts(
                    -0.025f * pixM2, x2/2, z,
                    +0.025f * pixM2, x2/2, z
            );
            mLine.draw(matrix);*/
        }
    }

    //-------------------------------------------------------------------------
    // Render map scale ruler
    //
    protected void renderMapScale(float[] matrix)
    {
        float z;
        z = zfloat;
        float ypos = -0.97f;
        float ytip = ypos + 0.03f;

        float distance = 20;
        float x1 = mMapZoom * distance;

        while (x1 > pixM / 3) {
            if (distance > 4) {
                distance = distance - 4;
                x1 = mMapZoom * distance;
            }
            else {
                // very close - and also catch the potential
                // endless loop
                distance = 1;
                x1 = mMapZoom;
                break;
            }
        }

        // Scale line
        mLine.SetWidth(1);
        mLine.SetColor(foreShadeR, foreShadeG, foreShadeB, 1);
        mLine.SetVerts(
                ypos * pixW2 + 0, ypos * pixH2, z,
                ypos * pixW2 + x1, ypos * pixH2, z
        );
        mLine.draw(matrix);
        mLine.SetVerts(
                ypos * pixW2 + 0, ypos * pixH2, z,
                ypos * pixW2 + 0, ytip * pixH2, z
        );
        mLine.draw(matrix);
        mLine.SetVerts(
                ypos * pixW2 + x1, ypos * pixH2, z,
                ypos * pixW2 + x1, ytip * pixH2, z
        );
        mLine.draw(matrix);

        String t = String.format("%3.0f nm", distance);
        glText3.begin(foreShadeR, foreShadeG, foreShadeB, 1, matrix); // White
        //glText.setScale(1.5f);
        glText3.draw(t, ytip * pixW2, ypos * pixH2);            // Draw  String
        glText3.end();

        // leader line  - same as scale line
        /*
        mLine.SetVerts(
                0, 0, z,
                0, x1, z
        );
        mLine.draw(matrix);
        mLine.SetVerts(
                -0.025f * pixM2, x1, z,
                +0.025f * pixM2, x1, z
        );
        mLine.draw(matrix);
        */
    }

    protected void dimMapScale(float[] matrix, float alpha)
    {

    }


    //-------------------------------------------------------------------------
    // Map Zooming
    //
    public void setMapZoom(float zoom)
    {
        mMapZoom = zoom;
    }

    public void zoomIn()
    {
        mMapZoom *= 1.5;
        if (mMapZoom > MAX_ZOOM) mMapZoom = MAX_ZOOM;
    }

    public void zoomOut()
    {
        mMapZoom /= 1.5;
        if (mMapZoom < MIN_ZOOM) mMapZoom = MIN_ZOOM;
    }

    public void setAutoZoomActive(boolean active)
    {
        autoZoomActive = active;
    }

    public boolean isAutoZoomActive()
    {
        return autoZoomActive;
    }

    //-------------------------------------------------------------------------
    //
    //
    protected void setAutoZoom()
    {
        float a = mSelWptDme * mMapZoom;
        while ((a > pixM2) && (mMapZoom > MIN_ZOOM)) {
            zoomOut();
            a = mSelWptDme * mMapZoom;
        }
        while ((a < pixM2) && (mMapZoom < MAX_ZOOM)) {
            zoomIn();
            a = mSelWptDme * mMapZoom;
        }
    }


    //-------------------------------------------------------------------------
    // North Que
    //
    protected void renderNorthQue(float[] matrix)
    {
        float  z = zfloat;

        mTriangle.SetWidth(1);
        // Right triangle
        mTriangle.SetColor(foreShadeR, foreShadeG, foreShadeB, 1); // lightest gray (0.7)
        mTriangle.SetVerts(0, -0.08f*pixM2, z,
                0,            +0.08f*pixM2, z,
                0.03f*pixM2,  -0.12f*pixM2,z);
        mTriangle.draw(matrix);

        // left triangle
        mTriangle.SetColor(tapeShadeR, tapeShadeG, tapeShadeB, 1); // darker gray (0.5)
        mTriangle.SetVerts(0, -0.08f*pixM2, z,
                +0,           +0.08f*pixM2, z,
                -0.03f*pixM2, -0.12f*pixM2,z);
        mTriangle.draw(matrix);

        glText3.begin(tapeShadeR, tapeShadeG, tapeShadeB, 1, matrix); // lighter gray (0.6)
        //glText.setScale(1.5f); // 2 seems full size
        glText3.drawCX("N", 0, 0.09f*pixM2);
        glText3.end();
    }

    //-------------------------------------------------------------------------
    // Vector Text
    //
    protected void renderVChar(float[] matrix, char c)
    {
        float  z = zfloat;
        float m = pixM2;


        mPolyLine.SetColor(1, 1, 0, 1);
        mPolyLine.SetWidth(1);

        switch (c) {
            case 'A': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.3f * m, z,
                        0.3f * m, 0.9f * m, z,
                        0.6f * m, 0.3f * m, z,
                        0.6f * m, 0.0f * m, z,
                        0.6f * m, 0.3f * m, z,
                        0.0f * m, 0.3f * m, z
                };
                mPolyLine.VertexCount = 7;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'B': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.6f * m, 0.8f * m, z,
                        0.6f * m, 0.6f * m, z,
                        0.5f * m, 0.5f * m, z,
                        0.1f * m, 0.5f * m, z,
                        0.5f * m, 0.5f * m, z,
                        0.6f * m, 0.4f * m, z,
                        0.6f * m, 0.1f * m, z,
                        0.5f * m, 0.0f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.1f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 13;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'C': {
                float[] vertPoly = {
                        // 6 x 8
                        0.6f * m, 0.8f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.0f * m, 0.8f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.5f * m, 0.0f * m, z,
                        0.6f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 7;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'D': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.6f * m, 0.8f * m, z,
                        0.6f * m, 0.1f * m, z,
                        0.5f * m, 0.0f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.1f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 8;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'E': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.5f * m, 0.8f * m, z,
                        0.5f * m, 0.6f * m, z,
                        0.4f * m, 0.5f * m, z,
                        0.1f * m, 0.5f * m, z,
                        0.4f * m, 0.5f * m, z,
                        0.5f * m, 0.4f * m, z,
                        0.5f * m, 0.1f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.1f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 13;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }


            default:
                break;
        }

    }

    protected void renderVText(float[] matrix, String s)
    {
        //renderVChar(matrix, 'A');
        //renderVChar(matrix, 'B');
        //renderVChar(matrix, 'C');
        //renderVChar(matrix, 'D');
    }



}


//-----------------------------------------------------------------------------
/*
Some leftover code fragments from the original c code.
This may still be useful one day

void GLPFD::renderDIMarkers()
{
	GLint i, j;
	GLfloat innerTic, midTic, outerTic, z, pixPerUnit, iPix;

	glLineWidth( 2 );
  pixPerUnit = pixH2/DIInView;
  z = zfloat;

  font = QFont("Fixed", 10, QFont::Normal);
	QFontMetrics fm = fontMetrics();

  innerTic = 0.80 * pixH2;	// inner & outer are relative to the vertical scale line
  outerTic = 0.90 * pixH2;
  midTic = 0.87 * pixH2;

	// The numbers & tics for the tape
	qglColor( QColor( "white" ) );

  for (i = 0; i < 360; i=i+30) {
    iPix = (float) i * pixPerUnit;
    t = QString( "%1" ).arg( i );

    glBegin(GL_LINE_STRIP);
      glVertex3f( iPix, innerTic, z);
      glVertex3f( iPix, outerTic, z);
    glEnd();
    QGLWidget::renderText (iPix - fm.width(t)/2 , outerTic  + fm.ascent() / 2 , z, t, font, 2000 );

    //for (j = i + 20; j < i+90; j=j+20) {
    for (j = i + 10; j < i+90; j=j+10) {
      iPix = (float) j * pixPerUnit;
			glBegin(GL_LINE_STRIP);
        glVertex3f( iPix, innerTic,z);
        glVertex3f( iPix, midTic,z);
			glEnd();
    }


    iPix = (float) (360-i) * pixPerUnit;
    t = QString( "%1" ).arg( i );
    glBegin(GL_LINE_STRIP);
      glVertex3f( -iPix, innerTic, z);
      glVertex3f( -iPix, outerTic, z);
    glEnd();
    QGLWidget::renderText (-iPix - fm.width(t)/2 , outerTic  + fm.ascent() / 2 , z, t, font, 2000 );

    //for (j = i + 20; j < i+90; j=j+20) {
    for (j = i + 10; j < i+90; j=j+10) {
      iPix = (float) j * pixPerUnit;
      glBegin(GL_LINE_STRIP);
        glVertex3f( -iPix, innerTic,z);
        glVertex3f( -iPix, midTic,z);
      glEnd();
    }
	}

  // The horizontal scale bar
	glBegin(GL_LINE_STRIP);
    glVertex3f(-180 * pixPerUnit , innerTic, z);
    glVertex3f(180 * pixPerUnit, innerTic, z);
  glEnd();
}


//
//	Set the indicator
//

void GLPFD::setHeading(int degrees)
{
  DIValue = degrees;
  while (DIValue < 0) DIValue += 360;
  DITranslation = DIValue / DIInView  * pixH2;
	updateGL();
}

void GLPFD::setSlip(int slip)
{
  SlipValue = slip;
  updateGL();
}


void GLPFD::setGForce(float gforce)
{
  GForceValue = gforce;
  updateGL();
}

void GLPFD::setGS(float gs)
{
  GSValue = gs;
  updateGL();
}

void GLPFD::setROT(float rot)
{
  ROTValue = rot;
  updateGL();
}

//
//	Set the bearing (to steer)
//

void GLPFD::setBearing(int degrees)
{
  baroPressure = degrees;
	updateGL();
}

*/



