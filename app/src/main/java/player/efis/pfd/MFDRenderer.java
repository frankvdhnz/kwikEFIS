/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.pfd;

import android.content.Context;
import android.graphics.Color;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import java.util.Iterator;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import player.efis.common.AircraftData;
import player.efis.common.AirspaceClass;
import player.efis.common.DemColor;
import player.efis.common.DemGTOPO30;
import player.efis.common.Gpx;
import player.efis.common.OpenAirspace;
import player.efis.common.OpenAirspacePoint;
import player.efis.common.OpenAirspaceRec;
import player.efis.common.Point;
import player.efis.common.WxRadarMap;
import player.gles20.GLText;
import player.gles20.Line;
import player.gles20.PolyLine;
import player.gles20.Polygon;
import player.gles20.Square;
import player.gles20.Triangle;
import player.ulib.UMath;
import player.ulib.UNavigation;
import player.ulib.UTrig;

public class MFDRenderer extends EFISRenderer implements GLSurfaceView.Renderer
{
    private static final String TAG = "MFDRenderer";
    protected boolean ServiceableMap;      // Flag to indicate Map failure

    public MFDRenderer(Context context)
    {
        super(context);
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config)
    {
        // Set the background frame color
        GLES20.glClearColor(backShadeR, backShadeG, backShadeB, 1.0f);

        mTriangle = new Triangle();
        mSquare = new Square();
        mLine = new Line();
        mPolyLine = new PolyLine();
        mPolygon = new Polygon();

        // Create the GLText
        glText3 = new GLText(context.getAssets());
        glText4 = new GLText(context.getAssets());
        glText5 = new GLText(context.getAssets());
        glText6 = new GLText(context.getAssets());
        glText7 = new GLText(context.getAssets());

        glText8 = new GLText(context.getAssets());
        glText9 = new GLText(context.getAssets());
        //roseTextScale = 1f;
    }

    @Override
    public void onDrawFrame(GL10 gl)
    {
        // Draw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        DemIncAngle = 1 + (int) Math.min(5,  Math.abs(ROTValue*1.0));
        //Log.d("kwik", " DemIncAngle:" + DemIncAngle + " ROTValue:"+ ROTValue + "  VSIValue:"  + VSIValue);

        DrawFrameMfd(gl);
    }


/*
ninelima@yahoo.com

-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1.4.7-Mobility Email: (MingW32)

mQENBE0lygcBCADOwYhbGnXlfjtGeWdxRu6yNVSek6H5ZcgsAxD1C9RbVZ+iEDhD
/+u2KmaTWYmUzKP4UqrzNHa3OBZ34+Y+hfzxybCGAb2KxFf89S019jlXpJ/t1xTx
/cFx3U08a0jvl4Jcg+mXRMjAnYJuN8BAnubKOwZymAx0D2EwpHh9tPCSINMd2rAM
Ac9HUuneeuZFt2ZrIlDzyztxkfw+Vv8k6MPPZGDB9CiZAcRNneggDlA/u+THMRBy
MkoLtEulbjanAYmRQGY7V4aN7yOkCOakF1BDPCvyluYxlZaBc1Dvx2XdnGRODgrS
Ejn1YJddoSf2RsHjl5R6NV6VDTla5P6slX47ABEBAAG0JE5pbmUgTGltYSAoMTIz
KSA8bmluZWxpbWFAeWFob28uY29tPokBOAQTAQIAIgUCTSXKBwIbAwYLCQgHAwIG
FQgCCQoLBBYCAwECHgECF4AACgkQOcgn69ZgK8cSOggAyH7LIzHBTb56OTTpkA6g
FffnroLd/BQcZV87y1oAxIjJ4CCujb1r56CCpNdujQVB1twrzxc3LS6sM4vaXy6e
AbxvbSKjA19XZzvMpel8TbER2A1/daArby+Orkk/LOAK3W+ZHdXY9BYMBZ0puB28
HpS0Eg9XDgVQAgYbVwBSSKx2swfb3Ri2hDMjp4M4ZGOjSXK8VJKaecDgxdcJLhrb
WU7fdMDsUJIkjzvYRypiEPO1oRwbfR3Di7CSln4arwbztdgznvhIah8gqIGH8XVZ
s9mXaGFFNQUOiGYqk9sfmKNx7FLic4kGzpmnMkvLsTiHPRaTeC5eeBtMul1UrtaI
37kBDQRNJcoHAQgAtAXUyq2+mXKoSIjdTgcjhSX2rbW6LSfX11xCnj4qFc+ya7/+
wB1McFqAFp2zIsjYS1F+ny0H2ov/OPFsy4QeVIfZObSxXcXwDujSTDKIubdpMMTs
/4uJxI26A95LDxBsY887UgJllyTh6e8Ps51dPH/9Wh7kPopk2wHGmV6uoQoNnquw
LUekXSSXM0ePd0dnX0RVy63MUojOcNOGs9+cNeShlg0tOgbweWKLIG2+VcFKneuN
J1NUjHhF252l7U3552clKIE3TCHV1XWDKtmG6zqtFWg3JIOHnrw46P1rq5xc85vX
b+oAP/dw/Yw7jWtOw7QIevBpaHzS1lfrwCxzkQARAQABiQEfBBgBAgAJBQJNJcoH
AhsMAAoJEDnIJ+vWYCvHuk4IAMkYUuGRIHm6zyUYpmQbM3Se7C34QYb710PRXCyY
gNq33NfCn/dn5iNjnauXmFcv5ZyWUGiqoGInzKkzQMrCvnFURLvQ3s4mx7i5GTMN
1yCqTtTOCxJmF72r53IGISXXjxledfb7O5Pwh/EG7k6F/tzPxWRZ4n5pDQspoI+C
jehkRzzPhrOFQzdoruAv/sJWrhSZhRBt25w3VOfd640toePH5URmh1Zn76AAxat1
H06iCvbYnpywTDH8Z8Y66fyCkL5AjYy/H28FEkDw0iQDfVbeICEnZzMpxEV5Fb8w
ay8jq8SJcuQ2mpCJ+l8VP4WYOz1YFhNGXgZpl1sEvVjc2jE=
=5RxY
-----END PGP PUBLIC KEY BLOCK-----
*/


    //---------------------------------------------------------------
    // Multi-Function-Display Drawing (DMAP)
    //
    private void DrawFrameMfd(GL10 gl)
    {
        // Set the camera position (View matrix)
        if (displayMirror)
            Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);  // Mirrored View
        else
            Matrix.setLookAtM(mViewMatrix, 0, 0, 0, +3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);  // Normal View

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        zfloat = 0;

        float xlx;
        float xly;

        // fatFingerActive just for performance
        if (!fatFingerActive) {
            if (displayDEM) renderDEMTerrainMfd(mMVPMatrix);
            if (displayWX) renderWeatherMapMfd(mMVPMatrix);
		}

        if (displayAirspace) renderAirspaceMfd(mMVPMatrix);
        if (displayAirport) renderAPT(mMVPMatrix);  // must be on the same matrix as the Pitch
        if (true) renderTargets(mMVPMatrix);        // TODO: 2018-08-31 Add control of targets
        if (displayCCIP) renderCCIP(mMVPMatrix);           // TODO: 2018-08-31 Add control of CCIP

        // Remote Magnetic Inidicator - RMI
        if (displayRMI) {
            // Add switch for orientation
            if (Layout == layout_t.PORTRAIT) {
                //Portrait
                xlx = 0;
                xly = -0.20f * pixH2;
                roseScale = 1.9f;
            }
            else {
                // Landscape
                xlx = 0;
                xly = -1.80f * pixH2;
                roseScale = 1.9f;
                GLES20.glViewport(0, pixH2, pixW, pixH);
            }

            Matrix.translateM(mMVPMatrix, 0, xlx, xly, 0);
            // Create a rotation for the RMI
            Matrix.setRotateM(mRmiRotationMatrix, 0, DIValue, 0, 0, 1);  // compass rose rotation
            Matrix.multiplyMM(rmiMatrix, 0, mMVPMatrix, 0, mRmiRotationMatrix, 0);
            renderFixedCompassMarkers(mMVPMatrix);
            Matrix.translateM(mMVPMatrix, 0, -xlx, -xly, 0);
            renderCompassRose(rmiMatrix);
            GLES20.glViewport(0, 0, pixW, pixH);  // fullscreen
        }

        //-----------------------------
        if (displayFlightDirector) {
            if (autoZoomActive) setAutoZoom();
            renderDctTrack(mMVPMatrix);
        }

        if (displayTape) {
            //if (displayTape == true) renderFixedVSIMarkers(mMVPMatrix); // todo: maybe later

            xlx = 0.99f * pixW2;
            xly = -0.6f * pixM2;

            Matrix.translateM(mMVPMatrix, 0, xlx, 0, 0);
            renderFixedAltMarkers(mMVPMatrix);
            Matrix.translateM(mMVPMatrix, 0, 0, xly, 0);
            renderFixedRadAltMarkers(mMVPMatrix); // AGL
            Matrix.translateM(mMVPMatrix, 0, -xlx, -xly, 0);

            xlx = -0.99f * pixW2;
            Matrix.translateM(mMVPMatrix, 0, xlx, 0, 0);
            renderFixedASIMarkers(mMVPMatrix);
            Matrix.translateM(mMVPMatrix, 0, -xlx, -0, 0);

            xlx = 0;
            xly = +0.90f * pixH2;
            Matrix.translateM(mMVPMatrix, 0, xlx, xly, 0);
            renderFixedDIMarkers(mMVPMatrix);
            //renderHDGValue(mMVPMatrix);
            Matrix.translateM(mMVPMatrix, 0, -xlx, -xly, 0);
        }

        //-----------------------------
        if (displayInfoPage) {
            dimAcillaryDetails(mMVPMatrix, wxDim);
            renderAncillaryDetails(mMVPMatrix);
            renderBatteryPct(mMVPMatrix);

            // North Que
            xlx = -0.84f * pixW2;
            xly = +0.88f * pixH2;

            Matrix.translateM(mMVPMatrix, 0, xlx, xly, 0);
            Matrix.setRotateM(mRmiRotationMatrix, 0, DIValue, 0, 0, 1);  // compass rose rotation
            Matrix.multiplyMM(rmiMatrix, 0, mMVPMatrix, 0, mRmiRotationMatrix, 0);
            renderNorthQue(rmiMatrix);
            Matrix.translateM(mMVPMatrix, 0, -xlx, -xly, 0);
        }

        if (displayFlightDirector) {
            dimAutoWptDetails(mMVPMatrix, wxDim);
            renderAutoWptDetails(mMVPMatrix);
        }
        renderMapScale(mMVPMatrix);  // do before the DI
        renderACSymbol(mMVPMatrix, true); // unlike COMP rose is not centered

        if (bBannerActive) renderBannerMsg(mMVPMatrix);
        if (bSimulatorActive) renderSimulatorActive(mMVPMatrix);

        // Do this last so that everything else wil be dimmed for fatfinger entry
        if (displayFlightDirector) {
            dimSelWptDetails(mMVPMatrix, wxDim);
            renderSelWptDetails(mMVPMatrix);
            renderSelWptValue(mMVPMatrix);
        }

        // Do this after any dimming
        if (!ServiceableDevice) renderUnserviceableDevice(mMVPMatrix);
        if (!ServiceableMap) renderUnserviceablePage(mMVPMatrix);
        if (!ServiceableAlt) renderUnserviceableAlt(mMVPMatrix);
        if (!ServiceableAsi) renderUnserviceableAsi(mMVPMatrix);
        if (!ServiceableDi) renderUnserviceableDi(mMVPMatrix);
        if (!Gpx.bReady) renderUnserviceableSelWpt(mMVPMatrix);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        // Adjust the viewport based on geometry changes, such as screen rotation
        GLES20.glViewport(0, 0, width, height);

        // this projection matrix is applied to  object coordinates in the onDrawFrame() method
        //b2 Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
        //Matrix.frustumM(mProjMatrix, 0, -ratio, ratio, -1, 1, 2, 7); // - this apparently fixed for the Samsung S2?

        //b2 start
        // Capture the window scaling for use by the rendering functions
        pixW = width;
        pixH = height;
        pixW2 = pixW / 2;
        pixH2 = pixH / 2;

        pixM = Math.min(pixW, pixH);

        // because the ascpect ratio is different in landscape and portrait (due to menu bar)
        // we just fudge it as 88% throughout,  looks OK in landscape as well
        pixM = pixM * 88 / 100;
        pixM2 = pixM / 2;

        setSpinnerParams(); // Set up the spinner locations and SelWpt display

        // Set the window size specific scales, positions and sizes (nothing dynamic yet...)
        pitchInView = 25.0f;     // degrees to display from horizon to top of viewport
        IASInView = 40.0f;       // IAS units to display from center to top of viewport
        MSLInView = 300.0f;      // IAS units to display from center to top of viewport

        // this projection matrix is applied to  object coordinates in the onDrawFrame() method
        float ratio = (float) width / height;
        //Matrix.frustumM(mProjectionMatrix, 0, -ratio*pixH2, ratio*pixH2, -pixH2, pixH2, 3f, 7f); // all the rest
        Matrix.frustumM(mProjectionMatrix, 0, -ratio * pixH2, ratio * pixH2, -pixH2, pixH2, 2.99f, 75f); //hack for Samsung G2

        // Load the font from file (set size + padding), creates the texture
        // NOTE: after a successful call to this the font is ready for rendering!
        // glText.load( "Roboto-Regular.ttf", 14, 2, 2 );  // Create Font (Height: 14 Pixels / X+Y Padding 2 Pixels)
        glText3.load("square721_cn_bt_roman.ttf", pixM * 3 / 100, 2, 2);  // Create Font (Height: ~ 3%  / X+Y Padding 2 Pixels)
        glText4.load("square721_cn_bt_roman.ttf", pixM * 4 / 100, 2, 2);  // Create Font (Height: ~ 4%  / X+Y Padding 2 Pixels)
        glText5.load("square721_cn_bt_roman.ttf", pixM * 5 / 100, 2, 2);  // Create Font (Height: ~ 5%  / X+Y Padding 2 Pixels)
        glText6.load("square721_cn_bt_roman.ttf", pixM * 6 / 100, 2, 2);  // Create Font (Height: ~ 6%  / X+Y Padding 2 Pixels)
        glText7.load("square721_cn_bt_roman.ttf", pixM * 7 / 100, 2, 2);  // Create Font (Height: ~ 7%  / X+Y Padding 2 Pixels)

        glText8.load("square721_cn_bt_roman.ttf", pixM * 8 / 100, 2, 2);  // Create Font (Height: ~ 8%  / X+Y Padding 2 Pixels)
        glText9.load("square721_cn_bt_roman.ttf", pixM * 9 / 100, 2, 2);  // Create Font (Height: ~ 9%  / X+Y Padding 2 Pixels)

        // enable texture + alpha blending
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);
    }

    @Override
    protected void renderUnserviceableDevice(float[] matrix)
    {
        renderUnserviceablePage(matrix);
        renderUnserviceableDi(matrix);
        renderUnserviceableAlt(matrix);
        renderUnserviceableAsi(matrix);
    }

    //
    // project
    //
    // relbrg in degrees
    // dme in nm
    // elev in m
    @Override
    protected Point project(float relbrg, float dme)
    {
        return new Point(
                mMapZoom * dme * UTrig.icos(90-(int)relbrg),
                mMapZoom * dme * UTrig.isin(90-(int)relbrg)
        );
    }

    @Override
    protected Point project(float relbrg, float dme, float elev)
    {
        return new Point(
                mMapZoom * dme * UTrig.icos(90-(int)relbrg),
                mMapZoom * dme * UTrig.isin(90-(int)relbrg)
        );
    } // end of project


    //-------------------------------------------------------------------------
    // Set the spinner control parameters
    //
    public void setSpinnerParams()
    {
        // This code determines where the spinner control
        // elements are displayed. Used by WPT and ALT
        if (Layout == layout_t.LANDSCAPE) {
            // Landscape --------------
            lineAutoWptDetails =   +0.60f;
            lineAncillaryDetails = -0.30f;

            if (fatFingerActive) {
                selWptDec = 0.75f * pixH2;
                selWptInc = 0.45f * pixH2;
                selAltDec = -0.45f * pixH2;
                selAltInc = -0.75f * pixH2;

                lineC = 0.2f;
                leftC = -0.55f;
                spinnerStep = 0.25f;
                spinnerTextScale = 2;
            }
            else {
                // Top
                selWptDec = 0.90f * pixH2;
                selWptInc = 0.74f * pixH2;
                selAltDec = -0.74f * pixH2;
                selAltInc = -0.90f * pixH2;

                lineC = 0.50f;
                leftC = 0.6f;
                spinnerStep = 0.1f;
                spinnerTextScale = 1;
            }
        }
        else {
            // Portrait ---------------
            lineAutoWptDetails = -0.60f;
            lineAncillaryDetails = -0.85f;

            if (fatFingerActive) {
                selWptDec = 0.7f * pixH2;
                selWptInc = 0.4f * pixH2;
                selAltDec = -0.4f * pixH2;
                selAltInc = -0.7f * pixH2;

                lineC = 0.15f;
                leftC = -0.75f;
                spinnerStep = 0.5f;
                spinnerTextScale = 2;
            }
            else {
                selWptDec = -0.60f * pixH2;
                selWptInc = -0.71f * pixH2;
                selAltDec = -0.80f * pixH2;
                selAltInc = -0.91f * pixH2;

                lineC = -0.82f;
                leftC = 0.6f;
                spinnerStep = 0.1f;
                spinnerTextScale = 1;
            }
        }
    }

    //-------------------------------------------------------------------------
    // Render the Digital Elevation Model (DEM) - DMAP.
    //
    // This is the meat and potatoes of the synthetic vision implementation
    // The loops are very performance intensive, therefore all the hardcoded
    // magic numbers
    //
    protected void renderDEMTerrainMfd(float[] matrix)
    {
        float z, x1, y1, z1;
        float lat, lon;
        z = zfloat;

        float dme;             //in nm
        float step = 0.50f;    //in nm, normally this should be = gridy
                               // 0.5 nm is appox 1km which is the size of the DEM tiles.
        float agl_ft;          // in Feet
        float demRelBrg;       // = DIValue + Math.toDegrees(Math.atan2(deltaLon, deltaLat));
        float caution;
        final float cautionMin = 0.2f;
        final float IASValueThreshold = AircraftData.Vx; //1.5f * Vs0;
        float range = 1.1f * pixM / mMapZoom;  // range is more than CFD ~ 2X

        //if (mMapZoom < 16) step *= 2;
        //if (mMapZoom < 8) step *= 2;

        float hgt = mMapZoom * step/2; // 50% overlap, optional * 0.7071f;  // 1/sqrt(2)
        float wid; // = hgt;

        for (dme = range; dme >= 0; dme = dme - step) { // DEM_HORIZON=20, was 30
            float _x1=0, _y1=0;
            float circ = (float)UTrig.M_2PI * dme; // circumference of dme
            hgt = Math.max(hgt, mMapZoom * circ * DemIncAngle / 720f);  // / 360f / 2);
            wid = hgt;

            for (demRelBrg = -180; demRelBrg <= 180; demRelBrg = demRelBrg + DemIncAngle) { //1
                lat = LatValue + dme / 60 * UTrig.icos((int) (DIValue + demRelBrg));
                lon = LonValue + dme / 60 * UTrig.isin((int) (DIValue + demRelBrg));
                z1 = DemGTOPO30.getElev(lat, lon);

                x1 = mMapZoom * (dme * UTrig.icos(90-(int)demRelBrg));
                y1 = mMapZoom * (dme * UTrig.isin(90-(int)demRelBrg));

                if ((_x1 != 0) || (_y1 != 0)) {

                    DemColor color = DemGTOPO30.getColor((short) z1);
                    // Handle Monochrome
                    if (colorTheme == 2) {
                        color.red = 0;
                        color.blue = 0;
                    }
                    caution = cautionMin + (color.red + color.green + color.blue);
                    agl_ft = MSLValue - z1 * 3.28084f;  // in ft

                    if (agl_ft > 1000) mSquare.SetColor(color.red, color.green, color.blue, 1);                     // Enroute
                    else if (IASValue < IASValueThreshold) mSquare.SetColor(color.red, color.green, color.blue, 1); // Taxi or  approach
                    else if (agl_ft > 200) mSquare.SetColor(caution, caution, 0, 1f);  // Proximity notification
                    else mSquare.SetColor(caution, 0, 0, 1f);                          // Proximity warning
                    float[] squarePoly = {
                            x1-wid, y1-hgt, z,
                            x1-wid, y1+hgt, z,
                            x1+wid, y1+hgt, z,
                            x1+wid, y1-hgt, z
                    };
                    mSquare.SetVerts(squarePoly);
                    mSquare.draw(matrix);
                }
                _x1 = x1;
                _y1 = y1;
            }
        }
    }

    //-------------------------------------------------------------------------
    // Render the Weather map.
    //
    // This is the meat and potatoes of the synthetic vision implementation
    // The loops are very performance intensive, therefore all the hardcoded
    // magic numbers
    //
    protected void renderWeatherMapMfd(float[] matrix)
    {
        //float min = 1, max = 0;
        float z, x1, y1, z1;
        float lat, lon;
        z = zfloat;

        float dme;             //in nm
        float step = 0.50f;    //in nm, normally this should be = gridy
                               // 0.5 nm is appox 1km which is the size of the DEM tiles (30 arc sec).
        step = 60 * WxRadarMap.SPANLAT / WxRadarMap.BUFY; //step = 256f / 45f;  // in nm

        float demRelBrg;       // = DIValue + Math.toDegrees(Math.atan2(deltaLon, deltaLat));
        float range = 1.1f * pixM / mMapZoom;

        float hgt = mMapZoom * step/2; // optional  * 0.7071f;  // 1/sqrt(2)
        float wid;

        for (dme = range; dme >= 0; dme = dme - step) { // DEM_HORIZON=20, was 30
            float _x1=0, _y1=0;
            float circ = (float)UTrig.M_2PI * dme; // circumference of dme

            hgt =  Math.max(hgt, mMapZoom * circ * DemIncAngle / 720f);  // / 360f / 2);
            wid = hgt;

            for (demRelBrg = -180; demRelBrg <= 180; demRelBrg = demRelBrg + DemIncAngle) { //1
                lat = LatValue + dme / 60 * UTrig.icos((int) (DIValue + demRelBrg));
                lon = LonValue + dme / 60 * UTrig.isin((int) (DIValue + demRelBrg));
                int zz = WxRadarMap.getDbz(lat, lon);

                if (zz == 0)
                    continue;

                x1 = mMapZoom * (dme * UTrig.icos(90-(int)demRelBrg));
                y1 = mMapZoom * (dme * UTrig.isin(90-(int)demRelBrg));

                if ((_x1 != 0) || (_y1 != 0)) {
                    // Handle Monochrome
                    if (colorTheme == 2) {
                        zz = zz & 0x0000ff00;
                    }

                    mSquare.SetColor(zz, 128);
                    float[] squarePoly = {
                            x1-wid, y1-hgt, z,
                            x1-wid, y1+hgt, z,
                            x1+wid, y1+hgt, z,
                            x1+wid, y1-hgt, z
                    };
                    mSquare.SetVerts(squarePoly);
                    mSquare.draw(matrix);
                }
                _x1 = x1;
                _y1 = y1;
            }
        }
    }

    protected void renderWeatherMapMfd__old(float[] matrix)
    {
        //float min = 1, max = 0;
        float z, x1, y1, z1;
        float lat, lon;
        z = zfloat;

        float dme;             //in nm
        float step = 0.50f;    //in nm, normally this should be = gridy
                               // 0.5 nm is appox 1km which is the size of the DEM tiles (30 arc sec).
        step = 15f * WxRadarMap.SPANLAT / WxRadarMap.BUFY; //step = 256f / 45f;  // in nm
        //step = 2f; // 0.5f;// todo: tbf 0.05 also works slow

        float demRelBrg;       // = DIValue + Math.toDegrees(Math.atan2(deltaLon, deltaLat));
        float range = 1.1f * pixM / mMapZoom;

        if (mMapZoom < 16) step *= 2;
        if (mMapZoom < 8) step *= 2;

        float hgt = mMapZoom * step; // optional  * 0.7071f;  // 1/sqrt(2)
        float wid = hgt;

        for (dme = range; dme >= 0; dme = dme - step) { // DEM_HORIZON=20, was 30
            float _x1=0, _y1=0;
            float circ = (float)UTrig.M_2PI * dme; // circumference of dme

            hgt =  Math.max(hgt, mMapZoom * circ * DemIncAngle / 720f);  // / 360f / 2);
            wid = hgt;//*1.5f;// = 2;*/

            for (demRelBrg = -180; demRelBrg <= 180; demRelBrg = demRelBrg + DemIncAngle) { //1
                lat = LatValue + dme / 60 * UTrig.icos((int) (DIValue + demRelBrg));
                lon = LonValue + dme / 60 * UTrig.isin((int) (DIValue + demRelBrg));
                z1 = WxRadarMap.getDbz(lat, lon);

                if (z1 == 0)
                    continue;

                x1 = mMapZoom * (dme * UTrig.icos(90-(int)demRelBrg));
                y1 = mMapZoom * (dme * UTrig.isin(90-(int)demRelBrg));

                if ((_x1 != 0) || (_y1 != 0)) {

                    int zz = UMath.trunc(z1) & 0x00ffffff;

                    // On HSV model, H (hue) define the base color, S (saturation) control the amount of gray
                    // and V controls the brightness. So, if you enhance V and decrease S at same time, you gets
                    // more luminance
                    //
                    // 0 = Red
                    // 60 = Yellow
                    // 120 = Green
                    // 180 = Cyan
                    // 240 = Blue
                    // 300 = Purple

                    // White = 0mm Black = 200mm

                    float[] hsv = {0, 0, 0, 0};
                    Color.colorToHSV(zz, hsv);
                    // If we want to tweak anything
                    //hsv[0] = hsv[0];  // hue 0..360
                    //hsv[1] = hsv[1];  // sat 0..1
                    //hsv[2] = hsv[2];  // val 0..1
                    //hsv[3] = hsv[3];  // alpha 0..1
                    //Log.v("kwik", "h= " + hsv[0] + " s= " + hsv[1] + " v= " + hsv[2]);

                    // Excludes
                    if (hsv[1] < 0.475) continue;  // Cloud threshold. 0.500=almost all cloud, 0.450=show significant amount
                    if (hsv[1] < 0.4) continue;    // Ignore the white edge
                    if (hsv[2] < 0.1) continue;    // Ignore black edge

                    // 0.5 = Good overall (pops but no bleaching), 1 = dull color for Wx on all , 0 = Only good for full color theme
                    if ((hsv[0] > 210) && (hsv[0] < 270)) {
                        // Clouds
                        hsv[0] -= 120;  // blue to green
                        hsv[2] /= 2;
                    }
                    else {
                        // Rain

                        //v range ~ 0.3 - 0.5
                        if (hsv[2] > 0.4) continue; //hsv[0] = 300;
                        if (hsv[2] < 0.35) hsv[0] -= 60; //hsv[0] = 000;
                        if (hsv[2] < 0.30) hsv[0] -= 60; //= 300;

                        hsv[2] *= 2;
                    }
                    hsv[3] = 0.5f;  // alpha 50% transparent

                    // Handle Monochrome
                    if (colorTheme == 2) {
                        hsv[0] = 120;// green
                    }

                    mSquare.SetColor(hsv);
                    float[] squarePoly = {
                            x1-wid, y1-hgt, z,
                            x1-wid, y1+hgt, z,
                            x1+wid, y1+hgt, z,
                            x1+wid, y1-hgt, z
                    };
                    mSquare.SetVerts(squarePoly);
                    mSquare.draw(matrix);
                }
                _x1 = x1;
                _y1 = y1;
            }
        }
    }


    //-------------------------------------------------------------------------
    // Airspace
    //
    protected void renderAirspaceMfd(float[] matrix)
    {
        float z;
        float x1, y1;
        float _x1, _y1;

        z = zfloat;

        // 0.16667 deg lat  = 10 nm
        // 0.1 approx 5nm
        float dme = 0;           // =  60 * 6080 * Math.hypot(deltaLon, deltaLat);  // ft
        //float _dme = 6080000;    // 1,000 nm in ft
        float airspacepntRelBrg; // = DIValue + Math.toDegrees(Math.atan2(deltaLon, deltaLat));
        DemColor color;

        if (OpenAirspace.bReady) {
            nrAirspaceFound = 0;
            Iterator<OpenAirspaceRec> it = OpenAirspace.airspacelst.iterator();
            while (it.hasNext()) {
                OpenAirspaceRec currAirspace;

                _x1 = 0;
                _y1 = 0;
                String airspaceDesc;
                try {
                    currAirspace = it.next();
                    airspaceDesc = String.format("%s LL FL%d", currAirspace.ac, currAirspace.al);
                }
                //catch (ConcurrentModificationException e) {
                catch (Exception e) {
                    break;
                }

                // Set the individual airspace colors
                if (currAirspace.ac.equals("A") && AirspaceClass.A)
                    color = new DemColor(0.37f, 0.62f, 0.42f); // ?
                else if (currAirspace.ac.equals("B") && AirspaceClass.B)
                    color = new DemColor(0.37f, 0.42f, 0.62f); // Dk mod Powder blue 0.6
                else if (currAirspace.ac.equals("C") && AirspaceClass.C)
                    color = new DemColor(0.37f, 0.42f, 0.62f); // Dk mod Powder blue 0.6
                else if (currAirspace.ac.equals("P") && AirspaceClass.P)
                    color = new DemColor(0.45f, 0.20f, 0.20f);
                else if (currAirspace.ac.equals("R") && AirspaceClass.R)
                    color = new DemColor(0.45f, 0.20f, 0.20f);
                else if (currAirspace.ac.equals("Q") && AirspaceClass.Q)
                    color = new DemColor(0.25f, 0.10f, 0.10f);
                else if (currAirspace.ac.equals("CTR") && AirspaceClass.CTR)
                    color = new DemColor(0.4f, 0.4f, 0.4f); // grey
                else continue; //color = new DemColor(0.4f, 0.4f, 0.4f);

                // Handle Monochrome
                if (colorTheme == 2) {
                    color.red = 0;
                    color.blue = 0;
                }

                Iterator<OpenAirspacePoint> it2 = currAirspace.pointList.iterator();
                while (it2.hasNext()) {
                    OpenAirspacePoint currAirPoint;
                    try {
                        currAirPoint = it2.next();
                    }
                    //catch (ConcurrentModificationException e) {
                    catch (Exception e) {
                        break;
                    }

                    dme = UNavigation.calcDme(LatValue, LonValue, currAirPoint.lat, currAirPoint.lon); // in ft

                    // Apply selection criteria
                    if (dme > MX_APT_SEEK_RNG)
                        break;

                    airspacepntRelBrg = UNavigation.calcRelBrg(LatValue, LonValue, currAirPoint.lat, currAirPoint.lon, DIValue);
                    x1 = mMapZoom * (dme * UTrig.icos(90 - (int) airspacepntRelBrg));
                    y1 = mMapZoom * (dme * UTrig.isin(90 - (int) airspacepntRelBrg));

                    if (_x1 != 0 || _y1 != 0) {
                        mLine.SetWidth(8);
                        mLine.SetColor(color.red, color.green, color.blue, 0.85f);
                        mLine.SetVerts(
                                _x1, _y1, z,
                                x1, y1, z
                        );
                        mLine.draw(matrix);
                    }
                    else {
                        // Draw the airspace description at the first coordinate
                        glText3.begin(color.red, color.green, color.blue, 0.95f, matrix);
                        glText3.drawCY(airspaceDesc, x1, y1 + glText3.getCharHeight() / 2);
                        glText3.end();
                    }
                    _x1 = x1;
                    _y1 = y1;

                }
            }
        }
    }

    //---------------------------------------------------------------------------
    // DMAP serviceability ... aka the Red X's
    //

    // Artificial Horizon serviceability
    public void setServiceableMap()
    {
        ServiceableMap = true;
    }

    public void setUnServiceableMap()
    {
        ServiceableMap = false;
    }

}
