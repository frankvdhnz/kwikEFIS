/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.pfd;

import static player.efis.pfd.R.id;
import static player.efis.pfd.R.string;
import static player.efis.pfd.R.xml;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import player.efis.common.AircraftData;
import player.efis.common.AirspaceClass;
import player.efis.common.DemGTOPO30;
import player.efis.common.OpenSky;
import player.efis.common.SensorComplementaryFilter;
import player.efis.common.WxRadarMap;
import player.efis.common.orientation_t;
import player.efis.common.prefs_t;
import player.ulib.UMath;
import player.ulib.UNavigation;
import player.ulib.Unit;

public class PFDMainActivity extends EFISMainActivity implements Listener, SensorEventListener, LocationListener
{
    public static final String PREFS_NAME = string.app_name + ".prefs";
    private MainSurfaceView mGLView;
    //private GLSurfaceView mGLView;

    //
    //  Add the action bar buttons
    //
    //Menu menu;
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed()
    {
        if (mGLView.mRenderer.fatFingerActive) {
            mGLView.mRenderer.fatFingerActive = false;
            mGLView.mRenderer.setSpinnerParams();
        }
        else openOptionsMenu();
    }

    // This method is called once the menu is selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case id.settings:
                // Launch settings activity
                Intent i = new Intent(this, PrefSettings.class);
                startActivity(i);
                break;
            case id.manage:
                // Launch manage activity
                Intent j = new Intent(this, PrefManage.class);
                startActivity(j);
                break;
            case id.quit:
                // Quit the app
                if (!bLockedMode) {
                    finish();
                    //System.exit(0); // This is brutal, it does not exit gracefully
                }
                else Toast.makeText(this, "Locked Mode: Active", Toast.LENGTH_SHORT).show();
                break;
            // more code...
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    // This code will catch the actual keypress.
    // for now we will leave the menu bar in case it is needed later
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Keep the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Create a GLSurfaceView instance and set it
        // as the ContentView for this Activity

        /*
        EFISRenderer Renderer;  // normally this would be private but we want to access the sel wpt from main activity
        if (activeModule == module_t.PFD)
            Renderer = new PFDRenderer(this);
        else if (activeModule == module_t.MFD)
            Renderer = new MFDRenderer(this);
        else
            Renderer = new CFDRenderer(this);

        mGLView = new CFDSurfaceView(this);
        mGLView.mRenderer = Renderer;
        mGLView.setRenderer((GLSurfaceView.Renderer) Renderer);    // from constructor
        mGLView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // from constructor
        setContentView(mGLView);
         //*/
        createView();

        // Get the version number of the app
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        }
        catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        Toast.makeText(this, "Kwik EFIS version: " + version, Toast.LENGTH_LONG).show();

        try {
            mSensorManager = (SensorManager) getSystemService(Activity.SENSOR_SERVICE);
            registerSensorManagerListeners();
        }
        catch (Exception e) {
            Toast.makeText(this, "Hardware compatibility issue", Toast.LENGTH_LONG).show();
        }

        // testing for lightweight -- may or may not use
        sensorComplementaryFilter = new SensorComplementaryFilter();

        // Location
        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location provider -> use default
        // Criteria criteria = new Criteria();
        // provider = locationManager.getBestProvider(criteria, false);
        provider = LocationManager.GPS_PROVIDER;  // Always use the GPS as the provide
        locationManager.requestLocationUpdates(provider, GPS_UPDATE_PERIOD, GPS_UPDATE_DISTANCE, this);  // 400ms or 1m
        locationManager.addGpsStatusListener(this);
        //locationManager.addNmeaListener(this);
        Location location = locationManager.getLastKnownLocation(provider);
        // end location

        // Preferences
        PreferenceManager.setDefaultValues(this, xml.settings, false);
        PreferenceManager.setDefaultValues(this, xml.manage, false);

        // Set the window to be full brightness
        WindowManager.LayoutParams layout = getWindow().getAttributes();
        layout.screenBrightness = -1f;  // 1f = full bright 0 = selected
        getWindow().setAttributes(layout);

        // Initial lazy loads
        lazyLoadDemGTOPO30DemBuffer(gps_lat, gps_lon);  // Do this before the databases; Sets DemGTOPO30.lat0 and DemGTOPO30.lon0,
        lazyLoadGpxDatabase(gps_lat, gps_lon);
        lazyLoadOpenAirspaceDatabase(gps_lat, gps_lon);

        // This should never happen but we catch and force it to something valid it just in case
        if (mGLView.mRenderer.mSelWptName.length() != 4) mGLView.mRenderer.mSelWptName = "ZZZZ";
        if (mGLView.mRenderer.mAltSelName.length() != 5) mGLView.mRenderer.mSelWptName = "00000";

        createMediaPlayer();
        mGLView.setTheme(colorTheme);

        // Overall the device is now ready.
        // The individual elements will be enabled or disabled by the location provided
        // based on availability
        mGLView.setServiceableDevice();
    }

    void createView()
    {
        EFISRenderer Renderer;
        if (activeModule == 2) //module_t.PFD)
            Renderer = new PFDRenderer(this);
        else if (activeModule == 1) //module_t.MFD)
            Renderer = new MFDRenderer(this);
        else
            Renderer = new CFDRenderer(this); // default to C7FD

        mGLView = new MainSurfaceView(this);
        mGLView.mRenderer = Renderer;
        mGLView.setRenderer((GLSurfaceView.Renderer) Renderer);    // from constructor
        mGLView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // from constructor
        setContentView(mGLView);

        // Apply the saved settings to this view
        restorePersistentVars();
    }

    protected void pauseActivity()
    {
        mGLView.onPause();
    }

    protected void resumeActivity()
    {
        mGLView.onResume();
    }

    //
    // Sensor methods
    //
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
        switch (sensor.getType()) {
            case Sensor.TYPE_GRAVITY:
                break;

            case Sensor.TYPE_MAGNETIC_FIELD:
                // SENSOR_STATUS_ACCURACY_HIGH
                // SENSOR_STATUS_ACCURACY_MEDIUM
                // SENSOR_STATUS_ACCURACY_LOW
                // SENSOR_STATUS_UNRELIABLE
                break;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                sensorComplementaryFilter.setAccel(event.values);
                break;

            case Sensor.TYPE_GYROSCOPE:
                sensorComplementaryFilter.setGyro(event.values);
                break;

            case Sensor.TYPE_MAGNETIC_FIELD:
                //b2b2 sensorFusion.setMagnet(event.values);
                break;

            case Sensor.TYPE_ORIENTATION:
                orientationAzimuth = event.values[0];
                orientationPitch = -event.values[2];
                orientationRoll = -event.values[1];
                break;

            case Sensor.TYPE_PRESSURE:
                // altitude = mSensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, event.values[0]);
                break;
        }
        //updateEFIS();
    }


    @Override
    public void onLocationChanged(Location location)
    {
        if (bSimulatorActive) return;
        if (bStratuxActive) return;

        gps_lat = (float) location.getLatitude();
        gps_lon = (float) location.getLongitude();
        gps_agl = DemGTOPO30.calculateAgl(gps_lat, gps_lon, gps_altitude);

        if (location.hasSpeed()) {
            gps_speed = location.getSpeed();
            if (gps_speed == 0) gps_speed = 0.01f;  // nip div zero issues in the bud
            mGLView.setServiceableAsi();
            hasSpeed = true;
        }
        else {
            mGLView.setUnServiceableAsi();
            hasSpeed = false;
        }

        if (location.hasAltitude()) {
            gps_altitude = (float) location.getAltitude();
            gps_rateOfClimb = calculateRateOfClimb(gps_altitude);
            mGLView.setServiceableAlt();
        }
        else {
            mGLView.setUnServiceableAlt();
        }

        if (location.hasBearing()) {
            if (gps_speed > 2) {
                gps_course = (float) Math.toRadians(location.getBearing());
                gps_rateOfTurn = calculateRateOfTurn(gps_course);
                mGLView.setServiceableDi();
            }
        }
        else {
            gps_course = 0;
            gps_rateOfTurn = 0;
            mGLView.setUnServiceableDi();
        }

        if (location.hasSpeed() && location.hasBearing()) {
            mGLView.setServiceableAh();
        }
        else {
            mGLView.setUnServiceableAh();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {
        if (!LocationManager.GPS_PROVIDER.equals(provider)) {
            return;
        }
        // do something
    }

    protected void savePersistentVars()
    {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        // Save persistent preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("WptSelName", mGLView.mRenderer.mSelWptName);
        editor.putString("WptSelComment", mGLView.mRenderer.mSelWptComment);
        editor.putFloat("WptSelLat", mGLView.mRenderer.mSelWptLat);
        editor.putFloat("WptSelLon", mGLView.mRenderer.mSelWptLon);
        editor.putFloat("mAltSelValue", mGLView.mRenderer.mAltSelValue);
        editor.putString("mAltSelName", mGLView.mRenderer.mAltSelName);
        editor.putFloat("mObsValue", mGLView.mRenderer.mObsValue);
        editor.putFloat("mMapZoom", mGLView.mRenderer.mMapZoom);

        // Save last known location
        editor.putFloat("GpsLat", gps_lat);
        editor.putFloat("GpsLon", gps_lon);

        // Commit the edits
        editor.commit();
    }

    private void restorePersistentVars()
    {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        // Restore persistent preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        mGLView.mRenderer.mSelWptName = settings.getString("WptSelName", "ZZZZ");
        mGLView.mRenderer.mSelWptComment = settings.getString("WptSelComment", "Null Island");
        mGLView.mRenderer.mSelWptLat = settings.getFloat("WptSelLat", 00f);
        mGLView.mRenderer.mSelWptLon = settings.getFloat("WptSelLon", 00f);
        mGLView.mRenderer.mAltSelValue = settings.getFloat("mAltSelValue", 0f);
        mGLView.mRenderer.mAltSelName = settings.getString("mAltSelName", "00000");
        mGLView.mRenderer.mObsValue = settings.getFloat("mObsValue", 0f);
        mGLView.mRenderer.mMapZoom = settings.getFloat("mMapZoom", mGLView.mRenderer.MIN_ZOOM);  // Zoom multiplier for map. 1 (max out) to 200 (max in)

        // Restore last known location
        gps_lat = settings.getFloat("GpsLat", 0);
        gps_lon = settings.getFloat("GpsLon", 0);
    }


    /*
    // This must be implemented otherwise the older
    // systems does not get seem to get updates.
    @Override
    public void onGpsStatusChanged(int state)
    {
        setGpsStatus();
    }

    @Override
    public void onProviderEnabled(String provider)
    {
        Toast.makeText(this, "Enabled new provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider)
    {
        Toast.makeText(this, "Disabled provider " + provider, Toast.LENGTH_SHORT).show();
    }
	*/
    // end location abs ------------------------


    protected void setUserPrefs()
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        mGLView.setPrefs(prefs_t.DEM, settings.getBoolean("displayDEM", false));
        mGLView.setPrefs(prefs_t.WX, settings.getBoolean("displayWX", false));
        mGLView.setPrefs(prefs_t.TAPE, settings.getBoolean("displayTape", true));
        mGLView.setPrefs(prefs_t.MIRROR, settings.getBoolean("displayMirror", false));
        mGLView.setPrefs(prefs_t.INFO_PAGE, settings.getBoolean("infoPage", true));
        mGLView.setPrefs(prefs_t.REMOTE_INDICATOR, settings.getBoolean("displayRMI", true));
        mGLView.setPrefs(prefs_t.FLIGHT_DIRECTOR, settings.getBoolean("displayFlightDirector", false));
        mGLView.setPrefs(prefs_t.CCIP, settings.getBoolean("displayCCIP", false));
        // Only used in PFD
        mGLView.setPrefs(prefs_t.AH_COLOR, settings.getBoolean("displayAHColor", true));
        mGLView.setPrefs(prefs_t.HITS, settings.getBoolean("displayHITS", false));
        // Only used in MFD
        mGLView.setPrefs(prefs_t.AIRSPACE, settings.getBoolean("displayAirspace", true));
        AirspaceClass.A = settings.getBoolean("classA", true);
        AirspaceClass.B = settings.getBoolean("classB", true);
        AirspaceClass.C = settings.getBoolean("classC", true);
        AirspaceClass.D = settings.getBoolean("classD", true);
        AirspaceClass.E = settings.getBoolean("classE", true);
        AirspaceClass.F = settings.getBoolean("classF", true);
        AirspaceClass.G = settings.getBoolean("classG", true);
        AirspaceClass.P = settings.getBoolean("classP", true);
        AirspaceClass.R = settings.getBoolean("classR", true);
        AirspaceClass.Q = settings.getBoolean("classQ", true);
        AirspaceClass.CTR = settings.getBoolean("classCTR", true);

        bLockedMode = settings.getBoolean("lockedMode", false);
        sensorBias = Float.parseFloat(settings.getString("sensorBias", "0.15f"));

        // If we changed the local/global status force a Gpx reload
        // Note the menu option is to *enable* the local gpx mode. It is off by default.
        boolean rv = settings.getBoolean("dynamicGpxDatabase", false);
        if (bDynamicGpx != rv) {
            bDynamicGpx = rv;
            mGLView.setBannerMsg(true, "LOADING DATABASE");
            lazyLoadGpxDatabase(gps_lat, gps_lon);
        }

        // If we changed to Demo mode, use the current GPS as seed location
        if (bSimulatorActive != settings.getBoolean("simulatorActive", false)) {
            //if (gps_lon != 0 && gps_lat != 0) {
            if (!UNavigation.isNullLatLon(gps_lat, gps_lon)) {
                _gps_lon = gps_lon;
                _gps_lat = gps_lat;
            }
        }
        bSimulatorActive = settings.getBoolean("simulatorActive", false);

        bStratuxActive = settings.getBoolean("stratuxActive", false);
        bHudMode = settings.getBoolean("displayMirror", false);
        bWeatherActive = settings.getBoolean("displayWX", false);

        // If the aircraft is changed, update the paramaters
        String s = settings.getString("AircraftModel", "RV8");
        AircraftData.setAircraftData(s); //mGLView.mRenderer.setAircraftData(s);  // refactored  to static model

        // landscape / portrait mode toggle
        bLandscapeMode = settings.getBoolean("landscapeMode", false);
        if (bLandscapeMode) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            mGLView.mRenderer.Layout = CFDRenderer.layout_t.LANDSCAPE;
        }
        else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            mGLView.mRenderer.Layout = CFDRenderer.layout_t.PORTRAIT;
        }
        bLandscapeMode = settings.getBoolean("landscapeMode", false);

        // If we changed display schemes, a color gamma rec-calc is required
        int _colorTheme = Integer.parseInt(settings.getString("colorTheme", "0"));
        if (colorTheme != _colorTheme) {
            colorTheme = _colorTheme;

            createView();
            mGLView.setTheme(colorTheme);
            mGLView.invalidate();
            mGLView.setServiceableDevice();
            setUserPrefs();  // make a recursive call
        }

        // If we changed active modules, new view is required
        int _activeModule = Integer.parseInt(settings.getString("activeModule", "0"));
        if (activeModule != _activeModule) {
            activeModule = _activeModule;

            createView();
            mGLView.setServiceableDevice();
            setUserPrefs();  // make a recursive call
        }
    }

    //
    // Flight Path Vector (the birdie)
    //
    protected void updateFPV()
    {
        float deltaA;

        //deltaA = UNavigation.compassRose180(gps_course - orientationAzimuth);
        float fpvX = (float) filterfpvX.runningAverage(Math.toDegrees(Math.atan2(-gyro_rateOfTurn * 100.0f, gps_speed))); // a point 100m ahead of nose
        float fpvY = (float) filterfpvY.runningAverage(Math.toDegrees(Math.atan2(gps_rateOfClimb, gps_speed)));    // simple RA of the two velocities

        mGLView.setFPV(fpvX, fpvY); // need to clean this up
    }

    //
    // Force a blank screen and no birdie
    //
    protected void forceBlankScreen()
    {
        rollValue = 0;
        pitchValue = -270;
        mGLView.setFPV(180, 180);
    }

    //
    // Stratux handler
    //
    protected int handleStratux()
    {
        int rv = super.handleStratux();

        if (rv == STRATUX_OK) {
            hasGps = true;
            hasSpeed = true;

            mGLView.setServiceableDevice();
            mGLView.setServiceableDi();
            mGLView.setServiceableAsi();
            mGLView.setServiceableAlt();
            mGLView.setServiceableAh();
            mGLView.setServiceableMap();
            mGLView.setDisplayAirport(true);
            updateFPV();
            mGLView.setBannerMsg(false, " ");
        }
        else if (rv == STRATUX_SERVICE) {
            // no loop running, no hope of restart -- it is hopeless
            mGLView.setUnServiceableDevice();
            mGLView.setBannerMsg(true, "STRATUX SERVICE");
        }
        else if (rv == STRATUX_DEVICE) {
            // no pulse
            mGLView.setUnServiceableDevice();
            mGLView.setBannerMsg(true, "STRATUX DEVICE");
        }
        else if (rv == STRATUX_WIFI) {
            // No Wifi
            mGLView.setUnServiceableDevice();
            mGLView.setBannerMsg(true, "STRATUX WIFI");
        }
        else if (rv == STRATUX_GPS) {
            // No GPS, but we may still have attitude since all the vital checks are prior
            hasGps = false;
            hasSpeed = false;

            mGLView.setServiceableDevice();
            mGLView.setServiceableAh();
            mGLView.setUnServiceableMap();  // map needs GPS
            mGLView.setUnServiceableDi();   // also does the rose
            mGLView.setUnServiceableAsi();
            mGLView.setUnServiceableAlt();
            mGLView.setDisplayAirport(false);
            mGLView.setBannerMsg(true, " "); // "STRATUX GPS"
        }
        return rv;
    }

    //
    // Android  handler
    //
    protected boolean handleAndroid()
    {
        float[] gyro = new float[3]; // gyroscope vector, rad/sec
        float[] accel = new float[3]; // accelerometer vector, m/s^2

        //
        // Read the Sensors
        //
        if (bLandscapeMode) {
            if (bHudMode)
                sensorComplementaryFilter.setOrientation(orientation_t.HORIZONTAL_LANDSCAPE);
            else sensorComplementaryFilter.setOrientation(orientation_t.VERTICAL_LANDSCAPE);
        }
        else {
            if (bHudMode)
                sensorComplementaryFilter.setOrientation(orientation_t.HORIZONTAL_PORTRAIT);
            else sensorComplementaryFilter.setOrientation(orientation_t.VERTICAL_PORTRAIT);
        }

        sensorComplementaryFilter.getGyro(gyro);      // Use the gyroscopes for the attitude
        sensorComplementaryFilter.getAccel(accel);    // Use the accelerometer for G and slip

        if (bLandscapeMode) {
            gyro_rateOfTurn = (float) filterRateOfTurnGyro.runningAverage(-gyro[0]);
            slipValue = (float) filterSlip.runningAverage(Math.toDegrees(Math.atan2(accel[1], accel[0]))); //?
        }
        else {
            gyro_rateOfTurn = (float) filterRateOfTurnGyro.runningAverage(-gyro[1]);
            slipValue = (float) -filterSlip.runningAverage(Math.toDegrees(Math.atan2(accel[0], accel[1])));
        }

        loadfactor = sensorComplementaryFilter.getLoadFactor();
        loadfactor = filterG.runningAverage(loadfactor);

        //
        // Check if we have a valid GPS
        //
        hasGps = isGPSAvailable();

        //
        // Calculate the augmented bank angle and also the flight path vector
        //
        float deltaA, fpvX = 0, fpvY = 0;
        if (hasGps) {
            mGLView.setServiceableDevice();
            mGLView.setServiceableMap();
            if (gps_speed > 5) {
                // Testing shows that reasonable value is sensorBias of 75% gps and 25% gyro on most older devices,
                // if the gyro and accelerometer are good quality and stable, use sensorBias of 100%
                rollValue = sensorComplementaryFilter.calculateBankAngle((sensorBias) * gyro_rateOfTurn + (1 - sensorBias) * gps_rateOfTurn, gps_speed);
                pitchValue = sensorComplementaryFilter.calculatePitchAngle(gps_rateOfClimb, gps_speed);

                // the Flight Path Vector (FPV)
                updateFPV();
                mGLView.setDisplayAirport(true);
            }
            if (gps_speed < 9) {  // m/s
                // taxi mode
                rollValue = 0;
                pitchValue = 0;
            }
        }
        else {
            // No GPS no speed ... no idea what the AH is
            //forceBlankScreen();
            mGLView.setUnServiceableDevice();
            mGLView.setUnServiceableMap();    // map needs GPS
        }

        // for debug - set to true
        if (false) {
            hasGps = true;          //debug
            hasSpeed = true;        //debug
            gps_speed = 3;//60;     //m/s debug
            gps_rateOfClimb = 1.0f; //m/s debug
        }
        // end debug
        return true;
    }


    protected void updateDEM()
    {
        mGLView.setBannerMsg(false, " "); // clear any banners

        //
        // Handle the DEM buffer.
        // Load new data to the buffer when the horizon gets close to the edge or
        // if we have gone off the current tile.
        //
        float dem_dme = UNavigation.calcDme(DemGTOPO30.lat0, DemGTOPO30.lon0, gps_lat, gps_lon);

        //
        // Load new data into the buffer when the horizon gets close to the edge
        // and see if we are stuck on null island or even on the tile
        //

        // Departure gets very small near the pole - Causes a display problem
        // If we are close the arctic circle (arbitrarily) dispense with the DEM_HORIZON
        float dem_veil = dem_dme + (Math.abs(gps_lat) > 60 ? 0 : DemGTOPO30.DEM_HORIZON);
        float dem_threshold = UNavigation.calcDeparture(DemGTOPO30.BUFX / 4, gps_lat);

        //Log.d("kwik", "gps_lat= " + gps_lat + " gps_lon= " + gps_lon +  " dem_dme= " + dem_dme + " dem_veil= " + dem_veil + " dem_threshold= " + dem_threshold );

        if ( (dem_veil > dem_threshold)
            || ((dem_dme != 0) && (!DemGTOPO30.isOnTile(gps_lat, gps_lon))) ) {

            if (bDynamicGpx) {
                mGLView.setBannerMsg(true, "LOADING DATABASE");
                lazyLoadGpxDatabase(gps_lat, gps_lon);
            }

            mGLView.setBannerMsg(true, "LOADING TERRAIN");
            //int rv = DemGTOPO30.loadDemBuffer(gps_lat, gps_lon);
            lazyLoadDemGTOPO30DemBuffer(gps_lat, gps_lon);

            mGLView.setBannerMsg(true, "LOADING AIRSPACE");
            //OpenAirspace.loadDatabase(gps_lat, gps_lon);
            lazyLoadOpenAirspaceDatabase(gps_lat, gps_lon);

            mGLView.setBannerMsg(false, " ");  // clear any banners

            // Handle errors
            if (DemGTOPO30.demLastError != DemGTOPO30.DEM_OK) {
                switch (DemGTOPO30.demLastError) {
                    case DemGTOPO30.DEM_SYN_NOT_INSTALLED:
                        mGLView.setBannerMsg(true, "DATAPAC " + DemGTOPO30.getRegionDatabaseName(gps_lat, gps_lon) + " MISSING");
                        break;
                    case DemGTOPO30.DEM_TERRAIN_ERROR:
                        mGLView.setBannerMsg(true, "DATAPAC " + DemGTOPO30.getRegionDatabaseName(gps_lat, gps_lon) + " ERROR");
                        break;
                }
            }
            updateWX();  // This is a useful place to catch large movements and sync the WX
        }
    }

    protected void updateWX()
    {
        time.setToNow();

        // Only update if active and older than 10 minutes 1000*60*10 ms
        //if (bWeatherActive && (time.toMillis(true) - wxTimeStamp) > 600000) {
        {
            mGLView.setBannerMsg(false, " "); // clear any banners
            mGLView.setBannerMsg(true, "LOADING WEATHER");

            // Load from OpenWeatherMap
            updateWeather();
            wxTimeStamp = time.toMillis(true);
            mGLView.setBannerMsg(false, " "); // clear any banners
        }
    }

    protected void updateTraffic()
    {
        super.updateTraffic();
    }

    //-------------------------------------------------------------------------
    // Effectively the main execution loop. updateEFIS will get called when
    // something changes, eg a sensor has new data or new gps fix becomes available.
    //
    protected void updateEFIS()
    {
        ctr++;

        //
        // Mode handlers
        //
        if (bSimulatorActive) {
            // Simulator handler
            mGLView.setSimulatorActive(true, "SIMULATOR");
            Simulate();
            // Set the GPS flag to true and
            // make all the instruments serviceable
            mGLView.setServiceableDevice();
            mGLView.setServiceableDi();
            mGLView.setServiceableAsi();
            mGLView.setServiceableAlt();
            mGLView.setServiceableAh();
            mGLView.setServiceableMap();
            mGLView.setDisplayAirport(true);
            hasGps = true;
            hasSpeed = true;
        }
        else {
            // Clear the simulator splash
            mGLView.setSimulatorActive(false, " ");

            // Handle Stratux or Android sensors
            if (bStratuxActive) {
                // We are set to SENSOR_DELAY_UI approx 60ms
                // 5 x 60 will give 3 updates a second
                // if (ctr % 5 == 0)
                  handleStratux();
            }
            else {
                handleAndroid();

                // Apply a little filtering to the pitch, bank (only for Android, not Stratux)
                pitchValue = filterPitch.runningAverage(pitchValue);
                rollValue = filterRoll.runningAverage(UNavigation.compassRose180(rollValue));
            }
        }

        //
        // Get the battery percentage
        //
        float batteryPct = getRemainingBattery();


        // for debug - set to true
        if (false) {
            hasGps = true;          //debug
            hasSpeed = true;        //debug
            gps_speed = 3;//60;     //m/s debug
            gps_rateOfClimb = 1.0f; //m/s debug
            gps_course = (float) Math.toRadians(1); // debug
        }
        // end debug

        float gps_dme = mGLView.mRenderer.getSelWptDme(); // in nm

        //
        // Pass the values to mGLView for updating
        //
        String s; // general purpose string

        mGLView.setPitch(pitchValue);                             // in degrees
        mGLView.setRoll(rollValue);                               // in degrees
        mGLView.setGForce(loadfactor);                            // in gunits
        mGLView.setSlip(slipValue);                               // in degrees
        mGLView.setVSI((int) Unit.MeterPerSecond.toFeetPerMinute(gps_rateOfClimb));  // in fpm
        mGLView.setTurn((float) Math.toDegrees((sensorBias) * gyro_rateOfTurn + (1 - sensorBias) * gps_rateOfTurn)); // in deg/sec
        mGLView.setHeading((float) Math.toDegrees(gps_course));   // in degrees
        mGLView.setALT((int) Unit.Meter.toFeet(gps_altitude));    // in Feet
        mGLView.setAGL((int) Unit.Meter.toFeet(gps_agl));         // in Feet
        mGLView.setASI(Unit.MeterPerSecond.toKnots(gps_speed));   // in knots
        mGLView.setLatLon(gps_lat, gps_lon);

        mGLView.setBatteryPct(batteryPct);                        // in percentage

        s = String.format("GPS %d / %d", gps_infix, gps_insky);
        mGLView.setGpsStatus(s);

        // Handle traffic
        if (bStratuxActive && (mStratux != null)) {
            mGLView.setTargets(mStratux.getTargetList()); // Stratux traffic list
        }
        OpenSky.setLatLon(gps_lat, gps_lon);
        mGLView.setTargets(OpenSky.getTargetList()); // OpenSky traffic list

        // Handle weather
        WxRadarMap.setLatLon(gps_lat, gps_lon);
        mGLView.setActiveDevice(getCurrentSsid());

        //
        // Audio cautions and messages
        //
        if (hasGps) {
            try {
                // We have new traffic
                // Implement a suitable detection and reporting strategy
                if (mGLView.getProximityAlert()) {
                    if (!mpCautionTraffic.isPlaying()) mpCautionTraffic.start();
                    mGLView.setProximityAlert(false);
                }

                // We are stalling, advise captain "Crash" of his imminent flight emergency
                if (hasSpeed
                        && (gps_speed < 3 + AircraftData.Vs0 / 2) // m/s, warn 3 m/s before stall
                        && (gps_speed > 3)                        // m/s, warn only when faster than 3 m/s
                        && (gps_agl > 10)) {                      // meters
                    if (!mpStall.isPlaying()) mpStall.start();
                }

                // Sigh ... Now, we are plummeting to the ground, inform the prick on the stick
                if (gps_rateOfClimb < -10) { // m ~ 2000 fpm //gps_rateOfClimb * 196.8504f for fpm
                    if (!mpSinkRate.isPlaying()) mpSinkRate.start();
                }

                // We are at risk of becoming a wet spot somewhere on terra firma
                if (DemGTOPO30.demDataValid) {
                    // Play the "caution terrain" song above Vx
                    if ((gps_speed > AircraftData.Vx / 2)  // m/s
                            && (gps_agl > 0)
                            && (gps_agl < 100)) { // meters
                        if (!mpCautionTerrian.isPlaying()) mpCautionTerrian.start();
                    } // caution terrain

                    // Play the "five hundred" song when decending through 500ft
                    // and closer than 2nm from dest
                    if ((_gps_agl > 152.4f)  // 500ft
                            && (gps_agl <= 152.4f) // 500ft
                            && (gps_dme < 2)) {
                        if (!mpFiveHundred.isPlaying()) mpFiveHundred.start();
                    }
				} // DemGTOPO30 required options

            }
            catch (IllegalStateException e) {
                //e.printStackTrace();
            }
        }
        _gps_agl = gps_agl; // save the previous height agl
    }


    protected void Simulate()
    {
        // 0=CFD, 1=MFD, 2=PFD
        if (activeModule == 1) SimulateMfd();
        else SimulateCfd();

        super.Simulate();
    }

    protected void SimulateCfd()
    {
        pitchValue = -sensorComplementaryFilter.getPitch();
        rollValue = -sensorComplementaryFilter.getRoll();

        pitchValue = 0.05f * (float) Math.random() + 0.75f * UMath.clamp(mGLView.mRenderer.commandPitch, -3, 3);
        rollValue = 0.05f * (float) Math.random() + 0.75f * mGLView.mRenderer.commandRoll;
    }

    protected void SimulateMfd()
    {
        int target_agl = 3000; //ft

        pitchValue = -sensorComplementaryFilter.getPitch();
        rollValue = -sensorComplementaryFilter.getRoll();

        pitchValue = 0.125f * (float) Math.random() - 0.75f * UMath.clamp(gps_agl - Unit.Feet.toMeter(target_agl), -3, 3);
        rollValue = 1.125f * (float) Math.random() + 0.75f * mGLView.mRenderer.commandRoll;
    }

}


