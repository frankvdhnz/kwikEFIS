/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.pfd;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import player.efis.common.AircraftData;
import player.efis.common.DemColor;
import player.efis.common.DemGTOPO30;
import player.efis.common.Gpx;
import player.efis.common.Point;
import player.gles20.GLText;
import player.gles20.Line;
import player.gles20.PolyLine;
import player.gles20.Polygon;
import player.gles20.Square;
import player.gles20.Triangle;
import player.ulib.UTrig;
import player.ulib.Unit;

public class PFDRenderer extends EFISRenderer implements GLSurfaceView.Renderer
{
    private static final String TAG = "PFDRenderer";
    protected boolean ServiceableAh;       // Flag to indicate AH failure

    public PFDRenderer(Context context)
    {
        super(context);
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config)
    {
        // Set the background frame color
        GLES20.glClearColor(backShadeR, backShadeG, backShadeB, 1.0f);

        mTriangle = new Triangle();
        mSquare = new Square();
        mLine = new Line();
        mPolyLine = new PolyLine();
        mPolygon = new Polygon();

        // Create the GLText
        glText3 = new GLText(context.getAssets());
        glText4 = new GLText(context.getAssets());
        glText5 = new GLText(context.getAssets());
        glText6 = new GLText(context.getAssets());
        glText7 = new GLText(context.getAssets());

        glText8 = new GLText(context.getAssets());
        glText9 = new GLText(context.getAssets());
        //roseTextScale = 1f;
    }

    @Override
    public void onDrawFrame(GL10 gl)
    {
        // Draw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        DemIncAngle = 1 + (int) Math.min(5,  Math.abs(ROTValue*1.0));
        //Log.d("kwik", " DemIncAngle:" + DemIncAngle + " ROTValue:"+ ROTValue + "  VSIValue:"  + VSIValue);

        DrawFramePfd(gl);
    }

    //---------------------------------------------------------------
    // Primary-Flight-Display Drawing (EFIS)
    //
    private void DrawFramePfd(GL10 gl)
    {
        // Set the camera position (View matrix)
        if (displayMirror)
            Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);  // Mirrored View
        else
            Matrix.setLookAtM(mViewMatrix, 0, 0, 0, +3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);   // Normal View

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        // Create a rotation for the horizon
        Matrix.setRotateM(mRotationMatrix, 0, rollRotation, 0, 0, 1.0f);

        // Combine the rotation matrix with the projection and camera view
        // Note that the mMVPMatrix factor *must be first* in order
        // for the matrix multiplication product to be correct.
        Matrix.multiplyMM(scratch1, 0, mMVPMatrix, 0, mRotationMatrix, 0);
        Matrix.multiplyMM(scratch2, 0, mMVPMatrix, 0, mRotationMatrix, 0);

        // Pitch
        if (Layout == layout_t.LANDSCAPE) {
            portraitOffset = 0;
            // Slide pitch to current value
            Matrix.translateM(scratch1, 0, 0, pitchTranslation, 0); // apply the pitch
        }
        else {
            portraitOffset = 0.40f;  // the magic number for portrait offset

            // Slide pitch to current value adj for portrait
            float Adjust = pixH2 * portraitOffset;                           //portraitOffset set to 0.4
            Matrix.translateM(scratch1, 0, 0, pitchTranslation + Adjust, 0); // apply the pitch and offset
        }

        // Slide ALT to current value
        Matrix.translateM(altMatrix, 0, mMVPMatrix, 0, 0, -MSLTranslation, 0); // apply the altitude

        // Slide IAS to current value
        Matrix.translateM(iasMatrix, 0, mMVPMatrix, 0, 0, -IASTranslation, 0); // apply the altitude

        zfloat = 0;

        // fatFingerActive just for performance
        if (displayDEM && !fatFingerActive) {
            // Make the blue sky for the DEM.
            // Note: it extends a little below the horizon when AGL is positive
            renderDEMSky(scratch1);
            // underground is not valid
            if ((AGLValue > 0) && (DemGTOPO30.demDataValid)) renderDEMTerrainPfd(scratch1);
        }
        else if (displayAHColors) renderAHColors(scratch1);

        renderPitchMarkers(scratch1);

        // FPV only means anything if we have speed and rate of climb, ie altitude
        if (displayFPV) renderFPV(scratch1);      // must be on the same matrix as the Pitch
        if (displayAirport) renderAPT(scratch1);  // must be on the same matrix as the Pitch
        if (true) renderTargets(scratch1);        // TODO: Add control of targets sometime ...
        if (displayCCIP) renderCCIP(mMVPMatrix);

        if (displayHITS) renderHITS(scratch1);    // will not keep in the viewport

        // Flight Director - FD
        if (displayFlightDirector) {
            // Create a rotation for the Flight director
            Matrix.setRotateM(mFdRotationMatrix, 0, rollRotation + FDRotation, 0, 0, 1.0f);  // fd rotation
            Matrix.multiplyMM(fdMatrix, 0, mMVPMatrix, 0, mFdRotationMatrix, 0);

            if (Layout == layout_t.LANDSCAPE) {
                // Slide FD to current value
                Matrix.translateM(fdMatrix, 0, 0, pitchTranslation - FDTranslation, 0); // apply the altitude
            }
            else {
                // Slide pitch to current value adj for portrait
                float Adjust = pixH2 * portraitOffset;
                // Slide FD to current value
                Matrix.translateM(fdMatrix, 0, 0, pitchTranslation - FDTranslation + Adjust, 0); // apply the altitude
            }
            renderFlightDirector(fdMatrix);
        }

        // Remote Magnetic Inidicator - RMI
        if (displayRMI) {
            float xlx;
            float xly;

            // Add switch for orientation
            if (Layout == layout_t.LANDSCAPE) {
                // Landscape
                xlx = -0.74f * pixW2; // top left
                xly = 0.50f * pixH2;  // top left
                roseScale = 0.44f;
                GLES20.glViewport(0, 0, pixW, pixH);
            }
            else {
                //Portrait
                xlx = 0;
                xly = -0.44f * pixH2;
                roseScale = 0.52f;
            }

            Matrix.translateM(mMVPMatrix, 0, xlx, xly, 0);
            // Create a rotation for the RMI
            Matrix.setRotateM(mRmiRotationMatrix, 0, DIValue, 0, 0, 1);  // compass rose rotation
            Matrix.multiplyMM(rmiMatrix, 0, mMVPMatrix, 0, mRmiRotationMatrix, 0);
            renderBearingTxt(mMVPMatrix);
            renderFixedCompassMarkers(mMVPMatrix);
            renderACSymbol(mMVPMatrix, false);
            Matrix.translateM(mMVPMatrix, 0, -xlx, -xly, 0);

            renderCompassRose(rmiMatrix);
            renderBearing(rmiMatrix);
            //renderAutoWptDetails(mMVPMatrix);
            GLES20.glViewport(0, 0, pixW, pixH);  // fullscreen
        }


        if (Layout == layout_t.PORTRAIT) {
            // Slide pitch to current value adj for portrait
            int Adjust = (int) (pixH2 * portraitOffset);
            GLES20.glViewport(0,  Adjust, pixW, pixH); // Portrait //
        }
        renderFixedHorizonMarkers();
        renderRollMarkers(scratch2);

        //-----------------------------
        if (Layout == layout_t.LANDSCAPE)
            GLES20.glViewport(pixW / 30, pixH / 30, pixW - pixW / 15, pixH - pixH / 15); //Landscape
        else
            GLES20.glViewport(pixW / 100, pixH * 40 / 100, pixW - pixW / 50, pixH - pixH * 42 / 100); // Portrait

        if (displayTape) {
            renderAltMarkers(altMatrix);
            renderASIMarkers(iasMatrix);
            renderVSIMarkers(mMVPMatrix);
        }

        {
            int Adjust = (int) (pixH2 * portraitOffset);
            GLES20.glViewport(0, Adjust, pixW, pixH + Adjust); // Portrait //

            float xlx;
            float xly;

            //if (displayTape == true) renderFixedVSIMarkers(mMVPMatrix); // todo: maybe later

            xlx = 1.14f * pixM2;
            xly = -0.5f * portraitOffset * pixH2; // half of tape viewport //-0.7f * pixH2;
            Matrix.translateM(mMVPMatrix, 0, xlx, xly, 0);
            renderFixedAltMarkers(mMVPMatrix);
            Matrix.translateM(mMVPMatrix, 0, -xlx, -xly, 0);

            xly = -0.5f * pixH2;
            Matrix.translateM(mMVPMatrix, 0, xlx, xly, 0);
            renderFixedRadAltMarkers(mMVPMatrix);   // AGL
            Matrix.translateM(mMVPMatrix, 0, -xlx, -xly, 0);

            xlx = -1.10f * pixM2;
            xly = -0.5f * portraitOffset * pixH2; // half of tape viewport //-0.7f * pixH2;
            Matrix.translateM(mMVPMatrix, 0, xlx, xly, 0);
            renderFixedASIMarkers(mMVPMatrix);
            Matrix.translateM(mMVPMatrix, 0, -xlx, -xly, 0);

            xlx = 0;
            if (Layout == layout_t.PORTRAIT) xly = +0.40f * pixM2;
            else xly = +0.90f * pixM2;
            Matrix.translateM(mMVPMatrix, 0, xlx, xly, 0);
            renderFixedDIMarkers(mMVPMatrix);
            //renderHDGValue(mMVPMatrix);
            Matrix.translateM(mMVPMatrix, 0, -xlx, -xly, 0);

            GLES20.glViewport(0, 0, pixW, pixH);  // reset to fullscreen
        }

        //-----------------------------
        if (displayInfoPage) {
            dimAcillaryDetails(mMVPMatrix, wxDim);
            renderAncillaryDetails(mMVPMatrix);
            renderBatteryPct(mMVPMatrix);
        }

        if (displayFlightDirector) {
            dimAutoWptDetails(mMVPMatrix, wxDim);
            renderAutoWptDetails(mMVPMatrix);
        }
        renderTurnMarkers(mMVPMatrix);
        renderSlipBall(mMVPMatrix);
        renderGForceValue(mMVPMatrix);

        if (bBannerActive) renderBannerMsg(mMVPMatrix);
        if (bSimulatorActive) renderSimulatorActive(mMVPMatrix);

        // Do this last so that every else wil be dimmed for fatfinger entry
        if (displayFlightDirector || displayRMI || displayHITS) {
            dimSelWptDetails(mMVPMatrix, wxDim);
            renderSelWptDetails(mMVPMatrix);
            renderSelWptValue(mMVPMatrix);

            dimSelAltValue(mMVPMatrix, wxDim);
            renderSelAltValue(mMVPMatrix);
        }

        // Do this after any dimming
        if (!ServiceableDevice) renderUnserviceableDevice(mMVPMatrix);
        if (!ServiceableAh) renderUnserviceableAh(mMVPMatrix);
        if (!ServiceableAlt) renderUnserviceableAlt(mMVPMatrix);
        if (!ServiceableAsi) renderUnserviceableAsi(mMVPMatrix);
        if (!ServiceableDi) {
            renderUnserviceableDi(mMVPMatrix);
            renderUnserviceableCompassRose(mMVPMatrix);
        }
        if (!Gpx.bReady) renderUnserviceableSelWpt(mMVPMatrix);

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        // Adjust the viewport based on geometry changes, such as screen rotation
        GLES20.glViewport(0, 0, width, height);

        // this projection matrix is applied to  object coordinates in the onDrawFrame() method
        //b2 Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
        //Matrix.frustumM(mProjMatrix, 0, -ratio, ratio, -1, 1, 2, 7); // - this apparently fixed for the Samsung S2?

        //b2 start
        // Capture the window scaling for use by the rendering functions
        pixW = width;
        pixH = height;
        pixW2 = pixW / 2;
        pixH2 = pixH / 2;

        pixM = Math.min(pixW, pixH);

        // because the ascpect ratio is different in landscape and portrait (due to menu bar)
        // we just fudge it as 88% throughout,  looks OK in landscape as well
        pixM = pixM * 88 / 100;
        pixM2 = pixM / 2;

        setSpinnerParams(); // Set up the spinner locations and SelWpt display

        // Set the window size specific scales, positions and sizes (nothing dynamic yet...)
        pitchInView = 25.0f;     // degrees to display from horizon to top of viewport
        IASInView = 40.0f;       // IAS units to display from center to top of viewport
        MSLInView = 300.0f;      // IAS units to display from center to top of viewport

        // this projection matrix is applied to  object coordinates in the onDrawFrame() method
        float ratio = (float) width / height;
        //Matrix.frustumM(mProjectionMatrix, 0, -ratio*pixH2, ratio*pixH2, -pixH2, pixH2, 3f, 7f); // all the rest
        Matrix.frustumM(mProjectionMatrix, 0, -ratio * pixH2, ratio * pixH2, -pixH2, pixH2, 2.99f, 75f); //hack for Samsung G2

        // Load the font from file (set size + padding), creates the texture
        // NOTE: after a successful call to this the font is ready for rendering!
        // glText.load( "Roboto-Regular.ttf", 14, 2, 2 );  // Create Font (Height: 14 Pixels / X+Y Padding 2 Pixels)
        glText3.load("square721_cn_bt_roman.ttf", pixM * 3 / 100, 2, 2);  // Create Font (Height: ~ 3%  / X+Y Padding 2 Pixels)
        glText4.load("square721_cn_bt_roman.ttf", pixM * 4 / 100, 2, 2);  // Create Font (Height: ~ 4%  / X+Y Padding 2 Pixels)
        glText5.load("square721_cn_bt_roman.ttf", pixM * 5 / 100, 2, 2);  // Create Font (Height: ~ 5%  / X+Y Padding 2 Pixels)
        glText6.load("square721_cn_bt_roman.ttf", pixM * 6 / 100, 2, 2);  // Create Font (Height: ~ 6%  / X+Y Padding 2 Pixels)
        glText7.load("square721_cn_bt_roman.ttf", pixM * 7 / 100, 2, 2);  // Create Font (Height: ~ 7%  / X+Y Padding 2 Pixels)

        glText8.load("square721_cn_bt_roman.ttf", pixM * 8 / 100, 2, 2);  // Create Font (Height: ~ 8%  / X+Y Padding 2 Pixels)
        glText9.load("square721_cn_bt_roman.ttf", pixM * 9 / 100, 2, 2);  // Create Font (Height: ~ 9%  / X+Y Padding 2 Pixels)

        // 10 and 18

        // enable texture + alpha blending
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);
    }

    @Override
    protected void renderUnserviceableDevice(float[] matrix)
    {
        renderUnserviceableAh(matrix);
        renderUnserviceableCompassRose(matrix);
        renderUnserviceableDi(matrix);
        renderUnserviceableAlt(matrix);
        renderUnserviceableAsi(matrix);
    }

    //
    // project
    //
    // relbrg in degrees
    // dme in nm
    // elev in m
    @Override
    protected Point project(float relbrg, float dme)
    {
        float pixPerDegree = pixM / pitchInView;
        // note: we do not take apt elevation into account
        return new Point(
            (float) (+pixPerDegree * relbrg),
            (float) (-pixPerDegree * Math.toDegrees(Math.atan2(MSLValue, Unit.NauticalMile.toFeet(dme))))
        );
    }


    @Override
    protected Point project(float relbrg, float dme, float elev)
    {
        float pixPerDegree = pixM / pitchInView;

        // note: we take apt elevation into account
        //float y1 = (float) (-Math.toDegrees(UTrig.fastArcTan2(MSLValue - z1 * 3.28084f, dme_ft)) * pixPerDegree);

        //float dme_ft = dme * 6080;
        //float y = (float) (-Math.toDegrees(UTrig.fastArcTan2(MSLValue - elev * 3.28084f, dme_ft)) * pixPerDegree);
        float y =  (float) (-pixPerDegree * Math.toDegrees(Math.atan2(MSLValue - Unit.Meter.toFeet(elev), Unit.NauticalMile.toFeet(dme))));

        return new Point(
                (float) (+pixPerDegree * relbrg),
                (float) y
        );
    }

    //-------------------------------------------------------------------------
    // Set the spinner control parameters
    //
    public void setSpinnerParams()
    {
        // This code determines where the spinner control
        // elements are displayed. Used by WPT and ALT
        if (Layout == layout_t.LANDSCAPE) {
            // Landscape --------------
            lineAutoWptDetails = 0.00f;
            lineAncillaryDetails = -0.30f;

            if (fatFingerActive) {
                selWptDec = 0.75f * pixH2;
                selWptInc = 0.45f * pixH2;
                selAltDec = -0.45f * pixH2;
                selAltInc = -0.75f * pixH2;

                lineC = 0.2f;
                leftC = -0.55f;
                spinnerStep = 0.25f;
                spinnerTextScale = 2;
            }
            else {
                // Top
                selWptDec = 0.90f * pixH2;
                selWptInc = 0.74f * pixH2;
                selAltDec = -0.74f * pixH2;
                selAltInc = -0.90f * pixH2;

                lineC = 0.50f;
                leftC = 0.6f;
                spinnerStep = 0.1f;
                spinnerTextScale = 1;
            }
        }
        else {
            // Portrait ---------------
            lineAutoWptDetails = -0.50f;
            lineAncillaryDetails = -0.85f;

            if (fatFingerActive) {
                selWptDec = 0.7f * pixH2;
                selWptInc = 0.4f * pixH2;
                selAltDec = -0.4f * pixH2;
                selAltInc = -0.7f * pixH2;

                lineC = 0.15f;
                leftC = -0.75f;
                spinnerStep = 0.5f;
                spinnerTextScale = 2;
            }
            else {
                selWptDec = -0.30f * pixH2;
                selWptInc = -0.41f * pixH2;
                selAltDec = -0.80f * pixH2;
                selAltInc = -0.91f * pixH2;

                lineC = -0.55f;
                leftC = 0.6f;
                spinnerStep = 0.1f;
                spinnerTextScale = 1;
            }
        }
    }

    //-------------------------------------------------------------------------
    // Render the Digital Elevation Model (DEM) - EFIS.
    //
    // This is the meat and potatoes of the synthetic vision implementation
    // The loops are very performance intensive, therefore all the hardcoded
    // magic numbers
    //
    protected void renderDEMTerrainPfd(float[] matrix)
    {
        float z, pixPerDegree, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, zav;
        float lat, lon;
        pixPerDegree = pixM / pitchInView;
        z = zfloat;

        float dme;             //in nm
        float step = 0.50f;    //in nm, normally this should be = gridy
        float agl_ft;          //in Feet

        // oversize 20% a little to help with
        // bleed through caused by itrig truncating
        float gridy = 0.5f;    //in nm
        float gridx = DemIncAngle;   // 1.0f;    //in degree

        float dme_ft;            // =  60 * 6080 * Math.hypot(deltaLon, deltaLat);  // ft
        float demRelBrg;         // = DIValue + Math.toDegrees(Math.atan2(deltaLon, deltaLat));
        float caution;
        final float cautionMin = 0.2f;
        final float IASValueThreshold = AircraftData.Vx; //1.5f * Vs0;
        final float viewCone = 25;

        mSquare.SetWidth(1);

        for (dme = 0; dme <= DemGTOPO30.DEM_HORIZON; dme += step) {
            for (demRelBrg = -viewCone; demRelBrg < viewCone; demRelBrg = demRelBrg + DemIncAngle) {  //1

                dme_ft = dme * 6080;
                lat = LatValue + dme / 60 * UTrig.icos((int) (DIValue + demRelBrg));
                lon = LonValue + dme / 60 * UTrig.isin((int) (DIValue + demRelBrg));
                z1 = DemGTOPO30.getElev(lat, lon);
                x1 = demRelBrg * pixPerDegree;
                y1 = (float) (-Math.toDegrees(UTrig.fastArcTan2(MSLValue - z1 * 3.28084f, dme_ft)) * pixPerDegree);

                lat = LatValue + dme / 60 * UTrig.icos((int) (DIValue + demRelBrg + gridx));
                lon = LonValue + dme / 60 * UTrig.isin((int) (DIValue + demRelBrg + gridx));
                z2 = DemGTOPO30.getElev(lat, lon);
                x2 = (demRelBrg + gridx) * pixPerDegree;
                y2 = (float) (-Math.toDegrees(UTrig.fastArcTan2(MSLValue - z2 * 3.28084f, dme_ft)) * pixPerDegree);

                dme_ft = (dme + gridy) * 6080;
                lat = LatValue + (dme + gridy) / 60 * UTrig.icos((int) (DIValue + demRelBrg + gridx));
                lon = LonValue + (dme + gridy) / 60 * UTrig.isin((int) (DIValue + demRelBrg + gridx));
                z3 = DemGTOPO30.getElev(lat, lon);
                x3 = (demRelBrg + gridx) * pixPerDegree;
                y3 = (float) (-Math.toDegrees(UTrig.fastArcTan2(MSLValue - z3 * 3.28084f, dme_ft)) * pixPerDegree);

                lat = LatValue + (dme + gridy) / 60 * UTrig.icos((int) (DIValue + demRelBrg));
                lon = LonValue + (dme + gridy) / 60 * UTrig.isin((int) (DIValue + demRelBrg));
                z4 = DemGTOPO30.getElev(lat, lon);
                x4 = (demRelBrg) * pixPerDegree;
                y4 = (float) (-Math.toDegrees(UTrig.fastArcTan2(MSLValue - z4 * 3.28084f, dme_ft)) * pixPerDegree);

                //
                //  77%
                //
                //   Triangle #2   Triangle #1
                //    +             +--+
                //    |\             \ |
                //    | \             \|
                //    +--+             +
                //

                // Triangle #1 --------------
                zav = z1;  // in m asml
                DemColor color = DemGTOPO30.getColor((short) zav);

                // Handle Monochrome
                if (colorTheme == 2) {
                    color.red = 0;
                    color.blue = 0;
                }

                caution = cautionMin + (color.red + color.green + color.blue);
                agl_ft = MSLValue - zav * 3.28084f;  // in ft

                if (agl_ft > 1000) mTriangle.SetColor(color.red, color.green, color.blue, 1);                     // Enroute
                else if (IASValue < IASValueThreshold) mTriangle.SetColor(color.red, color.green, color.blue, 1); // Taxi or approach
                else if (agl_ft > 200) mTriangle.SetColor(caution, caution, 0, 1f);                               // Proximity notification (yellow)
                else mTriangle.SetColor(caution, 0, 0, 1f);                                                       // Proximity warning (red)

                mTriangle.SetVerts(
                        x1, y1, z,
                        x2, y2, z,
                        x4, y4, z);
                mTriangle.draw(matrix);

                // Triangle #2 --------------
                zav = (z1 + z2) / 2; // take the simple average
                color = DemGTOPO30.getColor((short) zav);

                // Handle Monochrome
                if (colorTheme == 2) {
                    color.red = 0;
                    color.blue = 0;
                }

                caution = cautionMin + (color.red + color.green + color.blue);
                agl_ft = MSLValue - zav * 3.28084f;  // in ft

                if (agl_ft > 1000) mTriangle.SetColor(color.red, color.green, color.blue, 1);                     // Enroute
                else if (IASValue < IASValueThreshold) mTriangle.SetColor(color.red, color.green, color.blue, 1); // Taxi or  approach
                else if (agl_ft > 200) mTriangle.SetColor(caution, caution, 0, 1f);  // Proximity notification
                else mTriangle.SetColor(caution, 0, 0, 1f);                          // Proximity warning

                mTriangle.SetVerts(
                        x2, y2, z,
                        x3, y3, z,
                        x4, y4, z);
                mTriangle.draw(matrix);

                /*
                //
                //  69%
                //
                //   Square
                //   4    3
                //    +--+
                //    |  |
                //    |  |
                //    +--+
                //   1    2

                    zav = z1;  // use the
                    getColor((short) zav);
                    agl_ft = MSLValue - zav*3.28084f;  // in ft

                    if (agl_ft > 100) mSquare.SetColor(red, green, blue, 1);                      // Enroute
                    else if (IASValue < IASValueThreshold) mTriangle.SetColor(red, green, blue, 1); // Taxi or  apporach
                    else mSquare.SetColor(caution, 0, 0, 1f);                                     // Proximity warning

                    float[] squarePoly = {
                            x1, y1, z,
                            x2, y2, z,
                            x3, y3, z,
                            x4, y4, z
                    };
                    mSquare.SetVerts(squarePoly);
                    mSquare.draw(matrix);
                */

            }
        }
    }

    //-------------------------------------------------------------------------
    // Airspace
    //
    protected void renderAirspacePfd(float[] matrix)
    {
        // Maybe later
    }


    //---------------------------------------------------------------------------
    // EFIS serviceability ... aka the Red X's
    //

    // Artificial Horizon serviceability
    public void setServiceableAh()
    {
        ServiceableAh = true;
    }

    public void setUnServiceableAh()
    {
        ServiceableAh = false;
    }

/*
ninelima@yahoo.com

-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1.4.7-Mobility Email: (MingW32)

mQENBE0lygcBCADOwYhbGnXlfjtGeWdxRu6yNVSek6H5ZcgsAxD1C9RbVZ+iEDhD
/+u2KmaTWYmUzKP4UqrzNHa3OBZ34+Y+hfzxybCGAb2KxFf89S019jlXpJ/t1xTx
/cFx3U08a0jvl4Jcg+mXRMjAnYJuN8BAnubKOwZymAx0D2EwpHh9tPCSINMd2rAM
Ac9HUuneeuZFt2ZrIlDzyztxkfw+Vv8k6MPPZGDB9CiZAcRNneggDlA/u+THMRBy
MkoLtEulbjanAYmRQGY7V4aN7yOkCOakF1BDPCvyluYxlZaBc1Dvx2XdnGRODgrS
Ejn1YJddoSf2RsHjl5R6NV6VDTla5P6slX47ABEBAAG0JE5pbmUgTGltYSAoMTIz
KSA8bmluZWxpbWFAeWFob28uY29tPokBOAQTAQIAIgUCTSXKBwIbAwYLCQgHAwIG
FQgCCQoLBBYCAwECHgECF4AACgkQOcgn69ZgK8cSOggAyH7LIzHBTb56OTTpkA6g
FffnroLd/BQcZV87y1oAxIjJ4CCujb1r56CCpNdujQVB1twrzxc3LS6sM4vaXy6e
AbxvbSKjA19XZzvMpel8TbER2A1/daArby+Orkk/LOAK3W+ZHdXY9BYMBZ0puB28
HpS0Eg9XDgVQAgYbVwBSSKx2swfb3Ri2hDMjp4M4ZGOjSXK8VJKaecDgxdcJLhrb
WU7fdMDsUJIkjzvYRypiEPO1oRwbfR3Di7CSln4arwbztdgznvhIah8gqIGH8XVZ
s9mXaGFFNQUOiGYqk9sfmKNx7FLic4kGzpmnMkvLsTiHPRaTeC5eeBtMul1UrtaI
37kBDQRNJcoHAQgAtAXUyq2+mXKoSIjdTgcjhSX2rbW6LSfX11xCnj4qFc+ya7/+
wB1McFqAFp2zIsjYS1F+ny0H2ov/OPFsy4QeVIfZObSxXcXwDujSTDKIubdpMMTs
/4uJxI26A95LDxBsY887UgJllyTh6e8Ps51dPH/9Wh7kPopk2wHGmV6uoQoNnquw
LUekXSSXM0ePd0dnX0RVy63MUojOcNOGs9+cNeShlg0tOgbweWKLIG2+VcFKneuN
J1NUjHhF252l7U3552clKIE3TCHV1XWDKtmG6zqtFWg3JIOHnrw46P1rq5xc85vX
b+oAP/dw/Yw7jWtOw7QIevBpaHzS1lfrwCxzkQARAQABiQEfBBgBAgAJBQJNJcoH
AhsMAAoJEDnIJ+vWYCvHuk4IAMkYUuGRIHm6zyUYpmQbM3Se7C34QYb710PRXCyY
gNq33NfCn/dn5iNjnauXmFcv5ZyWUGiqoGInzKkzQMrCvnFURLvQ3s4mx7i5GTMN
1yCqTtTOCxJmF72r53IGISXXjxledfb7O5Pwh/EG7k6F/tzPxWRZ4n5pDQspoI+C
jehkRzzPhrOFQzdoruAv/sJWrhSZhRBt25w3VOfd640toePH5URmh1Zn76AAxat1
H06iCvbYnpywTDH8Z8Y66fyCkL5AjYy/H28FEkDw0iQDfVbeICEnZzMpxEV5Fb8w
ay8jq8SJcuQ2mpCJ+l8VP4WYOz1YFhNGXgZpl1sEvVjc2jE=
=5RxY
-----END PGP PUBLIC KEY BLOCK-----
*/


}
