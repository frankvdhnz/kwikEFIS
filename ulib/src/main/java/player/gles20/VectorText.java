/*
 * Copyright (C) 2019 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//
// OpenGL ES 1.0 simple vector font system.
//

package player.gles20;

import player.gles20.Line;
import player.gles20.PolyLine;

import android.opengl.Matrix;

public class VectorText {

    protected Line mLine;
    protected PolyLine mPolyLine;


    //--Constants--
    public final static int CHAR_START = 32;           // First Character (ASCII Code)
    public final static int CHAR_END = 126;            // Last Character (ASCII Code)
    public final static int CHAR_CNT = (((CHAR_END - CHAR_START) + 1) + 1);  // Character Count (Including Character to use for Unknown)

    public final static int CHAR_NONE = 32;            // Character to Use for Unknown (ASCII Code)
    public final static int CHAR_UNKNOWN = (CHAR_CNT - 1);  // Index of the Unknown Character

    public final static int FONT_SIZE_MIN = 6;         // Minumum Font Size (Pixels)
    public final static int FONT_SIZE_MAX = 180;       // Maximum Font Size (Pixels)

    public final static int CHAR_BATCH_SIZE = 24;     // Number of Characters to Render Per Batch
    // must be the same as the size of u_MVPMatrix
    // in BatchTextProgram
    private static final String TAG = "VTEXT";

    //--Members--

    int fontPadX, fontPadY;                            // Font Padding (Pixels; On Each Side, ie. Doubled on Both X+Y Axis)

    final float fontHeight;                                  // Font Height (Actual; Pixels)
    final float fontAscent;                                  // Font Ascent (Above Baseline; Pixels)
    final float fontDescent;                                 // Font Descent (Below Baseline; Pixels)

    final int textureId;                                     // Font Texture ID [NOTE: Public for Testing Purposes Only!]
    int textureSize;                                   // Texture Size for Font (Square) [NOTE: Public for Testing Purposes Only!]
    TextureRegion textureRgn;                          // Full Texture Region

    float charWidthMax;                                // Character Width (Maximum; Pixels)
    float charHeight;                                  // Character Height (Maximum; Pixels)
    final float[] charWidths;                          // Width of Each Character (Actual; Pixels)
    final TextureRegion[] charRgn;                           // Region of Each Character (Texture Coordinates)
    int cellWidth, cellHeight;                         // Character Cell Width/Height
    int rowCnt, colCnt;                                // Number of Rows/Columns

    float scaleX, scaleY;                              // Font Scale (X,Y Axis)
    float spaceX;                                      // Additional (X,Y Axis) Spacing (Unscaled)

    private int mColorHandle;                           // Shader color handle


    //--Constructor--
    // D: create arrays, and initialize the members
    public VectorText() {

        charWidths = new float[CHAR_CNT];               // Create the Array of Character Widths
        charRgn = new TextureRegion[CHAR_CNT];          // Create the Array of Character Regions

        // initialize remaining members
        fontPadX = 0;
        fontPadY = 0;

        fontHeight = 0.0f;
        fontAscent = 0.0f;
        fontDescent = 0.0f;

        textureId = -1;
        textureSize = 0;

        charWidthMax = 0;
        charHeight = 0;

        cellWidth = 0;
        cellHeight = 0;
        rowCnt = 0;
        colCnt = 0;

        scaleX = 1.0f;                                  // Default Scale = 1 (Unscaled)
        scaleY = 1.0f;                                  // Default Scale = 1 (Unscaled)
        spaceX = 0.0f;
    }


    //--Load Font--
    // description
    //    this will load the specified font file, create a texture for the defined
    //    character range, and setup all required values used to render with it.
    // arguments:
    //    size - Requested pixel size of font (height)
    //    padX, padY - Extra padding per character (X+Y Axis); to prevent overlapping characters.
    public boolean load(int size, int padX, int padY) {
        // setup requested values
        fontPadX = padX;                                // Set Requested X Axis Padding
        fontPadY = padY;                                // Set Requested Y Axis Padding

        // determine the width of each character (including unknown character)
        // also determine the maximum character width
        char[] s = new char[2];                         // Create Character Array
        charWidthMax = 0;                  // Reset Character Width/Height Maximums
        float[] w = new float[2];                       // Working Width Value
        int cnt = 0;                                    // Array Counter
        for (char c = CHAR_START; c <= CHAR_END; c++) {  // FOR Each Character
            s[0] = c;                                    // Set Character
            //b2 -- paint.getTextWidths( s, 0, 1, w );           // Get Character Bounds
            charWidths[cnt] = w[0];                      // Get Width
            if (charWidths[cnt] > charWidthMax)        // IF Width Larger Than Max Width
                charWidthMax = charWidths[cnt];           // Save New Max Width
            cnt++;                                       // Advance Array Counter
        }
        s[0] = CHAR_NONE;                               // Set Unknown Character
        //b2 -- paint.getTextWidths( s, 0, 1, w );              // Get Character Bounds
        charWidths[cnt] = w[0];                         // Get Width
        if (charWidths[cnt] > charWidthMax)           // IF Width Larger Than Max Width
            charWidthMax = charWidths[cnt];              // Save New Max Width
        cnt++;                                          // Advance Array Counter

        // set character height to font height
        charHeight = fontHeight;                        // Set Character Height

        // find the maximum size, validate, and setup cell sizes
        cellWidth = (int) charWidthMax + (2 * fontPadX);  // Set Cell Width
        cellHeight = (int) charHeight + (2 * fontPadY);  // Set Cell Height
        int maxSize = Math.max(cellWidth, cellHeight);  // Save Max Size (Width/Height)
        if (maxSize < FONT_SIZE_MIN || maxSize > FONT_SIZE_MAX)  // IF Maximum Size Outside Valid Bounds
            return false;                                // Return Error

        // set texture size based on max font size (width or height)
        // NOTE: these values are fixed, based on the defined characters. when
        // changing start/end characters (CHAR_START/CHAR_END) this will need adjustment too!
        if (maxSize <= 24)
            textureSize = 256;
        else if (maxSize <= 40)
            textureSize = 512;
        else if (maxSize <= 80)
            textureSize = 1024;
        else
            textureSize = 2048;


        // calculate rows/columns
        // NOTE: while not required for anything, these may be useful to have :)
        colCnt = textureSize / cellWidth;               // Calculate Number of Columns
        rowCnt = (int) Math.ceil((float) CHAR_CNT / (float) colCnt);  // Calculate Number of Rows

        // render each of the characters to the canvas (ie. build the font map)
        float x = fontPadX;                             // Set Start Position (X)
        float y = (cellHeight - 1) - fontDescent - fontPadY;  // Set Start Position (Y)
        for (char c = CHAR_START; c <= CHAR_END; c++) {  // FOR Each Character
            s[0] = c;                                    // Set Character to Draw
            //b2 -- canvas.drawText( s, 0, 1, x, y, paint );     // Draw Character
            x += cellWidth;                              // Move to Next Character
            if ((x + cellWidth - fontPadX) > textureSize) {  // IF End of Line Reached
                x = fontPadX;                             // Set X for New Row
                y += cellHeight;                          // Move Down a Row
            }
        }
        s[0] = CHAR_NONE;                               // Set Character to Use for NONE

        // setup the array of character texture regions
        x = 0;                                          // Initialize X
        y = 0;                                          // Initialize Y
        for (int c = 0; c < CHAR_CNT; c++) {         // FOR Each Character (On Texture)
            charRgn[c] = new TextureRegion(textureSize, textureSize, x, y, cellWidth - 1, cellHeight - 1);  // Create Region for Character
            x += cellWidth;                              // Move to Next Char (Cell)
            if (x + cellWidth > textureSize) {
                x = 0;                                    // Reset X Position to Start
                y += cellHeight;                          // Move to Next Row (Cell)
            }
        }

        // create full texture region
        textureRgn = new TextureRegion(textureSize, textureSize, 0, 0, textureSize, textureSize);  // Create Full Texture Region

        // return success
        return true;                                    // Return Success
    }


    //--Begin/End Text Drawing--
    // D: call these methods before/after (respectively all draw() calls using a text instance
    //    NOTE: color is set on a per-batch basis, and fonts should be 8-bit alpha only!!!
    // A: red, green, blue - RGB values for font (default = 1.0)
    //    alpha - optional alpha value for font (default = 1.0)
    // 	  vpMatrix - View and projection matrix to use
    // R: [none]
    public void begin(float[] vpMatrix) {
        begin(1.0f, 1.0f, 1.0f, 1.0f, vpMatrix);                // Begin with White Opaque
    }

    public void begin(float alpha, float[] vpMatrix) {
        begin(1.0f, 1.0f, 1.0f, alpha, vpMatrix);               // Begin with White (Explicit Alpha)
    }

    public void begin(float red, float green, float blue, float alpha, float[] vpMatrix) {
        initDraw(red, green, blue, alpha);
    }

    void initDraw(float red, float green, float blue, float alpha) {
        // pass on to lines
    }

    public void end() {
    }

    //--Draw Text--
    // D: draw text at the specified x,y position
    // A: text - the string to draw
    //    x, y, z - the x, y, z position to draw text at (bottom left of text; including descent)
    //    angleDeg - angle to rotate the text
    // R: [none]
    public void draw(String text, float x, float y, float z, float angleDegX, float angleDegY, float angleDegZ) {
        float chrHeight = cellHeight * scaleY;          // Calculate Scaled Character Height
        float chrWidth = cellWidth * scaleX;            // Calculate Scaled Character Width
        int len = text.length();                        // Get String Length
        x += (chrWidth / 2.0f) - (fontPadX * scaleX);  // Adjust Start X
        y += (chrHeight / 2.0f) - (fontPadY * scaleY);  // Adjust Start Y

        // create a model matrix based on x, y and angleDeg
        float[] modelMatrix = new float[16];
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.translateM(modelMatrix, 0, x, y, z);
        Matrix.rotateM(modelMatrix, 0, angleDegZ, 0, 0, 1);
        Matrix.rotateM(modelMatrix, 0, angleDegX, 1, 0, 0);
        Matrix.rotateM(modelMatrix, 0, angleDegY, 0, 1, 0);

        float letterX = 0;

        for (int i = 0; i < len; i++) {              // FOR Each Character in String
            int c = (int) text.charAt(i) - CHAR_START;  // Calculate Character Index (Offset by First Char in Font)
            if (c < 0 || c >= CHAR_CNT)                // IF Character Not In Font
                c = CHAR_UNKNOWN;                         // Set to Unknown Character Index

            letterX += (charWidths[c] + spaceX) * scaleX;    // Advance X Position by Scaled Character Width
        }
    }

    public void draw(String text, float x, float y, float z, float angleDegZ) {
        draw(text, x, y, z, 0, 0, angleDegZ);
    }

    public void draw(String text, float x, float y, float angleDeg) {
        draw(text, x, y, 0, angleDeg);
    }

    public void draw(String text, float x, float y) {
        draw(text, x, y, 0, 0);
    }

    //--Draw Text Centered--
    // D: draw text CENTERED at the specified x,y position
    // A: text - the string to draw
    //    x, y, z - the x, y, z position to draw text at (bottom left of text)
    //    angleDeg - angle to rotate the text
    // R: the total width of the text that was drawn
    public float drawC(String text, float x, float y, float z, float angleDegX, float angleDegY, float angleDegZ) {
        float len = getLength(text);                  // Get Text Length
        draw(text, x - (len / 2.0f), y - (getCharHeight() / 2.0f), z, angleDegX, angleDegY, angleDegZ);  // Draw Text Centered
        return len;                                     // Return Length
    }

    public float drawC(String text, float x, float y, float z, float angleDegZ) {
        return drawC(text, x, y, z, 0, 0, angleDegZ);
    }

    public float drawC(String text, float x, float y, float angleDeg) {
        return drawC(text, x, y, 0, angleDeg);
    }

    public float drawC(String text, float x, float y) {
        float len = getLength(text);                  // Get Text Length
        return drawC(text, x - (len / 2.0f), y - (getCharHeight() / 2.0f), 0);

    }

    public float drawCX(String text, float x, float y) {
        float len = getLength(text);                  // Get Text Length
        draw(text, x - (len / 2.0f), y);            // Draw Text Centered (X-Axis Only)
        return len;                                     // Return Length
    }

    public void drawCY(String text, float x, float y) {
        draw(text, x, y - (getCharHeight() / 2.0f));  // Draw Text Centered (Y-Axis Only)
    }

    //--Set Scale--
    // D: set the scaling to use for the font
    // A: scale - uniform scale for both x and y axis scaling
    //    sx, sy - separate x and y axis scaling factors
    // R: [none]
    public void setScale(float scale) {
        scaleX = scaleY = scale;                        // Set Uniform Scale
    }

    public void setScale(float sx, float sy) {
        scaleX = sx;                                    // Set X Scale
        scaleY = sy;                                    // Set Y Scale
    }

    //--Get Scale--
    // D: get the current scaling used for the font
    // A: [none]
    // R: the x/y scale currently used for scale
    public float getScaleX() {
        return scaleX;                                  // Return X Scale
    }

    public float getScaleY() {
        return scaleY;                                  // Return Y Scale
    }

    //--Set Space--
    // D: set the spacing (unscaled; ie. pixel size) to use for the font
    // A: space - space for x axis spacing
    // R: [none]
    public void setSpace(float space) {
        spaceX = space;                                 // Set Space
    }

    //--Get Space--
    // D: get the current spacing used for the font
    // A: [none]
    // R: the x/y space currently used for scale
    public float getSpace() {
        return spaceX;                                  // Return X Space
    }

    //--Get Length of a String--
    // D: return the length of the specified string if rendered using current settings
    // A: text - the string to get length for
    // R: the length of the specified string (pixels)
    public float getLength(String text) {
        float len = 0.0f;                               // Working Length
        int strLen = text.length();                     // Get String Length (Characters)
        for (int i = 0; i < strLen; i++) {           // For Each Character in String (Except Last
            int c = (int) text.charAt(i) - CHAR_START;  // Calculate Character Index (Offset by First Char in Font)
            if ((c > 127) || (c < 0))
                return 0; // catch bug with non ascii
            len += (charWidths[c] * scaleX);           // Add Scaled Character Width to Total Length
        }
        len += (strLen > 1 ? ((strLen - 1) * spaceX) * scaleX : 0);  // Add Space Length
        return len;                                     // Return Total Length
    }

    //--Get Width/Height of Character--
    // D: return the scaled width/height of a character, or max character width
    //    NOTE: since all characters are the same height, no character index is required!
    //    NOTE: excludes spacing!!
    // A: chr - the character to get width for
    // R: the requested character size (scaled)
    public float getCharWidth(char chr) {
        int c = chr - CHAR_START;                       // Calculate Character Index (Offset by First Char in Font)
        return (charWidths[c] * scaleX);              // Return Scaled Character Width
    }

    public float getCharWidthMax() {
        return (charWidthMax * scaleX);               // Return Scaled Max Character Width
    }

    public float getCharHeight() {
        return (charHeight * scaleY);                 // Return Scaled Character Height
    }

    //--Get Font Metrics--
    // D: return the specified (scaled) font metric
    // A: [none]
    // R: the requested font metric (scaled)
    public float getAscent() {
        return (fontAscent * scaleY);                 // Return Font Ascent
    }

    public float getDescent() {
        return (fontDescent * scaleY);                // Return Font Descent
    }

    public float getHeight() {
        return (fontHeight * scaleY);                 // Return Font Height (Actual)
    }


    //-------------------------------------------------------------------------
    // Vector Text
    //
    protected void renderVChar(float[] matrix, char c) {
        float z = 0; //zfloat;
        float m = 200; // in pixels //pixM2;


        mPolyLine.SetColor(1, 1, 0, 1);
        mPolyLine.SetWidth(1);

        switch (c) {

            // space 0x20
            case ' ':
                break;

            // 0..9  0x30
            case '0': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.8f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.5f * m, 0.8f * m, z,
                        0.5f * m, 0.1f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.2f * m, 0.4f * m, z,
                        0.3f * m, 0.5f * m, z);
                mLine.draw(matrix);
                break;
            }
            case '1': {
                float[] vertPoly = {
                        // 6 x 9
                        0.2f * m, 0.8f * m, z,
                        0.3f * m, 0.9f * m, z,
                        0.3f * m, 0.0f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.4f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case '2': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.8f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.5f * m, 0.8f * m, z,
                        0.5f * m, 0.7f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.5f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 8;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case '3': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.8f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.5f * m, 0.8f * m, z,
                        0.5f * m, 0.55f * m, z,
                        0.4f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.4f * m, 0.45f * m, z,
                        0.5f * m, 0.35f * m, z,
                        0.5f * m, 0.1f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 13;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case '4': {
                float[] vertPoly = {
                        // 6 x 9
                        0.4f * m, 0.0f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.0f * m, 0.3f * m, z,
                        0.5f * m, 0.3f * m, z
                };
                mPolyLine.VertexCount = 4;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case '5': {
                float[] vertPoly = {
                        // 6 x 9
                        0.5f * m, 0.9f * m, z,
                        0.0f * m, 0.9f * m, z,
                        0.0f * m, 0.55f * m, z,
                        0.4f * m, 0.55f * m, z,
                        0.5f * m, 0.45f * m, z,
                        0.5f * m, 0.1f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case '6': {
                float[] vertPoly = {
                        // 6 x 9
                        0.4f * m, 0.9f * m, z,
                        0.2f * m, 0.9f * m, z,
                        0.0f * m, 0.6f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.5f * m, 0.1f * m, z,
                        0.5f * m, 0.35f * m, z,
                        0.4f * m, 0.45f * m, z,
                        0.0f * m, 0.45f * m, z
                };
                mPolyLine.VertexCount = 10;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case '7': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.1f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 3;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case '8': {
                float[] vertPoly = {
                        // 6 x 9
                        0.4f * m, 0.45f * m, z,
                        0.5f * m, 0.55f * m, z,
                        0.5f * m, 0.8f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.0f * m, 0.8f * m, z,
                        0.0f * m, 0.55f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.4f * m, 0.45f * m, z,

                        0.5f * m, 0.35f * m, z,
                        0.5f * m, 0.1f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z
                };
                mPolyLine.VertexCount = 16;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case '9': {
                float[] vertPoly = {
                        // 6 x 9
                        0.1f * m, 0.0f * m, z,
                        0.3f * m, 0.0f * m, z,
                        0.5f * m, 0.3f * m, z,
                        0.5f * m, 0.8f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.0f * m, 0.8f * m, z,
                        0.0f * m, 0.55f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.5f * m, 0.45f * m, z
                };
                mPolyLine.VertexCount = 10;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }

            // A..Z  ox41
            case 'A': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.3f * m, z,
                        0.3f * m, 0.9f * m, z,
                        0.6f * m, 0.3f * m, z,
                        0.6f * m, 0.0f * m, z,
                        0.6f * m, 0.3f * m, z,
                        0.0f * m, 0.3f * m, z
                };
                mPolyLine.VertexCount = 7;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'B': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.6f * m, 0.8f * m, z,
                        0.6f * m, 0.6f * m, z,
                        0.5f * m, 0.45f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.5f * m, 0.45f * m, z,
                        0.6f * m, 0.4f * m, z,
                        0.6f * m, 0.1f * m, z,
                        0.5f * m, 0.0f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.1f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 13;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'C': {
                float[] vertPoly = {
                        // 6 x 8
                        0.6f * m, 0.8f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.0f * m, 0.8f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.5f * m, 0.0f * m, z,
                        0.6f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 7;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'D': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.6f * m, 0.7f * m, z,
                        0.6f * m, 0.2f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.1f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 8;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'E': {
                float[] vertPoly = {
                        // 6 x 9
                        0.6f * m, 0.0f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.45f * m, z,
                        0.4f * m, 0.45f * m, z,
                        0.0f * m, 0.45f * m, z,
                        0.0f * m, 0.9f * m, z,
                        0.6f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 7;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                break;
            }
            case 'F': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.45f * m, z,
                        0.4f * m, 0.45f * m, z,
                        0.0f * m, 0.45f * m, z,
                        0.0f * m, 0.9f * m, z,
                        0.6f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 6;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'G': {
                float[] vertPoly = {
                        // 6 x 9
                        0.6f * m, 0.8f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.0f * m, 0.8f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.5f * m, 0.0f * m, z,
                        0.6f * m, 0.1f * m, z,
                        0.6f * m, 0.45f * m, z,
                        0.3f * m, 0.45f * m, z
                };
                mPolyLine.VertexCount = 10;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'H': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.45f * m, z,
                        0.6f * m, 0.45f * m, z,
                        0.6f * m, 0.9f * m, z,
                        0.6f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 6;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'I': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.2f * m, 0.9f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.2f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 6;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'J': {
                float[] vertPoly = {
                        // 6 x 9
                        0.6f * m, 0.9f * m, z,
                        0.6f * m, 0.2f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.0f * m, 0.2f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'K': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.9f * m, z,
                        0.0f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.6f * m, 0.9f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.6f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 7;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'L': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.6f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 3;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'M': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.9f * m, z,
                        0.3f * m, 0.45f * m, z,
                        0.6f * m, 0.9f * m, z,
                        0.6f * m, 0.0f * m, z,
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'N': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.9f * m, z,
                        0.6f * m, 0.0f * m, z,
                        0.6f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 4;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'O': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.2f * m, z,
                        0.0f * m, 0.7f * m, z,
                        0.2f * m, 0.9f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.6f * m, 0.7f * m, z,
                        0.6f * m, 0.2f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.0f * m, 0.2f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'P': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.9f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.6f * m, 0.8f * m, z,
                        0.6f * m, 0.55f * m, z,
                        0.5f * m, 0.45f * m, z,
                        0.0f * m, 0.45f * m, z
                };
                mPolyLine.VertexCount = 7;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'Q': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.2f * m, z,
                        0.0f * m, 0.7f * m, z,
                        0.2f * m, 0.9f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.6f * m, 0.7f * m, z,
                        0.6f * m, 0.3f * m, z,
                        0.3f * m, 0.0f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.0f * m, 0.2f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.6f * m, 0.0f * m, z,
                        0.3f * m, 0.3f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'R': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.9f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.6f * m, 0.8f * m, z,
                        0.6f * m, 0.55f * m, z,
                        0.5f * m, 0.45f * m, z,
                        0.0f * m, 0.45f * m, z,
                        0.3f * m, 0.45f * m, z,
                        0.6f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'S': {
                float[] vertPoly = {
                        // 6 x 9
                        0.6f * m, 0.8f * m, z,
                        0.5f * m, 0.9f * m, z,
                        0.1f * m, 0.9f * m, z,
                        0.0f * m, 0.8f * m, z,
                        0.0f * m, 0.6f * m, z,
                        0.1f * m, 0.5f * m, z,
                        0.5f * m, 0.4f * m, z,
                        0.6f * m, 0.3f * m, z,
                        0.6f * m, 0.1f * m, z,
                        0.5f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 12;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'T': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.6f * m, 0.9f * m, z,
                        0.3f * m, 0.9f * m, z,
                        0.3f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 4;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'U': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.6f * m, 0.2f * m, z,
                        0.6f * m, 0.0f * m, z,
                        0.6f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 7;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'V': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.3f * m, 0.0f * m, z,
                        0.6f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 3;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'W': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.3f * m, 0.45f * m, z,
                        0.5f * m, 0.0f * m, z,
                        0.6f * m, 0.9f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'X': {
                mLine.SetVerts(0.0f * m, 0.0f * m, z,
                        0.6f * m, 0.9f * m, z);
                mLine.draw(matrix);
                mLine.SetVerts(0.0f * m, 0.9f * m, z,
                        0.6f * m, 0.0f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'Y': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.3f * m, 0.45f * m, z,
                        0.6f * m, 0.9f * m, z,
                        0.3f * m, 0.45f * m, z,
                        0.3f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'Z': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.9f * m, z,
                        0.6f * m, 0.9f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.6f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 4;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }

            // a..z  ox61
            case 'a': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.1f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z,
                        0.4f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 10;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'b': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.1f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.8f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'c': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.1f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z
                };
                mPolyLine.VertexCount = 8;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'd': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.1f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.3f * m, 0.0f * m, z,
                        0.3f * m, 0.8f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'e': {
                float[] vertPoly = {
                        // 6 x 9
                        0.30f * m, 0.05f * m, z,
                        0.25f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.228f * m, z,
                        0.0f * m, 0.228f * m, z
                };
                mPolyLine.VertexCount = 10;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'f': {
                float[] vertPoly = {
                        // 6 x 9
                        0.1f * m, 0.0f * m, z,
                        0.1f * m, 0.7f * m, z,
                        0.2f * m, 0.8f * m, z,
                        0.3f * m, 0.8f * m, z,
                        0.4f * m, 0.7f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                mLine.SetVerts(0.0f * m, 0.5f * m, z,
                        0.1f * m, 0.5f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'g': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.1f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z,
                        0.3f * m, -0.2f * m, z,
                        0.2f * m, -0.3f * m, z,
                        0.1f * m, -0.3f * m, z,
                        0.0f * m, -0.2f * m, z,
                };
                mPolyLine.VertexCount = 13;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'h': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.8f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'i': {
                mLine.SetVerts(0.1f * m, 0.0f * m, z,
                        0.1f * m, 0.45f * m, z);
                mLine.draw(matrix);
                mLine.SetVerts(0.1f * m, 0.6f * m, z,
                        0.1f * m, 0.65f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'j': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.45f * m, z,
                        0.3f * m, -0.2f * m, z,
                        0.2f * m, -0.3f * m, z,
                        0.1f * m, -0.3f * m, z,
                        0.0f * m, -0.2f * m, z,
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                mLine.SetVerts(0.3f * m, 0.6f * m, z,
                        0.3f * m, 0.65f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'k': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.45f * m, z,
                        0.1f * m, 0.225f * m, z,
                        0.0f * m, 0.225f * m, z,
                        0.1f * m, 0.225f * m, z,
                        0.3f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                mLine.SetVerts(0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.8f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'l': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.8f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.1f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 3;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                mLine.SetVerts(0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.8f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'm': {
                float[] vertPoly = {
                        // 6 x 9
                        /*0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z,

                        0.3f * m, 0.35f * m, z,
                        0.4f * m, 0.45f * m, z,
                        0.5f * m, 0.45f * m, z,
                        0.6f * m, 0.35f * m, z,
                        0.6f * m, 0.1f * m, z*/

                        0.0f * m, 0.35f * m, z,
                        0.05f * m, 0.45f * m, z,
                        0.15f * m, 0.45f * m, z,
                        0.2f * m, 0.35f * m, z,
                        0.2f * m, 0.1f * m, z,

                        0.2f * m, 0.35f * m, z,
                        0.25f * m, 0.45f * m, z,
                        0.35f * m, 0.45f * m, z,
                        0.4f * m, 0.35f * m, z,
                        0.4f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 10;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.45f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'n': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.45f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'o': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.1f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'p': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.1f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.0f * m, 0.45f * m, z,
                        0.0f * m, -0.3f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'q': {
                float[] vertPoly = {
                        // 6 x 9
                        0.3f * m, 0.1f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z,
                        0.3f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 9;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.3f * m, 0.45f * m, z,
                        0.3f * m, -0.3f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'r': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z
                };
                mPolyLine.VertexCount = 4;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);

                mLine.SetVerts(0.0f * m, 0.0f * m, z,
                        0.0f * m, 0.45f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 's': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.1f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.3f * m, 0.1f * m, z,

                        0.2f * m, 0.20f * m, z,
                        0.1f * m, 0.25f * m, z,

                        0.0f * m, 0.35f * m, z,
                        0.1f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.35f * m, z
                };
                mPolyLine.VertexCount = 10;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 't': {
                float[] vertPoly = {
                        // 6 x 9
                        0.1f * m, 0.55f * m, z,
                        0.1f * m, 0.1f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.3f * m, 0.0f * m, z,
                        0.4f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                mLine.SetVerts(0.0f * m, 0.45f * m, z,
                        0.2f * m, 0.45f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'u': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.45f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.3f * m, 0.1f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                mLine.SetVerts(0.3f * m, 0.45f * m, z,
                        0.3f * m, 0.00f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'v': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.45f * m, z,
                        0.15f * m, 0.0f * m, z,
                        0.3f * m, 0.45f * m, z
                };
                mPolyLine.VertexCount = 3;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'w': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.45f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.2f * m, 0.45f * m, z,
                        0.3f * m, 0.0f * m, z,
                        0.4f * m, 0.45f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'x': {
                mLine.SetVerts(0.0f * m, 0.45f * m, z,
                        0.3f * m, 0.00f * m, z);
                mLine.draw(matrix);
                mLine.SetVerts(0.3f * m, 0.45f * m, z,
                        0.0f * m, 0.00f * m, z);
                mLine.draw(matrix);
                break;
            }
            case 'y': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.45f * m, z,
                        0.0f * m, 0.1f * m, z,
                        0.1f * m, 0.0f * m, z,
                        0.2f * m, 0.0f * m, z,
                        0.3f * m, 0.1f * m, z,
                        0.3f * m, 0.45f * m, z,
                        0.3f * m, -0.2f * m, z,
                        0.2f * m, -0.3f * m, z,
                        0.1f * m, -0.3f * m, z,
                        0.0f * m, -0.2f * m, z,

                };
                mPolyLine.VertexCount = 10;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }
            case 'z': {
                float[] vertPoly = {
                        // 6 x 9
                        0.0f * m, 0.45f * m, z,
                        0.3f * m, 0.45f * m, z,
                        0.0f * m, 0.0f * m, z,
                        0.3f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 4;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
            }

            default:
                float[] vertPoly = {
                        // 6 x 9
                        0.2f * m, 0.0f * m, z,
                        0.2f * m, 0.9f * m, z,
                        0.4f * m, 0.9f * m, z,
                        0.4f * m, 0.0f * m, z,
                        0.2f * m, 0.0f * m, z
                };
                mPolyLine.VertexCount = 5;
                mPolyLine.SetVerts(vertPoly);
                mPolyLine.draw(matrix);
                break;
        }

    }

    protected void renderVText(float[] matrix, String s) {
        renderVChar(matrix, 'z');
    }
}
