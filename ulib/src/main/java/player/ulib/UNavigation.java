/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.ulib;

public class UNavigation {

    public static final float EARTH_CIRC =   21600;  // Earth's circumference in nm
    public static final float EARTH_CIRC_2 = 10800;  // Half of Earth's circumference in nm
    public static final float EARTH_CIRC_4 =  5400;  // Quarter of Earth's circumference in nm
    public static final float EARTH_CIRC_8 =  2700;  // Quarter of Earth's circumference in nm

    public static final float EARTH_G_F =  32.174f;  // Earth Gravitational acceleration in feet/sec
    public static final float EARTH_G_M =  9.8067f;  // Earth Gravitational acceleration in meter/sec


    //-------------------------------------------------------------------------
    // Calculate the Departure (distance along a latitude)
    //
    public static float calcDeparture(float deltaLon, float lat)
    {
        //float d = deltaLon * UTrig.icos((int) lat);  // Euclidean distance
        return deltaLon * (float)  Math.cos(Math.toRadians(lat));
    }

    //-------------------------------------------------------------------------
    // Calculate the DME distance in nm
    //
    public static float calcDme(float lat1, float lon1, float lat2, float lon2)
    {
        float deltaLat = lat2 - lat1;
        float deltaLon = lon2 - lon1; // Cartesian distance
        float departure = calcDeparture(deltaLon, (lat1 + lat2)/2);  // Euclidean distance, mid lat

        //d =  364800 * Math.hypot(deltaLon, deltaLat);  // in ft, 1 deg of lat  6080 * 60 = 364,80 note hypot uses convergence and is very slow.
        return (float) (60 * Math.sqrt(departure * departure + deltaLat * deltaLat));
    }

    //-------------------------------------------------------------------------
    // Calculate the Relative Bearing in degrees
    //
    public static float calcRelBrg(float lat1, float lon1, float lat2, float lon2, float hdg)
    {
        float deltaLat = lat2 - lat1;
        float deltaLon = lon2 - lon1; // Cartesian distance
        float departure = calcDeparture(deltaLon, (lat1 + lat2)/2);  // Euclidean distance, mid lat

        float relBrg = (float) (Math.toDegrees(Math.atan2(departure, deltaLat)) - hdg) % 360;  // the relative bearing to the apt
        if (relBrg > 180) relBrg = relBrg - 360;
        if (relBrg < -180) relBrg = relBrg + 360;
        return relBrg;
    }

    //-------------------------------------------------------------------------
    // Calculate the Absolute Bearing in degrees
    //
    public static float calcAbsBrg(float lat1, float lon1, float lat2, float lon2)
    {
        float deltaLat = lat2 - lat1;
        float deltaLon = lon2 - lon1; // Cartesian distance
        float departure = calcDeparture(deltaLon, (lat1 + lat2)/2);  // Euclidean distance, mid lat

        float absBrg = (float) (Math.toDegrees(Math.atan2(departure, deltaLat))) % 360;
        while (absBrg < 0) absBrg += 360;
        return absBrg;
    }

    //-------------------------------------------------------------------------
    // Calculate a new offset coordinate on radial rad, distance d (nm)
    //
    public static Coordinate calcOffsetCoor(float lat1, float lon1, float rad, float d)
    {
        double alpha = Math.toRadians(rad);
        float deltaLat = (float) (d/60 * Math.cos(alpha));
        float deltaLon = (float) (d/60 * Math.sin(alpha));  // ignore departure
        deltaLon = deltaLon / (float) Math.cos(Math.toRadians(lat1));  // adjust for departure

        return new Coordinate(lat1 + deltaLat, lon1 + deltaLon);
    }

    //-------------------------------------------------------------------------
    // Calculate the Distance to the Horizon in m
    // h in meters
    //
    public static float calcHorizonMetric(float h)  // in m
    {
        return (float) (3570 * Math.sqrt(h)); // in m
    }

    //-------------------------------------------------------------------------
    // Calculate the Distance to the Horizon in nm
    // h in ft
    //
    public static float calcHorizonNautical(float h) // in ft
    {
        return (float) Math.sqrt(h); // in nm
    }

    //-------------------------------------------------------------------------
    // Utility function to normalize an angle from any angle to
    // 0 to +180 and 0 to -180
    //
    public static float compassRose180(float angle)
    {
        angle = (angle) % 360;

        if (angle >  180) angle = angle - 360;
        if (angle < -180) angle = angle + 360;
        return angle;
    }

    //-------------------------------------------------------------------------
    // Utility function to normalize an angle from any angle to
    // 0 to +360
    //
    public static float compassRose360(float angle)
    {
        angle = (angle) % 360;

        if (angle <  0) angle = angle + 360;
        return angle;
    }

    //-------------------------------------------------------------------------
    // Utility function to convert a DMS string to a Decimal D (float)
    //Eg. 36:24:22 = 36.4061
    //
    public static float DMStoD(String dms)
    {
        float d = Float.parseFloat(dms.split(":")[0]);
        float m = Float.parseFloat(dms.split(":")[1]);
        float s = Float.parseFloat(dms.split(":")[2]);
        return (float)d + (float)m/60f + (float)s/3600f;
    }

    //-------------------------------------------------------------------------
    // Check if a lat lon in valid
    //
    public static boolean isValidLatLon(float lat, float lon)
    {
        if (Math.abs(lat) > 90) return false;
        return !(Math.abs(lon) > 180);
    }

    public static boolean isNullLatLon(float lat, float lon)
    {
        return (lat == 0) && (lon == 0);
    }

    //-------------------------------------------------------------------------
    // Mercator translations
    //

    public static final float EARTH_RADIUS = 6378137f;
    public static final float MAX_LATITUDE = 85.0511287798f;

    public static player.ulib.Point __projectMercator(float lat, float lon)
    {
        float  d = (float) Math.PI / 180;
        float  max = MAX_LATITUDE;
        lat = Math.max(Math.min(max, lat), -max);
        float  _sin = (float) Math.sin(lat * d);

        player.ulib.Point p = new Point();
        p.x = (int) (EARTH_RADIUS * lon * d);
        p.y = (int) (EARTH_RADIUS * Math.log((1 + _sin) / (1 - _sin)) / 2);

        return p;
    }


    static final float TILE_SIZE = 256;   // TODO
    public static MercatorInfo getMercatorTileInfo(float lat, float lon, int zoom)
    {
        int scale = (int) Math.pow(2, zoom);

        Point worldCoordinate = projectMercator(lat, lon);

        Point pixelCoordinate = new Point(
                Math.floor(worldCoordinate.x * scale),
                Math.floor(worldCoordinate.y * scale)
        );

       Point tileCoordinate = new Point(
                Math.floor((worldCoordinate.x * scale) / TILE_SIZE),
                Math.floor((worldCoordinate.y * scale) / TILE_SIZE)
        );

        MercatorInfo rv = new MercatorInfo();
        rv.worldCoordinate = worldCoordinate;
        rv.pixelCoordinate = pixelCoordinate;
        rv.tileCoordinate = tileCoordinate;

        rv.north = tile2lat((int)tileCoordinate.y, zoom);
        rv.south = tile2lat((int)tileCoordinate.y + 1, zoom);
        rv.west = tile2lon((int)tileCoordinate.x, zoom);
        rv.east = tile2lon((int)tileCoordinate.x + 1, zoom);
        return rv;
    }

    public static Point projectMercator(double lat, double lon)
    {
        double siny = (float) Math.sin((lat * Math.PI) / 180);

        // Truncating to 0.9999 effectively limits latitude to 89.189. This is
        // about a third of a tile past the edge of the world tile.
        siny = Math.min(Math.max(siny, -0.9999), 0.9999);

        Point p = new Point();
        p.x = TILE_SIZE * (0.5f + lon / 360);
        p.y = TILE_SIZE * (0.5f - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI));

        return p;
    }

    static double tile2lon(int x, int z)
    {
        return x / Math.pow(2.0, z) * 360.0 - 180;
    }

    static double tile2lat(int y, int z)
    {
        double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
        return Math.toDegrees(Math.atan(Math.sinh(n)));
    }

}


/*
class BoundingBox {
    double north;
    double south;
    double east;
    double west;
  }
  BoundingBox tile2boundingBox(final int x, final int y, final int zoom) {
    BoundingBox bb = new BoundingBox();
    bb.north = tile2lat(y, zoom);
    bb.south = tile2lat(y + 1, zoom);
    bb.west = tile2lon(x, zoom);
    bb.east = tile2lon(x + 1, zoom);
    return bb;
  }

  static double tile2lon(int x, int z) {
     return x / Math.pow(2.0, z) * 360.0 - 180;
  }

  static double tile2lat(int y, int z) {
    double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
    return Math.toDegrees(Math.atan(Math.sinh(n)));
  }

Tile numbers to lon./lat.
n = 2 ^ zoom
lon_deg = xtile / n * 360.0 - 180.0
lat_rad = arctan(sinh(pi * (1 - 2 * ytile / n)))
lat_deg = lat_rad * 180.0 / pi

This code returns the coordinate of the _upper left_ (northwest-most)-point of the tile.


function createInfoWindowContent(latLng: google.maps.LatLng, zoom: number) {
  const scale = 1 << zoom;

  const worldCoordinate = project(latLng);

  const pixelCoordinate = new google.maps.Point(
    Math.floor(worldCoordinate.x * scale),
    Math.floor(worldCoordinate.y * scale)
  );

  const tileCoordinate = new google.maps.Point(
    Math.floor((worldCoordinate.x * scale) / TILE_SIZE),
    Math.floor((worldCoordinate.y * scale) / TILE_SIZE)
  );

  return [
    "Chicago, IL",
    "LatLng: " + latLng,
    "Zoom level: " + zoom,
    "World Coordinate: " + worldCoordinate,
    "Pixel Coordinate: " + pixelCoordinate,
    "Tile Coordinate: " + tileCoordinate,
  ].join("<br>");
}
=========

// The mapping between latitude, longitude and pixels is defined by the web
// mercator projection.
function project(latLng: google.maps.LatLng) {
  let siny = Math.sin((latLng.lat() * Math.PI) / 180);

  // Truncating to 0.9999 effectively limits latitude to 89.189. This is
  // about a third of a tile past the edge of the world tile.
  siny = Math.min(Math.max(siny, -0.9999), 0.9999);

  return new google.maps.Point(
    TILE_SIZE * (0.5 + latLng.lng() / 360),
    TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI))
  );




    public static float zoomScale(int zoom)
    {
        return (256f * (float) Math.pow(2, zoom));
    }

    function transform (Point point, float scale)
    {
        scale = scale || 1;
        point.x = scale * (2.495320233665337e-8  * point.x + 0.5);
        point.y = scale * (-2.495320233665337e-8 * point.y + 0.5);
        return point;
    }

    var point1 = project (lat1, lng1);
    var scaledZoom = zoomScale (zoom);
    point1 = transform (point1, scaledZoom);
    */



/*
First, convert the latitude and longitude from degrees to radians:

lonRad = math.radians(lonDeg)
latRad = math.radians(latDeg)
and then convert the coordinates into the Mercator projection with

columnIndex = lonRad
rowIndex = math.log(math.tan(latRad) + (1.0 / math.cos(latRad)))
At this point, we have a column and row, but for a set of tiles which has its origin in the center of the collection. Usually, the origin of the collection is in the top, left corner. Also, we haven’t accounted for the zoom level yet. Let’s do that next, with

columnNormalized = (1 + (columnIndex / math.pi)) / 2
rowNormalized = (1 - (rowIndex / math.pi)) / 2

tilesPerRow = 2 ** zoomLevel

column = round(columnNormalized * (tilesPerRow - 1))
row = round(rowNormalized * (tilesPerRow - 1))
At this point, column, row and zoomLevel can be used to request the appropriate tile from a map tile service, which will contain the starting latitude and longitude and will be at the specified zoom level.
*/
